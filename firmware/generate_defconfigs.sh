#!/bin/bash
#
# Generate default config files

if [ -e .config ]; then
    mv .config .config.bak
fi

make alldefconfig
sed 's/Automatically generated file; DO NOT EDIT./DEFAULT settings/' \
    < .config > configs/defaults_defconfig

for i in gps_1604 gps_2004 nautik bluepill co2ampel; do
    cp -a configs/${i}_defconfig .config
    make olddefconfig
    sed 's/Automatically generated file; DO NOT EDIT./DEFAULT settings for '${i}'/' \
        < .config > configs/${i}_defconfig
done

if [ -e .config.bak ]; then
    mv .config.bak .config
else
    rm .config
fi

cd configs || exit

git add {defaults,gps_*,nautik,bluepill,co2ampel}_defconfig

cd ..
