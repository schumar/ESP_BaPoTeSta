/*
    HTML and CSS for the webserver
 */

// The CONFIG_SUPPORT_ macros are "y" when selected, and undefined otherwise
// -> convert to something more useful

#ifdef CONFIG_SUPPORT_DALLAS
#define SUPPORT_DALLAS "supported"
#else
#define SUPPORT_DALLAS "unsupported"
#endif

#ifdef CONFIG_SUPPORT_DHT
#define SUPPORT_DHT "supported"
#else
#define SUPPORT_DHT "unsupported"
#endif

#ifdef CONFIG_SUPPORT_BMP280
#define SUPPORT_BMP280 "supported"
#else
#define SUPPORT_BMP280 "unsupported"
#endif

#ifdef CONFIG_SUPPORT_BME280
#define SUPPORT_BME280 "supported"
#else
#define SUPPORT_BME280 "unsupported"
#endif

#ifdef CONFIG_SUPPORT_TSL2561
#define SUPPORT_TSL2561 "supported"
#else
#define SUPPORT_TSL2561 "unsupported"
#endif

#ifdef CONFIG_SUPPORT_BATTERY
#define SUPPORT_BATTERY "supported"
#else
#define SUPPORT_BATTERY "unsupported"
#endif

#ifdef CONFIG_SUPPORT_PERF
#define SUPPORT_PERF "supported"
#else
#define SUPPORT_PERF "unsupported"
#endif

#ifdef CONFIG_SUPPORT_WIFIRSSI
#define SUPPORT_WIFIRSSI "supported"
#else
#define SUPPORT_WIFIRSSI "unsupported"
#endif

#ifdef CONFIG_MQTT
#define SUPPORT_MQTT "supported"
#else
#define SUPPORT_MQTT "unsupported"
#endif

// HTML
//
// The R"()" function prevents us from having to escape characters in the text,
// https://en.cppreference.com/w/cpp/language/string_literal
const char indexPage[] =
R"(<!DOCTYPE html>
<html>
    <head>
        <title>ESP_BaPoTeSta Maintenance</title>
        <link href="style.css" rel="stylesheet" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
    </head>
    <body>
        <h1>ESP_BaPoTeSta Maintenance</h1>
        <h2>Configuration</h2>
        <form method="POST" action="/config" enctype="multipart/form-data"  class="pure-form pure-form-aligned">
            <fieldset>
                <legend>Network</legend>
                <div class="inputgroup">
                    <label for="ssid">ssid</label><input type="text" id="ssid" name="ssid" value="${ssid}" />
                    <label for="password">password</label><input type="text" id="password" name="password" placeholder="0 or 8+ chars" value="${password}" />
                    <label for="ip">IP</label><input type="text" id="ip" name="ip" value="${ip}" pattern="\d\d?\d?.\d\d?\d?.\d\d?\d?.\d\d?\d?" />
                    <label for="netmask">Netmask</label><input type="number" min="0" max="31" id="netmask" name="netmask" value="${netmask}" />
                    <label for="gw">Gateway</label><input type="text" pattern="\d\d?\d?.\d\d?\d?.\d\d?\d?.\d\d?\d?" id="gw" name="gw" value="${gw}" />
                </div>
                <div class="inputgroup )" SUPPORT_MQTT R"(">
                    <label for="mqttip">MQTT IP</label><input type="text" pattern="\d\d?\d?.\d\d?\d?.\d\d?\d?.\d\d?\d?" id="mqttip" name="mqttip" value="${mqttip}" />
                    <label for="mqttport">MQTT Port</label><input type="number" min="1" max="65535" id="mqttport" name="mqttport" value="${mqttport}" />
                </div>
            </fieldset>
            <fieldset>
                <legend>External Sensors</legend>
                <div class="inputgroup )" SUPPORT_DALLAS R"(">
                    <label for="usedallas">Use DS18B20</label><input type="checkbox" id="usedallas" name="usedallas" ${usedallas} />
                    <label for="dallasres"> resolution</label><input type="number" min="9" max="12" id="dallasres" name="dallasres" value="${dallasres}" />
                    <label for="biasdallastemp"> bias</label><input type="number" min="-1.0" max="1.0" step="0.01" id="biasdallastemp" name="biasdallastemp" value="${biasdallastemp}" />
                    <label for="dallaswait"> check for result</label><input type="checkbox" id="dallaswait" name="dallaswait" ${dallaswait} />
                </div>
                <div class="inputgroup )" SUPPORT_DHT R"(">
                    <label for="usedht">Use DHT</label><input type="checkbox" id="usedht" name="usedht" ${usedht} />
                    <label for="dhttype"> type</label><input type="number" min="11" max="33" id="dhttype" name="dhttype" value="${dhttype}" />
                    <label for="biasdhttemp"> temp bias</label><input type="number" min="-1.0" max="1.0" step="0.01" id="biasdhttemp" name="biasdhttemp" value="${biasdhttemp}" />
                    <label for="biasdhthumid"> hum bias</label><input type="number" min="-10" max="10" step="0.01" id="biasdhthumid" name="biasdhthumid" value="${biasdhthumid}" />
                    <label for="dhthi"> report HI</label><input type="checkbox" id="dhthi" name="dhthi" ${dhthi} />
                </div>

                <div class="inputgroup )" SUPPORT_BMP280 R"(">
                    <label for="usebmp280">Use BMP280</label><input type="checkbox" id="usebmp280" name="usebmp280" ${usebmp280} />
                    <label for="bmp280addr"> I2C addr</label><input type="text" id="bmp280addr" name="bmp280addr" value="${bmp280addr}" placeholder="0x76" />
                    <label for="bmp280press"> report actual pressure</label><input type="checkbox" id="bmp280press" name="bmp280press" ${bmp280press} />
                    <label for="bmp280slp"> report sea-level pressure</label><input type="checkbox" id="bmp280slp" name="bmp280slp" ${bmp280slp} />
                </div>

                <div class="inputgroup )" SUPPORT_BME280 R"(">
                    <label for="usebme280">Use BME280</label><input type="checkbox" id="usebme280" name="usebme280" ${usebme280} />
                    <label for="bme280addr"> I2C addr</label><input type="text" id="bme280addr" name="bme280addr" value="${bme280addr}" placeholder="0x76" />
                    <label for="bme280press"> report actual pressure</label><input type="checkbox" id="bme280press" name="bme280press" ${bme280press} />
                    <label for="bme280slp"> report sea-level pressure</label><input type="checkbox" id="bme280slp" name="bme280slp" ${bme280slp} />
                    <label for="bme280humi"> report humidity</label><input type="checkbox" id="bme280humi" name="bme280humi" ${bme280humi} />
                </div>

                <div class="inputgroup )" SUPPORT_TSL2561 R"(">
                    <label for="usetsl2561">Use TSL2561</label><input type="checkbox" id="usetsl2561" name="usetsl2561" ${usetsl2561} />
                    <label for="tsl2561addr"> I2C addr</label><input type="text" id="tsl2561addr" name="tsl2561addr" value="${tsl2561addr}" placeholder="0x39" />
                    <label for="tsl2561inttime"> integration time</label><input type="number" min="0" max="2" id="tsl2561inttime" name="tsl2561inttime" value="${tsl2561inttime}" placeholder="1" />
                    <label for="tsl2561raw"> incl. raw value</label><input type="checkbox" id="tsl2561raw" name="tsl2561raw" ${tsl2561raw} />
                </div>

                <div class="inputgroup">
                    <label for="heightasl">Height above sea-level</label><input type="number" min="0" max="10000" id="heightasl" name="heightasl" value="${heightasl}" />
                </div>
            </fieldset>
            <fieldset>
                <legend>Internal Measurements</legend>
                <div class="inputgroup )" SUPPORT_BATTERY R"(">
                    <label for="battery">Report battery</label><input type="checkbox" id="battery" name="battery" ${battery} />
                    <label for="battraw"> incl. raw value</label><input type="checkbox" id="battraw" name="battraw" ${battraw} />
                </div>
                <div class="inputgroup )" SUPPORT_WIFIRSSI R"(">
                    <label for="dowifi">Report WiFi strength</label><input type="checkbox" id="dowifi" name="dowifi" ${dowifi} />
                </div>
                <div class="inputgroup )" SUPPORT_PERF R"(">
                    <label for="doperf">Report performance</label><input type="checkbox" id="doperf" name="doperf" ${doperf} />
                    <label for="perfraw"> incl. raw value</label><input type="checkbox" id="perfraw" name="perfraw" ${perfraw} />
                </div>
                <div class="inputgroup">
                    <label for="deltat">Period (secs)</label><input type="number" min="1" max="9999" id="deltat" name="deltat" value="${deltat}" />
                </div>
            </fieldset>

            <div style="clear:left">
                <button type="reset">Revert</button>
                <button type="submit" class="primary">Apply</button>
                <a class="button" href="/reset">Reset to defaults</a>
                <a class="button" href="/i2cscan">I2C Scanner</a>
            </div>
        </form>

        <h2>Firmware Update</h2>
        <form method="POST" action="/update" enctype="multipart/form-data">
            <label for="update">Firmware:</label><input type="file" id="update" name="update" />
            <button type="submit" class="primary">Update</button>
        </form>
        <p>Firmware needs to be compiled for ${flashsize} KiB flash size!</p>

        <h2>Info</h2>
        <p>Current firmware was built on ${buildtime}</p>
        <ul>
        <li>MQTT is )" SUPPORT_MQTT R"(</li>
        <li>Dallas DS18B20 is )" SUPPORT_DALLAS R"(</li>
        <li>DHT sensors are )" SUPPORT_DHT R"(</li>
        <li>Bosch BMP280 is )" SUPPORT_BMP280 R"(</li>
        <li>Bosch BME280 is )" SUPPORT_BME280 R"(</li>
        <li>AMS/TAOS TSL2561 is )" SUPPORT_TSL2561 R"(</li>
        <li>Battery measuring is )" SUPPORT_BATTERY R"(</li>
        <li>Performance measuring is )" SUPPORT_PERF R"(</li>
        <li>WiFi measuring is )" SUPPORT_WIFIRSSI R"(</li>
        <ul>
    </body>
</html>)";

// CSS
const char css[] =
R"(
/*! Pure v0.6.0 || Copyright 2014 Yahoo! Inc. All rights reserved. || Licensed under the BSD License. || https://github.com/yahoo/pure/blob/master/LICENSE.md */
html{font-family:sans-serif;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}audio,canvas,progress,video{display:inline-block;vertical-align:baseline}audio:not([controls]){display:none;height:0}[hidden],template{display:none}a{background-color:transparent}a:active,a:hover{outline:0}abbr[title]{border-bottom:1px dotted}b,strong{font-weight:700}dfn{font-style:italic}h1{font-size:2em;margin:.67em 0}mark{background:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sup{top:-.5em}sub{bottom:-.25em}img{border:0}svg:not(:root){overflow:hidden}figure{margin:1em 40px}hr{-moz-box-sizing:content-box;box-sizing:content-box;height:0}pre{overflow:auto}code,kbd,pre,samp{font-family:monospace,monospace;font-size:1em}button,input,optgroup,select,textarea{color:inherit;font:inherit;margin:0}button{overflow:visible}button,select{text-transform:none}button,html input[type=button],input[type=reset],input[type=submit]{-webkit-appearance:button;cursor:pointer}button[disabled],html input[disabled]{cursor:default}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}input{line-height:normal}input[type=checkbox],input[type=radio]{box-sizing:border-box;padding:0}input[type=number]::-webkit-inner-spin-button,input[type=number]::-webkit-outer-spin-button{height:auto}input[type=search]{-webkit-appearance:textfield;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;box-sizing:content-box}input[type=search]::-webkit-search-cancel-button,input[type=search]::-webkit-search-decoration{-webkit-appearance:none}fieldset{border:1px solid silver;margin:0 2px;padding:.35em .625em .75em}legend{border:0;padding:0}textarea{overflow:auto}optgroup{font-weight:700}table{border-collapse:collapse;border-spacing:0}td,th{padding:0}.hidden,[hidden]{display:none!important}.pure-img{max-width:100%;height:auto;display:block}

html {
    margin: 0em 1em;
}
h2 {
    border-top:1px solid;
    padding-top:0.5em;
}
fieldset {
    border:none;
    width: 23em;
    float: left;
    margin: 0em 1em;
}
legend {
    border-bottom: 2px solid #e5e5e5;
    color: #333;
    display: block;
    margin-bottom: 0.3em;
    padding: 0.3em 0;
    width: 100%;
}
label {
    display: inline-block;
    margin: 0 1em 0 0;
    text-align: right;
    vertical-align: middle;
    width: 12em;
}
input {
    border: 1px solid #ccc;
    border-radius: 4px;
    box-shadow: 0 1px 3px #ddd inset;
    box-sizing: border-box;
    display: inline-block;
    padding: 0.5em 0.6em;
    vertical-align: middle;
    width: 10em;
}
input[type="number"] {
    width:7em;
}
input[type="checkbox"] {
    margin: 0.7em 0.5em;
    width: 1em;
}
input[type="file"] {
    width: 30em;
}
button, a.button {
    -moz-user-select: none;
    background-color: #e6e6e6;
    border-radius: 2px;
    border: 0 none rgba(0, 0, 0, 0);
    box-sizing: border-box;
    color: rgba(0, 0, 0, 0.8);
    cursor: pointer;
    display: inline-block;
    padding: 0.5em 1em;
    text-align: center;
    text-decoration: none;
    vertical-align: middle;
    white-space: nowrap;
}
button.primary {
    background-color: #0078e7;
    color: #fff;
}
div.inputgroup {
    margin:0.5em 0em;
    border-bottom: 1px solid #e5e5e5;
}
div.unsupported {
    color: #444;
    background-color: #eee;
}
)"
;

// vim: sw=4:expandtab:ts=4:tw=80:ft=arduino
