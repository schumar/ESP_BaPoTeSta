
enum sensorType {
    TEMP,
    BATTERY,
    HUMIDITY,
    TIME,
    TEMPHI,
    PRESSURE,
    PRESSUREASL,
    WIFIRSSI,
    GPS_SATS,
    GPS_DAYSEC,
    GPS_DATE,
    GPS_LAT,
    GPS_LNG,
    GPS_ALT,
    GPS_HDOP,
    SPEED,
    DIRECTION,
    DISTANCE,
    POWER,
    LIGHT_BB,
    LIGHT_IR,
    LUMINANCE,
    ECO2,
    ETVOC,
    CO2,
    CO2BG,
    CO2RAW,
    VOLTAGE,
    TDS,
    TURBIDITY,
    FREEMEM,
};

const char* sensorTypeName[] = {
    "temp",
    "battery",
    "humidity",
    "time",
    "tempHI",
    "pressure",
    "pressureASL",
    "wifiRSSI",
    "GPS-sats",
    "GPS-time",
    "GPS-date",
    "latitude",
    "longitude",
    "altitude",
    "GPS-hdop",
    "speed",
    "direction",
    "distance",
    "power",
    "lightBB",
    "lightIR",
    "luminance",
    "eCO2",
    "eTVOC",
    "CO2",
    "CO2bg",
    "CO2raw",
    "voltage",
    "TDS",
    "turbidity",
    "freemem",
};

enum unitType {
    CENT_DEGC,
    PERCENT,
    RAW,
    MVOLT,
    USEC,
    CENT_PERC,
    PASCAL,
    DBM,
    NUMBER,
    UDEG,
    MDEG,
    METER,
    DMETER,
    SEC,
    MKNOT,
    MPROH,
    MMPROS,
    DAMETER,
    CNMILE,
    CENT_WATT,
    LUX,
    PPM,
    PPB,
    MMETER,
    UVOLT,
    NTU,
    BYTE,
};

const char* unitTypeName[] = {
    "centdegc",
    "percent",
    "raw",
    "millivolt",
    "microsec",
    "centpercent",
    "pascal",
    "dbm",
    "",
    "microdegree",
    "millidegree",
    "meter",
    "decimeter",
    "seconds",
    "milliknots",
    "meterperhour",
    "mmpersec",
    "dekameter",
    "centinautmile",
    "centiwatt",
    "lux",
    "ppm",
    "ppb",
    "millimeter",
    "microvolt",
    "ntu",
    "byte",
};

struct sensorMeasurement {
    unsigned int sensorId;
    enum sensorType type;
    int32_t value;
    enum unitType unit;
};

struct allMeasurements {
    unsigned long int chipId;
    unsigned int timestep;
    byte nrMeasurements;
    struct sensorMeasurement * sensorMeasurements;
};

// The "CONFIG_" macros are "y" or "n" (or undefined), we need booleans
const bool y = true;
const bool n = false;

// The fact that the CONFIG_ macros might be undefined (because the item is
// set to "n", or because it's not shown at all) makes the following very
// ugly. Some preprocessor-magic might help...

struct config {
    byte cfgversion = 9;

#ifdef CONFIG_SUPPORT_WIFI

    char ssid[32] = 
#ifdef CONFIG_DEFAULT_WIFI_ESSID
CONFIG_DEFAULT_WIFI_ESSID;
#else
"";
#endif
    char password[32] = 
#ifdef CONFIG_DEFAULT_WIFI_PASSWORD
CONFIG_DEFAULT_WIFI_PASSWORD;
#else
"";
#endif

    IPAddress ip =
    #ifdef CONFIG_DEFAULT_NW_IP
        CONFIG_DEFAULT_NW_IP;
    #else
        0;
    #endif
    byte netmask =
    #ifdef CONFIG_DEFAULT_NW_MASK
        CONFIG_DEFAULT_NW_MASK;
    #else
        0;
    #endif
    IPAddress gw =
    #ifdef CONFIG_DEFAULT_NW_GW
        CONFIG_DEFAULT_NW_GW;
    #else
        0;
    #endif

    IPAddress mqttip = 
#ifdef CONFIG_DEFAULT_NW_MQTT
CONFIG_DEFAULT_NW_MQTT;
#else
0x0200000a;
#endif

    unsigned int mqttport = 
#ifdef CONFIG_DEFAULT_NW_MQTTPORT
CONFIG_DEFAULT_NW_MQTTPORT;
#else
1883;
#endif

#endif

    bool usedallas = 
#ifdef CONFIG_DALLAS_DEFAULT
CONFIG_DALLAS_DEFAULT;
#else
false;
#endif
    byte dallasres = 
#ifdef CONFIG_DALLAS_RES
CONFIG_DALLAS_RES;
#else
12;
#endif
    float biasDallasTemp = 
#ifdef CONFIG_DALLAS_BIAS_MC
CONFIG_DALLAS_BIAS_MC / 1000.;
#else
0;
#endif
    bool dallaswait = 
#ifdef CONFIG_DALLAS_WAIT
CONFIG_DALLAS_WAIT;
#else
false;
#endif

    bool usedht = 
#ifdef CONFIG_DHT_DEFAULT
CONFIG_DHT_DEFAULT;
#else
false;
#endif
    byte dhttype = 
#ifdef CONFIG_DHT_TYPE
CONFIG_DHT_TYPE;
#else
22;
#endif
    float biasDHTTemp = 
#ifdef CONFIG_DHT_BIAS_MC
CONFIG_DHT_BIAS_MC / 1000.;
#else
0;
#endif
    float biasDHTHumid = 
#ifdef CONFIG_DHT_BIAS_MPERC
CONFIG_DHT_BIAS_MPERC / 1000.;
#else
0;
#endif
    bool dhthi = 
#ifdef CONFIG_DHT_HI
CONFIG_DHT_HI;
#else
false;
#endif

    bool battery = 
#ifdef CONFIG_BATTERY_DEFAULT
CONFIG_BATTERY_DEFAULT;
#else
false;
#endif
    bool battraw = 
#ifdef CONFIG_BATTERY_REPORT_RAW
CONFIG_BATTERY_REPORT_RAW;
#else
false;
#endif

    bool doperf = 
#ifdef CONFIG_PERF_DEFAULT
CONFIG_PERF_DEFAULT;
#else
false;
#endif
    bool perfraw = 
#ifdef CONFIG_PERF_RAW
CONFIG_PERF_RAW;
#else
false;
#endif

    bool dowifi = 
#ifdef CONFIG_WIFIRSSI_DEFAULT
CONFIG_WIFIRSSI_DEFAULT;
#else
    false;
#endif

    bool usebmp280 = 
#ifdef CONFIG_BMP280_DEFAULT
true;
#else
false;
#endif
    byte bmp280addr = 
#ifdef CONFIG_BMP280_I2CADDR
CONFIG_BMP280_I2CADDR;
#else
0;
#endif
    bool bmp280press = 
#ifdef CONFIG_BMP280_PRESSURE
CONFIG_BMP280_PRESSURE;
#else
false;
#endif
    bool bmp280slp = 
#ifdef CONFIG_BMP280_SLP
CONFIG_BMP280_SLP;
#else
false;
#endif

    bool usebme280 = 
#ifdef CONFIG_BME280_DEFAULT
true;
#else
false;
#endif
    byte bme280addr = 
#ifdef CONFIG_BME280_I2CADDR
CONFIG_BME280_I2CADDR;
#else
0;
#endif
    bool bme280press = 
#ifdef CONFIG_BME280_PRESSURE
CONFIG_BME280_PRESSURE;
#else
false;
#endif
    bool bme280slp = 
#ifdef CONFIG_BME280_SLP
CONFIG_BME280_SLP;
#else
false;
#endif
    bool bme280humi = 
#ifdef CONFIG_BME280_HUMIDITY
CONFIG_BME280_HUMIDITY;
#else
false;
#endif

    int16_t heightASL = 
#ifdef CONFIG_BMX280_HEIGHTASL
CONFIG_BMX280_HEIGHTASL;
#else
0;
#endif


    bool useccs811 = 
#ifdef CONFIG_CCS811_DEFAULT
true;
#else
false;
#endif
    bool ccs811co2 = 
#ifdef CONFIG_CCS811_CO2
CONFIG_CCS811_CO2;
#else
false;
#endif
    bool ccs811voc = 
#ifdef CONFIG_CCS811_VOC
CONFIG_CCS811_VOC;
#else
false;
#endif
    bool ccs811bme280 = 
#ifdef CONFIG_CCS811_BME280
CONFIG_CCS811_BME280;
#else
false;
#endif

    bool usetsl2561 = 
#ifdef CONFIG_TSL2561_DEFAULT
true;
#else
false;
#endif
    byte tsl2561addr = 
#ifdef CONFIG_TSL2561_I2CADDR
CONFIG_TSL2561_I2CADDR;
#else
0x39;
#endif
    byte tsl2561inttime = 
#ifdef CONFIG_TSL2561_INTTIME
CONFIG_TSL2561_INTTIME;
#else
1;
#endif
    bool tsl2561raw = 
#ifdef CONFIG_TSL2561_RAW
true;
#else
false;
#endif


    unsigned int deltat = CONFIG_DELTA_T;

};


// vim: sw=4:expandtab:ts=4:tw=80:ft=arduino
