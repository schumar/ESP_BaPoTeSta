/*
   ESP8266 Battery Powered Temperature Station
   https://git.io/ESPTemp

   Martin Schuster 2015 - 2020
 */

// get the CONFIG_ values from Kbuild
#include "autoconf.h"

// version number
#include "versionfile.h"
// if the versionfile.h is empty, avoid compile breakage
#ifndef COMMITTAG
#ifdef CONFIG_LCD
#warn "Version number not set, run 'make'"
#endif
#define COMMITTAG ""
#endif
#ifndef COMMITDATE
#define COMMITDATE ""
#endif
#ifndef MAKEDATE
#define MAKEDATE ""
#endif

#ifdef CONFIG_SUPPORT_PCF8574
#include <jm_PCF8574.h>
jm_PCF8574 pcf8574;
bool buttonState[8];
long lastChangeTime[8];
bool anchorageWatch = false;
long anchorageLat, anchorageLon;
bool logEnabled = false;
#else
bool logEnabled = true;
#endif

#ifdef CONFIG_SUPPORT_WIFI
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include <PubSubClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <ESP8266HTTPUpdateServer.h>
#endif

#ifdef CONFIG_UC_ATMEGA328
// powersaving stuff -- see
// http://www.nongnu.org/avr-libc/user-manual/group__avr__power.html
#include <avr/power.h>
#endif

#ifdef CONFIG_UC_STM32F103
#include "STM32LowPower.h"
#ifdef CONFIG_SUPPORT_STM32TEMP
#include "stm32yyxx_ll_adc.h"
// The temperature calculation needs some values which can be found in the
// datasheet (stm32f103c8.pdf)
#define AVG_SLOPE 4300
#define CALX_TEMP 25
#define VREFINT 1200
#ifdef CONFIG_CKS32F103CBT6
#define V25 1620
#else
#define V25 1430
#endif
#endif
#ifdef CONFIG_HW_WATCHDOG_STM
#include "IWatchdog.h"
#endif
#endif

#include <math.h>

#ifdef CONFIG_SUPPORT_DALLAS
// #include <OneWire.h>
#include <DallasTemperature.h>
OneWire oneWire(0xff);
DallasTemperature dallasSensors(&oneWire);
#endif

#if defined(CONFIG_SUPPORT_BMP280) || defined(CONFIG_SUPPORT_BME280) || defined(CONFIG_SUPPORT_TSL2561)
#include <Adafruit_Sensor.h>
#endif
#ifdef CONFIG_SUPPORT_BMP280
#include <Adafruit_BMP280.h>
#endif
#ifdef CONFIG_SUPPORT_BME280
#include <Adafruit_BME280.h>
#endif
#ifdef CONFIG_SUPPORT_TSL2561
#include <Adafruit_TSL2561_U.h>
#endif
#ifdef CONFIG_SUPPORT_CCS811
#include <SparkFunCCS811.h>
#endif
#ifdef CONFIG_SUPPORT_MHZ19
#include <MHZ19.h>
#endif
#ifdef CONFIG_EPAPER
#include <GxEPD2_BW.h>
#include "Font5x7FixedMono.h"
#include <Fonts/FreeMonoBold9pt7b.h>
#include "GxEPD2_display_selection_new_style.h"
#endif
#ifdef CONFIG_NEOPIXEL
#include "WS2812FX.h"
#endif
#ifdef CONFIG_SUPPORT_ADS1115
#include <Adafruit_ADS1X15.h>
#endif
// need to declare prototype, as we are using optional parameters
float getADS1115(uint8_t channel = 255, uint16_t gain = 0);

#ifdef CONFIG_SUPPORT_DHT
#include <DHT.h>
#endif

// EEPROM is used to store configuration which is set via the config webserver
// -> only available wih WiFi
#ifdef CONFIG_SUPPORT_WIFI
#include <EEPROM.h>
#endif

#include <Wire.h>

#ifdef CONFIG_LCD
#include <LiquidCrystal_I2C.h>
#endif

#if defined(CONFIG_SUPPORT_GPS) || defined(CONFIG_OPENLOG) || defined(CONFIG_LORA) || defined(CONFIG_SUPPORT_MHZ19) || ( defined(CONFIG_SUPPORT_AJSR04M) && !defined(CONFIG_AJSR04M_USE_SERIAL3) )
#include <SoftwareSerial.h>
#endif
#ifdef CONFIG_SUPPORT_GPS
#include <MicroNMEA.h>
#endif

#include "html_css.h"
#include "ESP_BaPoTeSta.h"

// globals
struct sensorMeasurement sensorMeasurements[CONFIG_MAX_SENSORS];
struct allMeasurements data;
struct config config;

#ifdef CONFIG_SUPPORT_WIFI
WiFiClient espClient;
ESP8266WebServer httpServer(80);
IPAddress IPSubnet(255, 255, 255, 0);
#ifdef CONFIG_SERIAL
ESP8266HTTPUpdateServer httpUpdater(true);
#else
ESP8266HTTPUpdateServer httpUpdater;
#endif
#endif

#ifdef CONFIG_MQTT
PubSubClient mqttClient(espClient);
char idBuffer[32];
#endif

#ifdef CONFIG_LCD
LiquidCrystal_I2C lcd(CONFIG_LCD_I2CADDR, CONFIG_LCD_WIDTH, CONFIG_LCD_HEIGHT);
uint8_t posx, posy;
#define CUSTOM_CHARS 8
byte customChar[CUSTOM_CHARS][8] = {
    // We can only define up to 8 custom characters; if this is not enough,
    // add some logic to switch out the N/S and E/W chars (they are never
    // displayed at the same time)
    // Satellite symbol
    { 0b00010, 0b00100, 0b11100, 0b10101, 0b10101, 0b11100, 0b00100, 0b00010 },
    // 0 degree
    { 0b00011, 0b00011, 0b00000, 0b01100, 0b10010, 0b10010, 0b10010, 0b01100 },
    // N degree
    { 0b11000, 0b11000, 0b00000, 0b00100, 0b01110, 0b10101, 0b00100, 0b00100 },
    // S degree
    { 0b11000, 0b11000, 0b00000, 0b00100, 0b00100, 0b10101, 0b01110, 0b00100 },
    // E degree
    { 0b11000, 0b11000, 0b00000, 0b00100, 0b00010, 0b11111, 0b00010, 0b00100 },
    // W degree
    { 0b11000, 0b11000, 0b00000, 0b00100, 0b01000, 0b11111, 0b01000, 0b00100 },
    // Clock symbol
    { 0b11111, 0b10001, 0b01010, 0b00100, 0b01010, 0b10001, 0b11111, 0b11111 },
    // Heart symbol
    { 0b00000, 0b00000, 0b00000, 0b01010, 0b11111, 0b11111, 0b01110, 0b00100 }
};
// anti-flicker buffer
char lcdbuf[CONFIG_LCD_WIDTH*CONFIG_LCD_HEIGHT + 1];
#endif

#ifdef CONFIG_EPAPER
uint16_t ep_posx;
uint16_t ep_posy;
#endif

bool configmode = false;

#ifdef CONFIG_SUPPORT_DHT
DHT dhtSensor(0,0);
#endif
#ifdef CONFIG_SUPPORT_BMP280
Adafruit_BMP280 bmp280;
#endif
#ifdef CONFIG_SUPPORT_BME280
Adafruit_BME280 bme280;
#endif
#ifdef CONFIG_SUPPORT_TSL2561
Adafruit_TSL2561_Unified tsl2561(CONFIG_TSL2561_I2CADDR);
#endif
#ifdef CONFIG_SUPPORT_CCS811
CCS811 myCCS811(CONFIG_CCS811_I2CADDR);
#endif
float comp_temp = -100;   // for compensation of CCS811
float comp_humidity = -100;

#ifdef CONFIG_SUPPORT_MHZ19
MHZ19 myMHZ19;
SoftwareSerial mhzSer(CONFIG_PIN_MHZ19_RX, CONFIG_PIN_MHZ19_TX, false);
#endif

#ifdef CONFIG_SUPPORT_ADS1115
Adafruit_ADS1115 ads;
#endif

#ifdef CONFIG_NEOPIXEL
WS2812FX ws2812fx = WS2812FX(CONFIG_NEOPIXEL_LEDS, CONFIG_PIN_NEOPIXEL,
        NEO_GRB + NEO_KHZ800);
#endif

#ifdef CONFIG_SUPPORT_GPS
SoftwareSerial swSer(CONFIG_PIN_GPS, -1, false);
char nmeaBuffer[100];
MicroNMEA nmea(nmeaBuffer, sizeof(nmeaBuffer));
uint32_t gpsDistance;
uint32_t lastGpsTime;
long lastGpsSpeed;
#endif

#ifdef CONFIG_SUPPORT_AJSR04M
    #ifdef CONFIG_AJSR04M_USE_HWSERIAL
        HardwareSerial ajSer(CONFIG_PIN_AJSR04M_RX, CONFIG_PIN_AJSR04M_TX);
        #if defined CONFIG_PIN_AJSR04M_RX2 && CONFIG_PIN_AJSR04M_RX2 != -1
            HardwareSerial ajSer2(CONFIG_PIN_AJSR04M_RX2, CONFIG_PIN_AJSR04M_TX2);
        #endif
    #else
        SoftwareSerial ajSer(CONFIG_PIN_AJSR04M_RX, CONFIG_PIN_AJSR04M_TX, false);
        #if defined CONFIG_PIN_AJSR04M_RX2 && CONFIG_PIN_AJSR04M_RX2 != -1
            SoftwareSerial ajSer2(CONFIG_PIN_AJSR04M_RX2, CONFIG_PIN_AJSR04M_TX2, false);
        #endif
    #endif
#endif

#ifdef CONFIG_OPENLOG
SoftwareSerial olSer(-1, CONFIG_PIN_OPENLOG, false);
#endif

#ifdef CONFIG_LORA
#ifdef CONFIG_LORA_USE_HWSERIAL
HardwareSerial loraSer(-1, CONFIG_PIN_LORA_TX);
#else
// Using the same pin for RX and TX will enable half-duplex mode
SoftwareSerial loraSer(CONFIG_PIN_LORA_TX, CONFIG_PIN_LORA_TX, false);
#endif
#endif

// general use string buffer
char buf[64];

uint32_t lastlogtime = 2e9;  // ensure that first data is logged


// Ensure that the debugPrint() function is only defined if serial debugging
// is enabled; otherwise, use a no-op macro, to prevent the compiler from
// wasting precious flash memory on messages no-one will ever read :)
#ifdef CONFIG_SERIAL
void debugPrint(String msg) {
#ifdef CONFIG_UC_ESP8266
    unsigned long int cycles;
    cycles = ESP.getCycleCount();
    Serial.printf("DBG:%8luus ", cycles/(F_CPU/1000000));
#else
    char buf2[18];
    sprintf(buf2, "DBG:%8luus ", micros());
    Serial.print(buf2);
#endif
    Serial.println(msg);
}
#else
#define debugPrint(x) do {} while (0)
#endif


void setup() {
    #ifdef CONFIG_PIN_POWERDOWN
    if (CONFIG_PIN_POWERDOWN >= 0) {
        pinMode(CONFIG_PIN_POWERDOWN, OUTPUT);
        digitalWrite(CONFIG_PIN_POWERDOWN, LOW);
    }
    #endif

    #if defined(CONFIG_SERIAL) || defined(CONFIG_SERIAL_MINIMAL)
    Serial.begin(CONFIG_BAUD);
    #ifdef CONFIG_UC_STM32F103
    delay(CONFIG_SLEEP_SERIAL_STM);
    #else
    delay(1);
    #endif
    Serial.println("\r\n\nSYNCSYNC\n");
    #endif

    #ifdef CONFIG_SERIAL
    if (COMMITTAG[0] != 0)
        Serial.println((String)"BaPoTeSta " + COMMITTAG +
                " (" + COMMITDATE + "), " + "built " + MAKEDATE);
    #endif

    #ifdef CONFIG_SUPPORT_MHZ19
    mhzSer.begin(9600);
    #endif

    #ifdef CONFIG_SUPPORT_GPS
    swSer.begin(9600);
    #endif

    #ifdef CONFIG_SUPPORT_AJSR04M
    ajSer.begin(9600);
    #if defined CONFIG_PIN_AJSR04M_RX2 && CONFIG_PIN_AJSR04M_RX2 != -1
    ajSer2.begin(9600);
    #endif
    #endif

    #ifdef CONFIG_OPENLOG
    olSer.begin(9600);
    #endif

    #ifdef CONFIG_LORA
    loraSer.begin(9600);
    #endif

    // EEPROM is used to store configuration which is set via the config webserver
    // -> only available wih WiFi
    #ifdef CONFIG_SUPPORT_WIFI
    EEPROM.begin(1024);
    getConfig();
    #endif

    setBlueLed(true);

    // check if "config mode" jumper is set
    // as the config mode starts up a webserver, this won't work on ATmega
    configmode = false;
#ifdef CONFIG_UC_ESP8266
    if (CONFIG_PIN_CONFIG >= 0) {
        pinMode(CONFIG_PIN_CONFIG, INPUT_PULLUP);
        configmode = ! digitalRead(CONFIG_PIN_CONFIG);
    }
#endif
    if (configmode) {
        debugPrint("Starting webserver");
        setupWebserver();
    } else {
        debugPrint("Starting normally");
        setupNormal();
    }
}

void setupNormal() {

    // Arm watchdog
    #ifdef CONFIG_HW_WATCHDOG_STM
    debugPrint("Starting watchdog");
    // Enabling the watchdog might break running timers, so make sure we have
    // fully flushed the debug msg
    #ifdef CONFIG_SERIAL
    Serial.flush();
    #endif
    IWatchdog.begin(CONFIG_WATCHDOG_TIMEOUT_STM * 1000); // in μs
    #endif

    // start WiFi
#ifdef CONFIG_SUPPORT_WIFI
    if (config.ssid != NULL && config.ssid[0] != 0) {
        debugPrint("Connecting to WiFi...");
        WiFi.mode(WIFI_STA);
        #ifndef CONFIG_WIFI_DHCP
        WiFi.config(config.ip, config.gw, IPSubnet);
        #endif
        if (config.password == NULL || config.password[0] == 0) {
            WiFi.begin(config.ssid);
        } else {
            WiFi.begin(config.ssid, config.password);
        }
    } else {
        debugPrint("WiFi disabled");
        strcpy(config.ssid, "");
    }
#endif

    // now is a perfect time for "other" stuff, as WiFi will need some time
    // to associate

    // get ChipID, will be used as unique ID when sending data
#ifdef CONFIG_UC_ESP8266
    data.chipId = ESP.getChipId();
#elif CONFIG_UC_ATMEGA328
    data.chipId = 0x00328000 + CONFIG_CHIPNR;
#elif CONFIG_UC_STM32F103
    data.chipId = 0x0F103000 + CONFIG_CHIPNR;
#endif

    data.sensorMeasurements = sensorMeasurements;

    #ifdef CONFIG_EPAPER
    //display.init(115200); // default 10ms reset pulse, e.g. for bare panels with DESPI-C02
    display.init(115200, true, 2, false); // USE THIS for Waveshare boards with "clever" reset circuit, 2ms reset pulse
    display.setRotation(CONFIG_EPAPER_ROTATION);
    display.setFont(&Font5x7FixedMono);
    // display.setFont(&FreeMonoBold9pt7b);
    display.setTextColor(GxEPD_BLACK);
    display.setFullWindow();
    display.firstPage();
    ep_posx = 0;
    ep_posy = CONFIG_EPAPER_LINEHEIGHT - 1;
    display.fillScreen(GxEPD_WHITE);
    #ifdef CONFIG_EPAPER_SHOWVERSION
    display.setCursor(CONFIG_EPAPER_WIDTH>>1, CONFIG_EPAPER_HEIGHT-1);
    display.print(COMMITTAG);
    #endif
    #endif

    #ifdef CONFIG_UC_STM32F103
    LowPower.begin();
    // use 12bit ADC
    analogReadResolution(12);
    #endif

    if (config.battery) getBattery();

    powerSensors(true);     // activate power to sensors
    delay(CONFIG_POWERUP_MSECS);

#ifdef CONFIG_I2CSCAN
    debugI2Cscan();
#endif

#ifdef CONFIG_SUPPORT_DALLAS
    // setup Dallas sensors
    if (config.usedallas) {
        oneWire = OneWire(CONFIG_PIN_1WIRE);
        dallasSensors.begin();
        dallasSensors.setResolution(config.dallasres);
        dallasSensors.setCheckForConversion(config.dallaswait);
    }
#endif

    // setup DHT sensor
    #ifdef CONFIG_SUPPORT_DHT
    if (config.usedht) {
        dhtSensor = DHT(CONFIG_PIN_DHT, config.dhttype);
        dhtSensor.begin();
    }
    #endif

    // setup I2C
    // If LCD or PCF8574 is supported, do this; otherwise,
    // only if an I2C-sensor is used
    #if ! (defined(CONFIG_LCD) || defined(CONFIG_SUPPORT_PCF8574) || defined(CONFIG_SUPPORT_ADS1115))
    if (config.usebmp280 || config.usebme280 || config.usetsl2561 || config.useccs811) {
    #endif
        #ifdef CONFIG_UC_ESP8266
        Wire.begin(CONFIG_PIN_I2C_SDA, CONFIG_PIN_I2C_SCL);
        #elif CONFIG_UC_ATMEGA328
        Wire.begin();
        #elif CONFIG_UC_STM32F103
        Wire.setSCL(CONFIG_PIN_I2C_SCL);
        Wire.setSDA(CONFIG_PIN_I2C_SDA);
        Wire.begin();
        #endif
    #if ! (defined(CONFIG_LCD) || defined(CONFIG_SUPPORT_PCF8574) || defined(CONFIG_SUPPORT_ADS1115))
    }
    #endif

    // setup BMP280
    #ifdef CONFIG_SUPPORT_BMP280
    if (config.usebmp280) {
        // if bmp280addr is set, use it, otherwise stick to the default (0x77)
        if (! (config.bmp280addr > 0 ?
                    bmp280.begin(config.bmp280addr) : bmp280.begin())) {
            debugPrint("BMP280 not found.");
        }
    }
    #endif

    // setup BME280
    #ifdef CONFIG_SUPPORT_BME280
    if (config.usebme280) {
        delay(1);
        // if bme280addr is set, use it, otherwise stick to the default (0x77)
        if (! (config.bme280addr > 0 ?
                    bme280.begin(config.bme280addr) : bme280.begin())) {
            debugPrint("BME280 not found.");
        }
    }
    #endif

    // setup TSL2561
    #ifdef CONFIG_SUPPORT_TSL2561
    if (config.usetsl2561) {
        tsl2561 = Adafruit_TSL2561_Unified(config.tsl2561addr);
        tsl2561.begin();
/*        debugPrint((String)"TSL2561 ver=" + tsl2561.version +
                " id=" + tsl2561.id + " values=" + tsl2561.min_value +
                ".." + tsl2561.max_value + " resolution=" + tsl2561.resolution +
                " Lux"); */
        tsl2561.enableAutoRange(true);
        tsl2561.setIntegrationTime((tsl2561IntegrationTime_t) config.tsl2561inttime);
    }
    #endif

    // setup 1602 display
    #ifdef CONFIG_LCD
        lcd.init();
        lcd.backlight();

        for (uint8_t c = 0; c < CUSTOM_CHARS; c++)
            lcd.createChar(c, customChar[c]);

        lcd.home();
        lcd.print("BaPoTeSta");
        if (CONFIG_LCD_HEIGHT > 1) {
            setCursor(0,1);
            lcd.print(((String)MAKEDATE).substring(0, CONFIG_LCD_WIDTH));
        }
        if (CONFIG_LCD_HEIGHT > 2) {
            setCursor(0,2);
            lcd.print(((String)COMMITTAG).substring(0, CONFIG_LCD_WIDTH));
        }
        if (CONFIG_LCD_HEIGHT > 3) {
            setCursor(0,3);
            lcd.print(((String)COMMITDATE).substring(0, CONFIG_LCD_WIDTH));
        }
        lcd.home();
        delay(1000);
        debugPrint("LCD initialized");
    #endif

    // setup PCF8574 port multiplier
    #ifdef CONFIG_SUPPORT_PCF8574
    if (! pcf8574.begin(CONFIG_PCF8574_I2CADDR)) {
        debugPrint("PCF8574 not found.");
        #ifdef CONFIG_LCD
        lcd.print("PCF8574 not found.");
        setCursor(0, 1);
        lcd.print("Rebooting...");
        #endif
        reboot();
    }
    if (CONFIG_PCF8574_BTN_ANCHOR != -1) {
        pcf8574.pinMode(CONFIG_PCF8574_BTN_ANCHOR, INPUT);
        buttonState[CONFIG_PCF8574_BTN_ANCHOR] =
            pcf8574.digitalRead(CONFIG_PCF8574_BTN_ANCHOR);
    }
    if (CONFIG_PCF8574_BTN_LOG != -1) {
        pcf8574.pinMode(CONFIG_PCF8574_BTN_LOG, INPUT);
        buttonState[CONFIG_PCF8574_BTN_LOG] =
            pcf8574.digitalRead(CONFIG_PCF8574_BTN_LOG);
    }
    if (CONFIG_PCF8574_OUT_BUZZER != -1)
        pcf8574.pinMode(CONFIG_PCF8574_OUT_BUZZER, OUTPUT);
    #endif

    // setup OpenLog
    #ifdef CONFIG_OPENLOG
    // Initialize card, to ensure that we open a new file after a reboot
    // (even if power to OpenLog was not interrupted)
    olSer.print("\x1a\x1a\x1a");    // 3 times ^Z switches to command mode
    delay(10);
    olSer.print("reset\r\n");
    delay(100);
    #endif

    #ifdef CONFIG_SUPPORT_CCS811
    if (config.useccs811) {
        CCS811::CCS811_Status_e returnCode = myCCS811.beginWithStatus();
        if (returnCode != CCS811::CCS811_Stat_SUCCESS) {
            debugPrint((String)"CCS811 init returns: " + returnCode);
            config.useccs811 = false;
        } else {
            // wait a bit for the heating element to get hot enough
            delay(500);
        }
    }
    #endif

    #ifdef CONFIG_SUPPORT_MHZ19
    myMHZ19.begin(mhzSer);
    myMHZ19.autoCalibration();
    #ifdef CONFIG_SERIAL
        char mhzVer;
        myMHZ19.getVersion(&mhzVer);
        debugPrint((String)"Found MH-Z19 version " + (int8_t)mhzVer);
        debugPrint((String)"    with ABC " + (myMHZ19.getABC() ? "on" : "off"));
        debugPrint((String)"    and range " + myMHZ19.getRange());
    #endif
    #endif

    #ifdef CONFIG_SUPPORT_ADS1115
    if (! ads.begin(CONFIG_ADS1115_ADDR)) {
        debugPrint("ADS1115 init failed!");
    }
    #endif

    #ifdef CONFIG_NEOPIXEL
    ws2812fx.init();
    ws2812fx.setBrightness(32);
    ws2812fx.setColor(BLUE);
    ws2812fx.setSpeed(2000);
    ws2812fx.setMode(FX_MODE_RAINBOW_CYCLE);
    ws2812fx.start();
    for (uint8_t i=0; i<200; i++) {
        ws2812fx.service();
        delay(10);
    }
    #endif

#ifdef CONFIG_SUPPORT_WIFI
    // The rest handles networking (WiFi/MQTT), we can skip that if WiFi
    // is disabled
    if (config.ssid[0] == 0) return;

    // wait until WiFi is connected, but max WIFICHECK_TRIES
    for (byte i = 0;
            i < CONFIG_WIFICHECK_TRIES
            && WiFi.status() != WL_CONNECTED;
            i++) {
        updateNeopixel();
        delay(CONFIG_SLEEP_WIFICHECK);
    }
    // if this didn't work, go back to sleep
    if (WiFi.status() != WL_CONNECTED) {
        debugPrint("Not able to connect to WiFi.");
        gotoSleep(CONFIG_NOCONN_SLEEPSECS);
        reboot();
    }
    // WiFi is set up now. Set a "random" local port-number to prevent re-using
    // a connection from last the last run, see
    // https://blog.voneicken.com/2018/lp-wifi-esp8266-2/
    // https://en.wikipedia.org/wiki/Ephemeral_port
    espClient.setLocalPortStart(49152 + (ESP.getCycleCount() % 10000));
#endif

    // Some debugging info
#ifdef CONFIG_SERIAL
#ifdef CONFIG_SUPPORT_WIFI
    debugPrint((String)"Connected to WiFi, IP=" + ipToString(WiFi.localIP()));
#endif
    sprintf(buf, "ChipID: 0x%08lx", data.chipId);
    debugPrint(buf);
#endif

#ifdef CONFIG_MQTT
    // connect to MQTT server
    mqttClient.setServer(config.mqttip, config.mqttport);
    sprintf(idBuffer, "esp8266-%08lx", data.chipId);
    debugPrint("Connecting to MQTT server " + ipToString(config.mqttip) + ":" +
            config.mqttport + " ...");

    boolean mqttResult;
    #ifdef CONFIG_MQTT_USEAUTH
    mqttResult = mqttClient.connect(idBuffer, CONFIG_MQTT_USER,
            CONFIG_MQTT_PASS);
    #else
    mqttResult = mqttClient.connect(idBuffer);
    #endif

    if (mqttResult) {
        debugPrint("    connected");
    } else {
        debugPrint("    FAILED");
        gotoSleep(CONFIG_NOCONN_SLEEPSECS);
        reboot();
    }
#endif

}


void loop() {
#ifdef CONFIG_SUPPORT_WIFI
    if (configmode) {
        httpServer.handleClient();
        delay(1);
        return;
    }
#endif

    updateNeopixel();

    // This will only "loop" in continuous mode; otherwise, the last command
    // (gotoSleep) will lead to a reset

    setBlueLed(true);

    // On a fresh start, we have already measured the battery (and the RAW
    // ADC value if config.battraw is set)
    // But otherwise, delete all data from previous run
    #ifdef CONFIG_SUPPORT_BATTERY
        if (data.nrMeasurements > 2 ||
                (! config.battraw && data.nrMeasurements > 1)) {
            // more than the battery has been measured -> not a fresh start
            data.nrMeasurements = 0;
            #ifdef CONFIG_EPAPER
            ep_posy = CONFIG_EPAPER_LINEHEIGHT - 1;
            display.fillScreen(GxEPD_WHITE);
            #endif
        }
    #else
        data.nrMeasurements = 0;
        #ifdef CONFIG_EPAPER
        ep_posy = CONFIG_EPAPER_LINEHEIGHT - 1;
        display.fillScreen(GxEPD_WHITE);
        #endif
    #endif

    data.timestep = 0;      // [XXX] this needs to be read from eprom and ++

    mqttSend();

    #ifdef CONFIG_LCD
    posx = posy = 0;
    memset(lcdbuf, ' ', CONFIG_LCD_HEIGHT*CONFIG_LCD_WIDTH);
    setCursor(CONFIG_LCD_WIDTH-1, CONFIG_LCD_HEIGHT-1);
    lcd.print("\x07");
    #endif

    // In continuous mode, we need to power up the sensors again
    #ifdef CONFIG_CONTINUOUS
    powerSensors(true);     // activate power to sensors
    delay(CONFIG_POWERUP_MSECS);
    #endif

    collectData();          // make measurements

    // I2C might be pulled-up to the power-sensor line, so update the
    // LCD before switching off the senors
    updateLCD();

    powerSensors(false);    // deactivate power to sensors again

    // Send data to server and/or OpenLog
    // substracting timestamps -> rollover-safe
    if (millis() - lastlogtime > CONFIG_DELTA_T_LOG * 1000L) {
        lastlogtime = millis();
        sendData();
        mqttSend();
        if (logEnabled) {
            logData();
        } else {
            debugPrint("Not sending data, logging disabled.");
        }
    } else {
        debugPrint((String)"Not sending data, lastlogtime=" + lastlogtime +
                " millis=" + millis());
    }

    // wait a little bit, to ensure that everything is sent
    delay(CONFIG_SLEEP_END);
    mqttSend();
    gotoSleep(config.deltat);
}

void collectData() {
    debugPrint("Collecting data...");
    // Battery might have already been measured
    if (config.battery && data.nrMeasurements == 0) getBattery();
    getADC();
    getSTM32temp();
    if (config.usedallas) getDallas();
    if (config.usebmp280) getBMP280();
    if (config.usebme280) getBME280();
    if (config.usetsl2561) getTSL2561();
    if (config.usedht) getDHT();
    if (config.useccs811) getCCS811();
    getMHZ19();

#ifdef CONFIG_SUPPORT_AJSR04M
    getAJSR04M(&ajSer, 0x4104);
#if defined CONFIG_PIN_AJSR04M_RX2 && CONFIG_PIN_AJSR04M_RX2 != -1
    getAJSR04M(&ajSer2, 0x4105);
#endif
#endif

    getTDS();
    getSEN0189();
    getADS1115();
    if (config.dowifi) getWiFi();
    if (config.doperf) getPerf();
    getMem();
    getGPS();
}

void getGPS() {
#ifdef CONFIG_SUPPORT_GPS
    bool coursevalid;
    debugPrint("Getting GPS data");

    addData(0x0701, GPS_SATS, nmea.getNumSatellites(), NUMBER);
    if (nmea.getHour() < 24) {   // wrong after initialization
        addData(0x0701, GPS_DAYSEC,
                (nmea.getHour() * 60 + nmea.getMinute()) * 60 + nmea.getSecond(),
                SEC);
        addData(0x0701, GPS_DATE,
                (nmea.getYear() * 10000 + nmea.getMonth() * 100 + nmea.getDay()),
                RAW);
    }
    if (nmea.isValid()) {
        long lat = nmea.getLatitude();
        long lon = nmea.getLongitude();
        long speed = nmea.getSpeed();

        // getSpeed() returns milli-knots/hour
        #ifdef CONFIG_GPS_SPEED_MS
        addData(0x0701, SPEED, speed * 1.852 / 3.6, MMPROS);
        #elif CONFIG_GPS_SPEED_KMH
        addData(0x0701, SPEED, speed * 1.852, MPROH);
        #else
        addData(0x0701, SPEED, speed, MKNOT);
        #endif

        // Ignore bogus 0.000 "course" values (when standing still)
        if (speed > 5e3 || nmea.getCourse() != 0) {
            coursevalid = true;
            addData(0x0701, DIRECTION, nmea.getCourse(), MDEG);
        } else {
            coursevalid = false;
        }

        addData(0x0701, GPS_LAT, lat, UDEG);

        addData(0x0701, GPS_HDOP, nmea.getHDOP(), DMETER);

        long alt;
        if (nmea.getAltitude(alt))
            addData(0x0701, GPS_ALT, alt / 1000, METER);

        addData(0x0701, GPS_LNG, lon, UDEG);

        #ifdef CONFIG_GPS_DISTANCE
        if (coursevalid && logEnabled) {
            if (lastGpsTime > 0) {
                //          use average of previous and current speed
                gpsDistance += ((float)speed+lastGpsSpeed)/2 /3600
                    * (millis() - lastGpsTime)/(float)1000;
            }
        }
        lastGpsTime = millis();
        lastGpsSpeed = speed;
        #ifdef CONFIG_GPS_SPEED_KNS
        addData(0x0701, DISTANCE, gpsDistance / 10, CNMILE);
        #else
        addData(0x0701, DISTANCE, gpsDistance * 1.852 / 10, DAMETER);
        #endif
        #endif

        // Check Anchorage Watch
        #ifdef CONFIG_SUPPORT_PCF8574
        if (anchorageWatch) {
            // 30m is ~30m/40km*360 degrees ~ 250 microDegree
            if (abs(lat - anchorageLat) > 250 || abs(lon - anchorageLon) > 250) {
                alert(true);
                #ifdef CONFIG_LCD
                *lcdpos(CONFIG_LCD_WIDTH-2, CONFIG_LCD_HEIGHT-1) = '!';
                #endif
            }
        }
        #endif
    }
#endif
}

void getDallas() {
#ifdef CONFIG_SUPPORT_DALLAS
    uint8_t addr[8];
    float temp;
    uint16_t id = CONFIG_DALLAS_ID;
    uint8_t nr_devices;

    debugPrint("Searching for Dallas sensors...");
    nr_devices = dallasSensors.getDeviceCount();
    debugPrint((String)"    " + nr_devices + " found.");

    if (nr_devices == 0) {
        if (id > 0) addData(id, TEMP, INT32_MAX, CENT_DEGC);
        return;
    }

    if (id > 0 && nr_devices > 1) {
        debugPrint("    WARNING: Fixed ID set, only using first device");
        nr_devices = 1;
    }

    for (uint8_t devnr = 0; devnr < nr_devices; devnr++) {

        id = CONFIG_DALLAS_ID;

        // try to find address of sensor
        if (dallasSensors.getAddress(addr, devnr) == 0) {
            // if CONFIG_DALLAS_ID was set to 0, we can't send an error value
            if (id > 0) addData(id, TEMP, INT32_MAX, CENT_DEGC);
            debugPrint((String)"    Not able to get addr of Dallas #" + devnr);
            continue;
        }

        if (id == 0)
            // use last two byte of serial as ID (addr[0] is "family code")
            id = (addr[2]<<8) + addr[1];

        dallasSensors.requestTemperaturesByAddress(addr);
        temp = dallasSensors.getTempC(addr);

        // sanity check; valid range taken from datasheet
        // "85" is the "reset value", ignore it too
        if (temp < -55.0 || temp > 125.0 || (temp > 84.95 && temp < 85.05)) {
            addData(id, TEMP, INT32_MAX, CENT_DEGC);
            continue;
        }

        // add the bias after checking for "85" above
        temp += config.biasDallasTemp;

        comp_temp = temp;   // for compensation of CCS811

        addData(id, TEMP, (int16_t) (temp * 100.0), CENT_DEGC);

    }
#endif
}

void getSTM32temp() {
#ifdef CONFIG_SUPPORT_STM32TEMP
    // calibrate ADC
    int32_t VRef = (VREFINT * 4096 / analogRead(AVREF));

    int32_t raw = analogRead(ATEMP);

    // see https://github.com/stm32duino/wiki/wiki/API#adc-internal-channels
    int32_t temp = __LL_ADC_CALC_TEMPERATURE_TYP_PARAMS(AVG_SLOPE, V25,
            CALX_TEMP, VRef, raw, 12);
    addData(0x0000, TEMP, (int16_t) (temp * 100.0), CENT_DEGC);

    #ifdef CONFIG_STM32TEMP_REPORT_RAW
    addData(0x0000, TEMP, raw, RAW);
    #endif
#endif
}

void getBMP280() {
#ifdef CONFIG_SUPPORT_BMP280
    float temp;
    float pres;

    debugPrint("Querying BMP280...");

    temp = bmp280.readTemperature();
    pres = bmp280.readPressure();

    if (pres < 1.0)
        return;

    // report temperature
    addData(0x0280, TEMP, (int16_t) (temp * 100.0), CENT_DEGC);

    if (config.bmp280press) {
        // report measured pressure (current height)
        addData(0x0280, PRESSURE, (uint32_t) (pres), PASCAL);
    }

    if (config.bmp280slp && config.heightASL > 0) {

        // sanity check; pressure at sea level will be >98kPa and <106kPa
        float slp = sealevelPressure(pres);
        if (slp < 98e3 || slp > 106e3)
            return;

        // report pressure at sea level
        addData(0x0280, PRESSUREASL, (uint32_t) slp, PASCAL);
    }
#endif
}

void getBME280() {
#ifdef CONFIG_SUPPORT_BME280
    float temp;
    float pres;
    float humi;

    debugPrint("Querying BME280...");

    temp = bme280.readTemperature();
    pres = bme280.readPressure();
    humi = bme280.readHumidity();

    if (pres < 1.0 || pres > 200e3 || temp < -100 || temp > 100)
        return;

    comp_temp = temp;
    comp_humidity = humi;

    // report temperature
    addData(0x1280, TEMP, (int16_t) (temp * 100.0), CENT_DEGC);

    if (config.bme280press) {
        // report measured pressure (current height)
        addData(0x1280, PRESSURE, (uint32_t) (pres), PASCAL);
    }
    if (config.bme280humi) {
        // report humidity
        addData(0x1280, HUMIDITY, (uint16_t) (humi * 100.0), CENT_PERC);
    }

    if (config.bme280slp && config.heightASL > 0) {

        // sanity check; pressure at sea level will be >98kPa and <106kPa
        float slp = sealevelPressure(pres);
        if (slp < 98e3 || slp > 106e3)
            return;

        // report pressure at sea level
        addData(0x1280, PRESSUREASL, (uint32_t) slp, PASCAL);
    }
#endif
}


void getTSL2561() {
#ifdef CONFIG_SUPPORT_TSL2561
    uint16_t raw_bb;
    uint16_t raw_ir;
    uint32_t luminance;

    debugPrint("Querying TSL2561...");

    tsl2561.getLuminosity(&raw_bb, &raw_ir);

    if (raw_bb == 0xffff && raw_ir == 0xffff)
        return;

    if (config.tsl2561raw) {
        addData(0x2561, LIGHT_BB, raw_bb, RAW);
        addData(0x2561, LIGHT_IR, raw_ir, RAW);
    }

    luminance = tsl2561.calculateLux(raw_bb, raw_ir);
    addData(0x2561, LUMINANCE, luminance, LUX);
#endif
}


void getDHT() {
#ifdef CONFIG_SUPPORT_DHT
    debugPrint((String)"Querying DHT" + config.dhttype + "...");

    delay(CONFIG_DHT_SLEEP);    // [XXX] should substract the time since powerup

    // store values, so they can be used for calculating the heat index later
    float temp = dhtSensor.readTemperature(false, true) + config.biasDHTTemp;
    float hum = dhtSensor.readHumidity() + config.biasDHTHumid;

    // sanity check; valid range taken from datasheet
    if (temp < -40.0 || temp > 80.0 || hum < 0.0 || hum > 100.0
            || isnan(temp) || isnan(hum))
        return;

    comp_temp = temp;
    comp_humidity = hum;

    // use the DHT_TYPE as sensor ID, as the DHT doesn't have a real ID
    addData(config.dhttype, TEMP, (int16_t) (temp * 100.0), CENT_DEGC);
    addData(config.dhttype, HUMIDITY, (uint16_t) (hum * 100.0), CENT_PERC);

    if (config.dhthi)
        addData(config.dhttype, TEMPHI,
                (int16_t) (dhtSensor.computeHeatIndex(temp, hum, false) * 100.0),
                CENT_DEGC);
#endif
}

void getCCS811() {
#ifdef CONFIG_SUPPORT_CCS811
    uint16_t co2;
    uint16_t tvoc;

    debugPrint("Querying CCS811...");

    #ifdef CONFIG_CCS811_BME280
    if (comp_humidity >= 0 && comp_temp > -50)
        myCCS811.setEnvironmentalData(comp_humidity, comp_temp);
    else
        debugPrint("CCS811: data for compensation missing");
    #endif

    // wait for data to be available
    delay(4000);
    for (uint8_t wait = 0; wait < 500 && ! myCCS811.dataAvailable(); wait++) {
        delay(100);
    }

    if (! myCCS811.dataAvailable()) {
        #ifdef CONFIG_SERIAL
        if (myCCS811.checkForStatusError()) {
            uint8_t error = myCCS811.getErrorRegister();

            if ( error == 0xFF ) { //comm error
                debugPrint("CCS811: Failed to get ERROR_ID register.");
            } else {
                if (error & 1 << 5) debugPrint("CCS811 error: HeaterSupply");
                if (error & 1 << 4) debugPrint("CCS811 error: HeaterFault");
                if (error & 1 << 3) debugPrint("CCS811 error: MaxResistance");
                if (error & 1 << 2) debugPrint("CCS811 error: MeasModeInvalid");
                if (error & 1 << 1) debugPrint("CCS811 error: ReadRegInvalid");
                if (error & 1 << 0) debugPrint("CCS811 error: MsgInvalid");
            }
        } else {
            debugPrint("CCS811: No data available.");
        }
        #endif
        return;
    }

    myCCS811.readAlgorithmResults();
    co2 = myCCS811.getCO2();
    tvoc = myCCS811.getTVOC();

    while (co2 == 0 && tvoc == 0) {
        debugPrint("CCS811: CO2=0, TVOC=0 -> Trying again");
        delay(500);
        while (! myCCS811.dataAvailable()) {
            delay(100);
        }
        myCCS811.readAlgorithmResults();
        co2 = myCCS811.getCO2();
        tvoc = myCCS811.getTVOC();
    }

    addData(0x0811, ECO2, co2, PPM);
    addData(0x0811, ETVOC, tvoc, PPB);
#endif
}


float getADS1115(uint8_t channel, uint16_t gain) {
#ifdef CONFIG_SUPPORT_ADS1115

    if (channel < 4) {
        debugPrint((String)"    Querying ADS1115 channel A" + channel + "...");

        ads.setGain((adsGain_t) gain);
        float result = ads.computeVolts(ads.readADC_SingleEnded(channel));

        #ifdef CONFIG_ADS1115_RAW_A0
        if (channel == 0)
            addData(0x1115, VOLTAGE, (int32_t)(1e6 * result + 0.5), UVOLT);
        #endif
        #ifdef CONFIG_ADS1115_RAW_A1
        if (channel == 1)
            addData(0x1116, VOLTAGE, (int32_t)(1e6 * result + 0.5), UVOLT);
        #endif
        #ifdef CONFIG_ADS1115_RAW_A2
        if (channel == 2)
            addData(0x1117, VOLTAGE, (int32_t)(1e6 * result + 0.5), UVOLT);
        #endif
        #ifdef CONFIG_ADS1115_RAW_A3
        if (channel == 3)
            addData(0x1118, VOLTAGE, (int32_t)(1e6 * result + 0.5), UVOLT);
        #endif

        return result;
    }

    // we only reach this point if channel==255, i.e. for the getADS1115()
    // call w/o any parameters. Read/report all raw values, if requested
    // using ADS1115_ALWAYS_RAW
    #ifdef CONFIG_ADS1115_ALWAYS_RAW

    debugPrint("Querying ADS1115...");

    #ifdef CONFIG_ADS1115_RAW_A0
    ads.setGain((adsGain_t) CONFIG_ADS1115_RAW_A0_GAIN);
    addData(0x1115, VOLTAGE,
            (int32_t)(1e6 * ads.computeVolts(ads.readADC_SingleEnded(0)) + 0.5), UVOLT);
    #endif
    #ifdef CONFIG_ADS1115_RAW_A1
    ads.setGain((adsGain_t) CONFIG_ADS1115_RAW_A1_GAIN);
    addData(0x1116, VOLTAGE,
            (int32_t)(1e6 * ads.computeVolts(ads.readADC_SingleEnded(1)) + 0.5), UVOLT);
    #endif
    #ifdef CONFIG_ADS1115_RAW_A2
    ads.setGain((adsGain_t) CONFIG_ADS1115_RAW_A2_GAIN);
    addData(0x1117, VOLTAGE,
            (int32_t)(1e6 * ads.computeVolts(ads.readADC_SingleEnded(2)) + 0.5), UVOLT);
    #endif
    #ifdef CONFIG_ADS1115_RAW_A3
    ads.setGain((adsGain_t) CONFIG_ADS1115_RAW_A3_GAIN);
    addData(0x1118, VOLTAGE,
            (int32_t)(1e6 * ads.computeVolts(ads.readADC_SingleEnded(3)) + 0.5), UVOLT);
    #endif

    #endif

#endif
    return NAN;   // we promised to return something
}


void getTDS() {
#ifdef CONFIG_SUPPORT_TDS

    float T = 25.0;

    debugPrint("Querying TDS...");

    #ifdef CONFIG_TDS_TEMPCOMP
    // look in stored data for water temperature
    for (uint8_t i; i <= data.nrMeasurements; i++) {
        if (sensorMeasurements[i].sensorId == CONFIG_TDS_WATERTEMP_SENSORID
         && sensorMeasurements[i].type == TEMP
         && sensorMeasurements[i].unit == CENT_DEGC) {
            T = (float) sensorMeasurements[i].value / 100.0;
        }
    }
    if (T <= 0.0 || T >= 50.0 || T == NAN) {
        debugPrint("    Ignoring wrong compensation temperature");
        T = 25.0;
    }
    #endif

    float U = getADS1115(CONFIG_TDS_CHANNEL, ADS1X15_REG_CONFIG_PGA_4_096V);
    if (U < 0 || U > 2.3) {
        addData(0x07D5, TDS, INT32_MAX, PPB);
        return;
    }

    float U2 = U*U;
    float U3 = U2*U;
    float ppm = (133.42*U3 - 255.86*U2 + 857.39*U)
                * CONFIG_TDS_K / 1000.0         // calibration
                /
                ( 1.0 + 0.02*(T - 25.0) )       // temp compensation
                /
                2                               // uS -> ppm
                ;

    if (ppm > 0 && ppm < 2500)
        addData(0x07D5, TDS, (int32_t)(ppm * 1e3), PPB);
    else
        addData(0x07D5, TDS, INT32_MAX, PPB);

#endif
}


void getSEN0189() {
#ifdef CONFIG_SUPPORT_SEN0189

    float T = 10.0;

    debugPrint("Querying SEN0189...");

    float U = getADS1115(CONFIG_SEN0189_CHANNEL, ADS1X15_REG_CONFIG_PGA_6_144V);
    U += CONFIG_SEN0189_MVOFF / 1000.0;

    #ifdef CONFIG_SEN0189_TEMPCOMP
    // look in stored data for water temperature
    for (uint8_t i; i <= data.nrMeasurements; i++) {
        if (sensorMeasurements[i].sensorId == CONFIG_SEN0189_WATERTEMP_SENSORID
         && sensorMeasurements[i].type == TEMP
         && sensorMeasurements[i].unit == CENT_DEGC) {
            T = (float) sensorMeasurements[i].value / 100.0;
        }
    }
    if (T <= 0.0 || T >= 50.0 || T == NAN) {
        debugPrint((String)"    Ignoring wrong compensation temperature " +
                (int32_t)(T*100) + "cC");
        T = 10.0;
    }

    // Add 1% for every 10 degC temperature difference
    // The diagram in datasheet suggests that it's more like 3%,
    // https://dfimg.dfrobot.com/nobody/wiki/8e585d98aafe2bab22be39c5b68165c5.pdf
    // but that doesn't match my measurements
    U *= 1 + 0.001 * T;
    #endif

    if (U < 2.5 || U > 4.2) {
        addData(0x0189, TURBIDITY, INT32_MAX, NTU);
        debugPrint((String)"    Ignoring U=" + U + "V");
        return;
    }

    float ntu = -1120.4 * U * U + 5742.3 * U - 4352.9;

    if (ntu > 0 && ntu < 4000)
        addData(0x0189, TURBIDITY, (int32_t) ntu, NTU);
    else {
        addData(0x0189, TURBIDITY, INT32_MAX, NTU);
        debugPrint((String)"    Ignoring ntu=" + ntu);
    }

#endif
}


void getMHZ19() {
#ifdef CONFIG_SUPPORT_MHZ19

    debugPrint("Querying MH-Z19...");

    int16_t co2 = myMHZ19.getCO2();
    if (co2 == 0 || myMHZ19.errorCode != RESULT_OK) {
        debugPrint("    CO2=0? trying again in 1s");
        delay(1000);
        co2 = myMHZ19.getCO2();
    }

    if (myMHZ19.errorCode == RESULT_OK && co2 > 0 && co2 < 10000)
        addData(0x0219, CO2, co2, PPM);
    else
        addData(0x0219, CO2, INT32_MAX, PPM);

#ifdef CONFIG_MHZ19_TEMP
    float temp = myMHZ19.getTemperature(false);
    if (temp > -30 && temp < 60.0) {
        temp += (float)(CONFIG_MHZ19_BIAS_MC) * 1e-3;
        addData(0x0219, TEMP, (int16_t) (temp * 100.0), CENT_DEGC);
    } else {
        addData(0x0219, TEMP, INT32_MAX, CENT_DEGC);
        debugPrint((String)"Temperature out of bounds: " + temp);
    }
#endif

#ifdef CONFIG_MHZ19_RAW
    co2 = myMHZ19.getCO2Raw();
    if (co2 > 10000 && co2 < 32000)
        addData(0x0219, CO2RAW, co2, RAW);
    else
        addData(0x0219, CO2RAW, INT32_MAX, RAW);
#endif

#ifdef CONFIG_MHZ19_BGCO2
    co2 = myMHZ19.getBackgroundCO2();
    if (co2 > 0 && co2 < 10000)
        addData(0x0219, CO2BG, co2, PPM);
    else
        addData(0x0219, CO2BG, INT32_MAX, PPM);
#endif

#endif
}


void getAJSR04M(auto * ser, uint16_t id) {
#ifdef CONFIG_SUPPORT_AJSR04M
    int sensorValue[CONFIG_AJSR04M_NRMEAS];
    uint8_t maxErrors = 10;
    float T = 25.0;

    #ifdef CONFIG_AJSR04M_IS_A022
    debugPrint((String)"Querying A022YYUW " + id + "...");
    #else
    debugPrint((String)"Querying AJ-SR04M " + id + "...");
    #endif

    // measure multiple times
    for (int8_t c=0; c<CONFIG_AJSR04M_NRMEAS; c++) {

        #if CONFIG_PIN_AJSR04M_TX != CONFIG_PIN_AJSR04M_RX
        if (id == 0x4104) {
            ser->write(0xFF);  // send any byte to start measurement
            delay(50);        // measurement takes ~60ms
        }
        #endif
        #if CONFIG_PIN_AJSR04M_TX2 != CONFIG_PIN_AJSR04M_RX2
        if (id == 0x4105) {
            ser->write(0xFF);  // send any byte to start measurement
            delay(50);        // measurement takes ~60ms
        }
        #endif

        uint8_t maxWait = 100;
        #ifndef CONFIG_AJSR04M_USE_HWSERIAL
        ser->listen();
        #endif
        while (ser->available() < 4 && --maxWait > 0) {
            delay(10);
        }
        if (maxWait == 0) {
            #ifdef CONFIG_AJSR04M_IS_A022
            debugPrint("    Error waiting for A022YYUW");
            #else
            debugPrint("    Error waiting for AJ-SR04M");
            #endif
            return;
        }

        if (ser->read() == 0xFF) {     // START
            uint8_t upper = ser->read();
            uint8_t lower = ser->read();
            uint8_t check = ser->read();

            // The only difference between AJSR04M and A022YYUW is that the
            // latter adds 0xFF (= -1) to the sum, so let's fix that
            #ifdef CONFIG_AJSR04M_IS_A022
                check++;
            #endif

            if ( ((upper + lower) & 0xFF) == check ) {
                uint16_t value = (upper << 8) | lower;
                if (value < 10000) {
                    sensorValue[c] = value;
                } else {
                    if (maxErrors-- == 0) return;
                    c--;
                }

                #ifdef CONFIG_AJSR04M_DEBUG
                debugPrint((String)"AJ-SR04M: " + sensorValue[c]);
                #endif
            } else {
                debugPrint((String)"AJ-SR04M Checksum error: " + upper +
                        " + " + lower + " != " + check);
                if (maxErrors-- == 0) return;
                c--;
            }
        }

        // empty buffer
        while (ser->available()) ser->read();
    }

    // calculate median
    bubbleSort(sensorValue, CONFIG_AJSR04M_NRMEAS);

    // as CONFIG_ADC_MEASUREMENTS is odd, we can just take the middle sample
    int raw = sensorValue[CONFIG_AJSR04M_NRMEAS/2];

    #ifdef CONFIG_AJSR04M_RAW
    addData(id, DISTANCE, raw, RAW);
    #endif

    if (raw > 10000) return;    // sensor reports 10555 in case of error

    int dist = raw;

    #ifdef CONFIG_AJSR04M_TEMPCORR
    // look in stored data for air temperature
    for (uint8_t i; i <= data.nrMeasurements; i++) {
        if (sensorMeasurements[i].sensorId == CONFIG_AJSR04M_AIRTEMP_SENSORID
         && sensorMeasurements[i].type == TEMP
         && sensorMeasurements[i].unit == CENT_DEGC) {
            T = (float) sensorMeasurements[i].value / 100.0;
        }
    }
    if (T <= 0.0 || T >= 50.0 || T == NAN) {
        debugPrint("    Ignoring wrong compensation temperature");
        T = 25.0;
    }

    // https://en.wikipedia.org/wiki/Speed_of_sound#Speed_of_sound_in_ideal_gases_and_air
    dist *= 345.0 / ( 331.3 + 0.606 * T / 100.0 );
    #endif

    #ifdef CONFIG_AJSR04M_INVERT
    dist = CONFIG_AJSR04M_OFFSET - dist;
    #else
    dist = CONFIG_AJSR04M_OFFSET + dist;
    #endif

    addData(id, DISTANCE, dist, MMETER);

#endif
}


int readADC(uint8_t pin, uint8_t nr, uint16_t sleep) {
#if defined CONFIG_SUPPORT_BATTERY || defined CONFIG_SUPPORT_ADC
    int sensorValue[nr];

    // measure multiple times
    for (byte c=0; c<nr; c++) {
        delay(sleep);
        sensorValue[c] = analogRead(pin);
#ifdef CONFIG_ADCDEBUG
        sprintf(buf, "ADC %d=%d", c, sensorValue[c]);
        debugPrint(buf);
#endif
    }

    // calculate median
    bubbleSort(sensorValue, nr);

    // as CONFIG_ADC_MEASUREMENTS is odd, we can just take the middle sample
    return sensorValue[nr/2];
#else
    return -1;
#endif
}

void getBattery() {
#ifdef CONFIG_SUPPORT_BATTERY
    int raw;
    float volt;

#ifdef CONFIG_UC_ATMEGA328
    // compare A0 voltage to internal 1.1V reference
    analogReference(INTERNAL);
#endif

    // v0.7 had a MOSFET to switch on/off the battery measuring
    // which needs pwrsens to be LOW
    #ifdef CONFIG_BATTERY_MOSFET
    digitalWrite(CONFIG_PIN_POWER_SENSORS, HIGH);
    pinMode(CONFIG_PIN_POWER_SENSORS, OUTPUT);
    delay(10);
    digitalWrite(CONFIG_PIN_POWER_SENSORS, LOW);
    #endif

    raw = readADC(CONFIG_PIN_BATTERY, CONFIG_BATTERY_MEASUREMENTS,
            CONFIG_SLEEP_BATTERY_READ);
    volt = calcBattery(raw);

    addData(0, BATTERY, (uint16_t) (volt * 1000.0), MVOLT);
    if (config.battraw)
        addData(0, BATTERY, raw, RAW);

    // If battery voltage is below 3.0V, the results from most sensors will
    // be totally off -> don't even try anymore
    if (volt < CONFIG_VLOWBAT/1000.) {
        debugPrint("LOW BATTERY");
        gotoSleep(CONFIG_LOWBAT_SLEEPSECS);
        reboot();
    }
#endif
}

void getADC() {
#ifdef CONFIG_SUPPORT_ADC
    int raw;
    float volt;

#ifdef CONFIG_UC_ATMEGA328
    // compare A0 voltage to internal 1.1V reference
    analogReference(INTERNAL);
#endif

    raw = readADC(CONFIG_PIN_ADC, CONFIG_ADC_MEASUREMENTS,
            CONFIG_SLEEP_ADC_READ);
    volt = calcADC(raw);

    addData(0x0adc, VOLTAGE, (uint16_t) (volt * 1000.0), MVOLT);
    #ifdef CONFIG_ADC_REPORT_RAW
        addData(0x0adc, VOLTAGE, raw, RAW);
    #endif

#endif
}

void getWiFi() {
#ifdef CONFIG_SUPPORT_WIFIRSSI
    if (config.ssid[0] != 0)
        addData(0, WIFIRSSI, WiFi.RSSI(), DBM);
#endif
}

void getPerf() {
#ifdef CONFIG_SUPPORT_PERF
    #ifdef CONFIG_UC_ESP8266
    // simply "measure" the number of CPU cycles since bootup
    unsigned long int cycles;
    cycles = ESP.getCycleCount();

    // After uploading a new firmware, the ESP will directly boot into that,
    // without resetting the uptime -> filter out those bogus values (>20s)
    if (cycles/F_CPU > 20)
        return;

    addData(0, TIME, cycles/(F_CPU/1000000), USEC);
    if (config.perfraw)
        addData(0, TIME, cycles, RAW);

    #else
    addData(0, TIME, micros(), USEC);
    #endif

#endif
}


void getMem() {
#ifdef CONFIG_SUPPORT_FREEMEM
    addData(0, FREEMEM, freeMemory(), BYTE);
#endif
}


void addData(unsigned int sensorId, enum sensorType type,
        int32_t value, enum unitType unit) {

    #ifdef CONFIG_HW_WATCHDOG_STM
    // we got a sensor value -- reset the watchdog
    IWatchdog.reload();
    #endif

    #ifdef CONFIG_SERIAL
    sprintf(buf, "data: %04x %11s %8ld %s",
            sensorId, sensorTypeName[type], value, unitTypeName[unit]);
    debugPrint(buf);
    #endif
    #ifdef CONFIG_SERIAL_MINIMAL
    Serial.print(millis());
    Serial.print(" id ");
    Serial.print(sensorId, 16);
    Serial.print(" type ");
    Serial.print(type);
    Serial.print(" = ");
    Serial.println(value);
    #endif

    // checkTrigger allows triggering e.g. alerts; it might return
    // false to indicate that the measurement should be ignored
    if (! checkTrigger(sensorId, type, value, unit))
        return;

    // ignore "error" values
    if (value == INT32_MAX) return;

    byte idx = data.nrMeasurements++;
    if (idx >= CONFIG_MAX_SENSORS) return;

    sensorMeasurements[idx].sensorId = sensorId;
    sensorMeasurements[idx].type = type;
    sensorMeasurements[idx].value = value;
    sensorMeasurements[idx].unit = unit;


    #if defined(CONFIG_LCD) || defined(CONFIG_EPAPER)

    #ifndef CONFIG_LCD_SHOW_PRESSURE
    if (type == PRESSURE) return;
    #endif
    #ifndef CONFIG_LCD_SHOW_GPS_SATS
    if (type == GPS_SATS) return;
    #endif
    #ifndef CONFIG_LCD_SHOW_GPS_ALT
    if (type == GPS_ALT) return;
    #endif
    #ifndef CONFIG_LCD_SHOW_BME280_TEMP
    if (sensorId == 0x1280 && type == TEMP) return;
    #endif
    #ifndef CONFIG_LCD_SHOW_BMP280_TEMP
    if (sensorId == 0x280 && type == TEMP) return;
    #endif

    // Don't print some "boring" values
    if (    (sensorId == 0      && type == TIME)
         || (sensorId == 0      && type == WIFIRSSI)
         || (sensorId == 0x0701 && type == GPS_DATE)
         || (sensorId == 0x0219 && unit == RAW)
         || (sensorId == 0x0219 && type == CO2BG)
       ) return;

        if (unit == CENT_DEGC && sensorId == 0) {
            // internal µC temperature, show with less accuracy
            sprintf(buf, "%lu C", value/100);
        }
        else if (unit == CENT_DEGC) {
            #ifdef CONFIG_LCD_TEMP_DECIMALS
            floatsprintf(buf, CONFIG_LCD_TEMP_DECIMALS, value/1e2);
            #else
            floatsprintf(buf, 2, value/1e2);
            #endif
            strcat(buf, " C");
        }
        else if (unit == PERCENT)
            sprintf(buf, "%lu%%", value);
        else if (unit == CENT_PERC) {
            floatsprintf(buf, 0, value/1e2);
            strcat(buf, "%");
        }
        else if (sensorId == 0 && type == TIME && unit == USEC) {
            floatsprintf(buf, 0, value/1e3);
            strcat(buf, "ms");
        }
        else if (unit == USEC) {
            floatsprintf(buf, 3, value/1e3);
            strcat(buf, "ms");
        }
        else if (unit == PASCAL) {
            #if defined CONFIG_PRESSURE_IN_KPA
                #ifdef CONFIG_LCD_PRESSURE_PRECISION
                    floatsprintf(buf, CONFIG_LCD_PRESSURE_PRECISION-3, value/1e3);
                #else
                    floatsprintf(buf, 1, value/1e3);
                #endif
                strcat(buf, "kPa");
            #elif defined CONFIG_PRESSURE_IN_HPA
                #ifdef CONFIG_LCD_PRESSURE_PRECISION
                    floatsprintf(buf, CONFIG_LCD_PRESSURE_PRECISION-4, value/1e2);
                #else
                    floatsprintf(buf, 0, value/1e2);
                #endif
                strcat(buf, "hPa");
            #else
                sprintf(buf, "%lu Pa", value);
            #endif

            // override all of the above with a difference to "standard"
            // pressure at-sea-level, if configured to do so
            #ifdef CONFIG_LCD_PRESSURE_SHOWASLDIFF
            if (type == PRESSUREASL) {
                sprintf(buf, "%-ld Pa", (int32_t)value - 101325);
            }
            #endif
        }
        else if (unit == DBM)
            sprintf(buf, "%ludBm", value);
        else if (unit == MVOLT) {
            floatsprintf(buf, 2, value/1e3);
            strcat(buf, "V");
        }
        else if (unit == UVOLT) {
            floatsprintf(buf, 3, value/1e6);
            strcat(buf, "V");
        }
        else if (unit == UDEG) {
            // replace degree symbol with N/E/S/W for GPS lat/lng
            char degree = 0xdf;
            if (type == GPS_LAT) {
                if (value > 0) {
                    degree = 2;
                } else if (value < 0) {
                    degree = 3;
                    value = -value;
                }
            } else if (type == GPS_LNG) {
                if (value > 0) {
                    degree = 4;
                } else if (value < 0) {
                    degree = 5;
                    value = -value;
                }
            }
            // 1udeg is ~0.1m at the equator -> ignore last digit
            floatsprintf(buf, 5, value/1e6);
            uint8_t len = strlen(buf);
            buf[len++] = degree;
            buf[len] = 0;
        }
        else if (unit == MDEG) {
            floatsprintf(buf, 1, value/1e3);
            strcat(buf, "C");
        }
        else if (unit == METER)
            sprintf(buf, "%lum", value);
        else if (unit == DMETER) {
            floatsprintf(buf, 1, value/10.);
            strcat(buf, "m");
        }
        else if (unit == SEC)
            if (type == GPS_DAYSEC) {
#ifdef CONFIG_LCD_SHOW_GPS_TIME_SECS
                uint8_t h, m, s;
                h = value / 3600; value -= 3600 * h;
                m = value /   60; value -=   60 * m;
                s = value;
                sprintf(buf, "%02d:%02d:%02d", h, m, s);
#else
                uint8_t h, m;
                h = value / 3600; value -= 3600 * h;
                m = value /   60;
                sprintf(buf, "%02d:%02d", h, m);
#endif
            } else {
                sprintf(buf, "%lus", value);
            }
        else if (unit == MKNOT) {
            floatsprintf(buf, 1,  value/1e3);
            strcat(buf, "kn");
        }
        else if (unit == MPROH) {
            floatsprintf(buf, 1,  value/1e3);
            strcat(buf, "km/h");
        }
        else if (unit == MMPROS) {
            floatsprintf(buf, 1,  value/1e3);
            strcat(buf, "m/s");
        }
        else if (unit == DAMETER) {
            floatsprintf(buf, 2,  value/1e2);
            strcat(buf, "km");
        }
        else if (unit == MMETER) {
            floatsprintf(buf, 3,  value/1e3);
            strcat(buf, "m");
        }
        else if (unit == CNMILE) {
            floatsprintf(buf, 1,  value/1e2);
            strcat(buf, "nm");
        }
        else if (type == GPS_SATS && unit == NUMBER)
            sprintf(buf, "%lu\x08", value); // 8 is same char as 0, can't use 0
        else if (unit == PPM)
            sprintf(buf, "%luppm", value);
        else if (unit == PPB && type == TDS)
            sprintf(buf, "%luppm", int(value/1e3));
        else if (unit == NTU)
            sprintf(buf, "%luntu", value);
        else if (type == DISTANCE && unit == RAW)
            sprintf(buf, "%lumm", value);
        else if (unit == BYTE)
            sprintf(buf, "%lu B", value);
        else
            sprintf(buf, "%lu", value);

    #endif

    #ifdef CONFIG_LCD
        // If the text doesn't fit in the line anymore, go to next line
        if (posx + strlen(buf) > CONFIG_LCD_WIDTH) {
            if (++posy == CONFIG_LCD_HEIGHT) posy = 0;
            posx = 0;
            // setCursor(posx = 0, posy);
        }

        memcpy(lcdpos(posx, posy), buf, strlen(buf));
        posx += strlen(buf);
        if (posx < CONFIG_LCD_WIDTH) {
            *lcdpos(posx, posy) = ' ';
            posx++;
        }
    #endif

    #ifdef CONFIG_EPAPER
        char buf2[20];
        if (sensorId == 0 && type == TEMP) {
            strcpy(buf2, "MCUtemp");
        } else if (sensorId == 0 && type == BATTERY) {
            strcpy(buf2, "Batterie");
        } else if (sensorId == 0x0adc) {
            strcpy(buf2, "Panel");
        } else if (type == PRESSUREASL) {
            strcpy(buf2, "LDrckAMH");
        } else if (type == PRESSURE) {
            strcpy(buf2, "LftDruck");
        } else if (sensorId == 0x1280 && type == HUMIDITY) {
            strcpy(buf2, "LuftFcht");
        } else if (sensorId == 0x1280 && type == TEMP) {
            strcpy(buf2, "LuftTemp");
        } else if (sensorId == 0x1820 && type == TEMP) {
            strcpy(buf2, "WasserT");
        } else if (type == FREEMEM) {
            strcpy(buf2, "Speicher");
        } else if (type == TIME) {
            strcpy(buf2, "Zeit");
        } else if (type == VOLTAGE && sensorId >= 0x1115 && sensorId < 0x1115+4) {
            sprintf(buf2, "ADC Ch%d", sensorId - 0x1115);
        } else if (type == DISTANCE && sensorId >= 0x4104 && sensorId < 0x4104+2) {
            sprintf(buf2, "Hoehe %d", sensorId - 0x4104 + 1);
        } else {
            sprintf(buf2, "%04X%s",
                    sensorId, sensorTypeName[type]);
            buf2[8] = 0;
        }

        display.setCursor(ep_posx, ep_posy-1);
        display.setFont(&Font5x7FixedMono);
        display.print(buf2);

        display.setCursor(ep_posx+11*5, ep_posy);
        display.setFont(&FreeMonoBold9pt7b);
        display.print(buf);

        ep_posy += CONFIG_EPAPER_LINEHEIGHT;
        if (ep_posy >= CONFIG_EPAPER_HEIGHT) {
            if (ep_posx == 0) {
                ep_posx = CONFIG_EPAPER_WIDTH >> 1;
            } else {
                ep_posx = 0;
            }
            ep_posy = CONFIG_EPAPER_LINEHEIGHT - 1;
        }
    #endif

}


#ifdef CONFIG_LCD
char * lcdpos(uint8_t col, uint8_t row) {
    return lcdbuf + CONFIG_LCD_WIDTH*row + col;
}
#endif

void updateLCD() {
#ifdef CONFIG_LCD

    debugPrint(lcdbuf);

    // sanity check -- buffer must not overflow
    if (lcdbuf[CONFIG_LCD_WIDTH*CONFIG_LCD_HEIGHT] != 0) {
        lcd.print("Buffer Overflow");
        return;
    }

    // Show anchorage statuus
    #ifdef CONFIG_SUPPORT_PCF8574
    if (anchorageWatch) {
        *lcdpos(CONFIG_LCD_WIDTH-2, CONFIG_LCD_HEIGHT-1) = 0xce;
    }
    if (logEnabled) {
        *lcdpos(CONFIG_LCD_WIDTH-1, CONFIG_LCD_HEIGHT-1) = '>';
    }
    #endif

    char printline[CONFIG_LCD_WIDTH+1];
    printline[CONFIG_LCD_WIDTH] = 0;
    for (uint8_t l=0; l < CONFIG_LCD_HEIGHT; l++) {
        debugPrint((String)"LCDisplaying line " + l);
        memcpy(printline, lcdbuf + CONFIG_LCD_WIDTH * l, CONFIG_LCD_WIDTH);
        debugPrint((String)"    '" + printline + "'");
        setCursor(0, l);
        lcd.print(printline);
    }

#endif
}


void updateNeopixel() {
#ifdef CONFIG_NEOPIXEL
    ws2812fx.service();
#endif
}


void sendData() {
#ifdef CONFIG_MQTT
    char payloadBuffer[16];
    char topicBuffer[128];

    if (config.ssid[0] == 0) return;

    debugPrint("Sending data");

    #ifdef CONFIG_CONTINUOUS
        // check that we are still connected to the MQTT server
        if (! mqttClient.connected()) {
            #ifdef CONFIG_SERIAL
            debugPrint((String)"Need to reconnect to MQTT, current state=" +
                    mqttClient.state());
            #endif
            #ifdef CONFIG_MQTT_USEAUTH
            mqttClient.connect(idBuffer, CONFIG_MQTT_USER, CONFIG_MQTT_PASS);
            #else
            mqttClient.connect(idBuffer);
            #endif
        }
    #endif

    for (byte i = 0; i < data.nrMeasurements; i++) {
        snprintf(topicBuffer, 128,
                "chip-%08lx/sensor-%d/%s-%s",
                data.chipId, data.sensorMeasurements[i].sensorId,
                sensorTypeName[data.sensorMeasurements[i].type],
                unitTypeName[data.sensorMeasurements[i].unit]);

        snprintf(payloadBuffer, 16,
                "%d", data.sensorMeasurements[i].value);

        mqttClient.publish(topicBuffer, payloadBuffer);
    }
#endif
}

void mqttSend() {
#ifdef CONFIG_MQTT
    if (config.ssid[0] != 0)
        mqttClient.loop();
#endif
}


void logData() {
#ifdef CONFIG_OPENLOG
    char mybuf[128];

    debugPrint("Logging data to OpenLog");

    for (byte i = 0; i < data.nrMeasurements; i++) {
        if (i) olSer.print(", ");

        snprintf(mybuf, 128,
                "%04x-%s-%s: %ld",
                data.sensorMeasurements[i].sensorId,
                sensorTypeName[data.sensorMeasurements[i].type],
                unitTypeName[data.sensorMeasurements[i].unit],
                data.sensorMeasurements[i].value
                );
        olSer.print(mybuf);
    }
    olSer.print("\r\n");
#endif

#ifdef CONFIG_LORA
    debugPrint("Logging data via LoRa");

#ifdef CONFIG_PIN_LORA_M1
    if (CONFIG_PIN_LORA_M1 >= 0) {
        pinMode(CONFIG_PIN_LORA_M1, OUTPUT);
        digitalWrite(CONFIG_PIN_LORA_M1, LOW);
        delay(10);   // allow the module to wake up
    }
#endif

    #ifdef CONFIG_LORA_BIN
    for (byte repeat = 0; repeat < CONFIG_LORA_REPEAT+1; repeat++) {
        if (repeat > 0) {
            loraSer.flush();
            // when the LoRa module does not receive data for "3 byte time"
            // (24 bit / 2.4kbps = 10ms), it sends the previous data
            delay(20);
        }
        char loraBuf[9+8*data.nrMeasurements];
        uint16_t bufPos = 0;

        loraBuf[bufPos++] = 0xc0;                      // start pattern
        loraBuf[bufPos++] = 0xfe;
        memcpy(loraBuf+bufPos, &data.chipId, 4);       // chip id
        bufPos += 4;
        loraBuf[bufPos++] = data.nrMeasurements;       // nr of measurements

        for (byte i = 0; i < data.nrMeasurements; i++) {
            memcpy(loraBuf+bufPos, &data.sensorMeasurements[i].sensorId, 2);
            bufPos += 2;
            loraBuf[bufPos++] = data.sensorMeasurements[i].type;
            loraBuf[bufPos++] = data.sensorMeasurements[i].unit;
            memcpy(loraBuf+bufPos, &data.sensorMeasurements[i].value, 4);
            bufPos += 4;
        }

        loraBuf[bufPos++] = 0xbe;                       // end pattern
        loraBuf[bufPos++] = 0xad;

        loraSer.write(loraBuf, bufPos);
    }

    #else
    for (byte i = 0; i < data.nrMeasurements; i++) {
        snprintf(buf, 64,
                "%08lx-%04x-%s-%s\t%ld\r\n",
                data.chipId,
                data.sensorMeasurements[i].sensorId,
                sensorTypeName[data.sensorMeasurements[i].type],
                unitTypeName[data.sensorMeasurements[i].unit],
                data.sensorMeasurements[i].value
                );
        loraSer.print(buf);
        delay(8*strlen(buf)*10/24); // 1000 ms/s / 2400 b/s
    }
    #endif // CONFIG_LORA_BIN

    loraSer.flush();

#ifdef CONFIG_PIN_LORA_M1
    if (CONFIG_PIN_LORA_M1 >= 0) {
        digitalWrite(CONFIG_PIN_LORA_M1, HIGH);
        pinMode(CONFIG_PIN_LORA_M1, INPUT);
    }
#endif

#endif
}


void powerSensors(bool on) {
    static bool oldState = false;
    // If there is no dedicated pin to power the sensors, we don't need to
    // switch anything :)
    if (CONFIG_PIN_POWER_SENSORS < 0)
        return;

    if (on == oldState) return;

    // when switching "off", ensure that the pin is not connected to GND or
    // Vcc anymore by changing it to INPUT
    pinMode(CONFIG_PIN_POWER_SENSORS, on ? OUTPUT : INPUT);
    // when pinMode is INPUT, writing LOW will disable the internal pullup
    digitalWrite(CONFIG_PIN_POWER_SENSORS, on ? HIGH : LOW);
    debugPrint((String)"Switched sensor-power " + ( on ? "ON" : "OFF" ));

    oldState = on;
}


#ifdef CONFIG_SUPPORT_BATTERY
float calcBattery(int raw) {
    /*
       V = Vbatt * battDivider
       raw/1024 = V - Voff

       Vbatt = V / battDivider

       battDivider = R2 / (R1 + R2)
     */

    return (
#ifdef CONFIG_UC_ATMEGA328
            // ATmega will report 1023 for 1.1V, as we have set the reference
            // INTERNAL
            1.1*
#elif CONFIG_UC_STM32F103
            // STM will report 4095 (4 times (2 bit more) than others) for 3.3V
            3.3/4*
#endif
            raw/1024. + CONFIG_BATTERY_VOFF_MV/1000.) / (
                (
                 (float)CONFIG_BATTERY_DIVIDER_R2 /
                 (CONFIG_BATTERY_DIVIDER_R1 + CONFIG_BATTERY_DIVIDER_R2)
                )
                );
}
#endif

#ifdef CONFIG_SUPPORT_ADC
float calcADC(int raw) {
    return (
#ifdef CONFIG_UC_ATMEGA328
            // ATmega will report 1023 for 1.1V, as we have set the reference
            // INTERNAL
            1.1*
#elif CONFIG_UC_STM32F103
            // STM will report 4095 (4 times (2 bit more) than others) for 3.3V
            3.3/4*
#endif
            raw/1024. + CONFIG_ADC_VOFF_MV/1000.) / (
                (
                 (float)CONFIG_ADC_DIVIDER_R2 /
                 (CONFIG_ADC_DIVIDER_R1 + CONFIG_ADC_DIVIDER_R2)
                )
                );
}
#endif

void bubbleSort(int * arr, int n) {
    bool swapped = true;
    int j = 0;
    int tmp;

    while (swapped) {
        swapped = false;
        j++;
        for (int i = 0; i < n - j; i++) {
            if (arr[i] > arr[i + 1]) {
                tmp = arr[i];
                arr[i] = arr[i + 1];
                arr[i + 1] = tmp;
                swapped = true;
            }
        }
    }
}

void floatsprintf(char * buf, uint8_t decimals, float value) {
    // memory-limited platforms (ATmega) don't have support for %f in sprintf

#if defined(CONFIG_UC_ESP8266) || defined(CONFIG_UC_STM32F103)
    // ESP8266 and STM32 do have it, so let's use it
    char mybuf[] = "%.0f";
    mybuf[2] = '0'+decimals;
    sprintf(buf, mybuf, value);
#elif CONFIG_UC_ATMEGA328
    // The avr_stdlib provides dtostrf()
    dtostrf(value, 0, decimals, buf);
#endif
}

float sealevelPressure(float pressure) {
    // calculate the pressure at sea level, given the measured pressure
    // and the height it was measured at
    float slp;

    slp = pressure / (
            exp( log(1 - 1.0*config.heightASL/44330) / 0.1903 )
            );
    return slp;
}

#ifdef CONFIG_LCD
void setCursor(uint8_t col, uint8_t row) {
    /*
       workaround for 1604 displays

       2004 memory layout (in 4-byte blocks):
       0000022222......1111133333......
       1604 memory layout (in 4-byte blocks):
       00002222........11113333........

       but the library always assumes the former!
     */

#if CONFIG_LCD_WIDTH == 16 && CONFIG_LCD_HEIGHT == 4
    if (row == 2)
        lcd.setCursor(16 + col, 0);
    else if (row == 3)
        lcd.setCursor(16 + col, 1);
    else
        lcd.setCursor(col, row);
#else
    lcd.setCursor(col, row);
#endif
}
#endif


void gotoSleep(unsigned int seconds) {

    setBlueLed(false);
    debugPrint((String)"Going to sleep at millis=" + millis() + '\n');

    #ifdef CONFIG_EPAPER
    display.nextPage();
    #endif

    #ifdef CONFIG_HW_WATCHDOG_STM
    IWatchdog.reload();
    #endif

#ifdef CONFIG_CONTINUOUS
    // Even while "sleeping" we need to take care of some stuff

    #ifdef CONFIG_SUPPORT_GPS
    swSer.listen();
    #endif

    for (uint16_t i = 0; i < seconds*50; i++) {
        delay(20);
        #ifdef CONFIG_HW_WATCHDOG_STM
        IWatchdog.reload();
        #endif
        #ifdef CONFIG_SUPPORT_GPS
        while (swSer.available() > 0) {
            char c = swSer.read();
            // Serial.write(c); // copy NMEA to serial debug port
            nmea.process(c);
            yield();
        }
        #endif
        #ifdef CONFIG_SUPPORT_PCF8574
        checkButtons();
        #endif
        updateNeopixel();
    }

#else
    #ifdef CONFIG_MQTT
    mqttClient.disconnect();
    delay(10);
    #endif

    #ifdef CONFIG_EPAPER
    display.hibernate();
    delay(10);
    #endif

    #ifdef CONFIG_PIN_POWERDOWN
    if (CONFIG_PIN_POWERDOWN >= 0) {
        digitalWrite(CONFIG_PIN_POWERDOWN, HIGH);
        delay(100);
        // we should never reach this point; if we do, alert and reboot after
        // some time
        debugPrint("ALERT: power-down via Done-pin did not work!");
        delay(30000);
        reboot();
    }
    #endif

#ifdef CONFIG_UC_ESP8266
    // go to sleep, reboot after 'seconds' seconds
    #ifdef CONFIG_SUPPORT_WIFI
    // WAKE_RF_DEFAULT, WAKE_RFCAL, WAKE_NO_RFCAL, WAKE_RF_DISABLED
    if (config.ssid[0] != 0)
        ESP.deepSleep(1e6 * seconds, WAKE_NO_RFCAL);
    else
    #endif
        // Using WAKE_RF_DISABLED breaks the ADC!
        #ifdef CONFIG_SUPPORT_BATTERY
        ESP.deepSleep(1e6 * seconds, WAKE_NO_RFCAL);
        #else
        ESP.deepSleep(1e6 * seconds, WAKE_RF_DISABLED);
        #endif

#elif CONFIG_UC_ATMEGA328
    // switch off unnecessary units
    power_adc_disable();
    power_spi_disable();
    power_twi_disable();
    #ifdef CONFIG_SERIAL
    // wait a bit for the UART to flush its buffer (64 byte?)
    delay(1e3*64*10/CONFIG_BAUD);
    #endif
    power_usart0_disable();
    //power_timer0_disable(); // NECESSARY for delay() and millis()
    power_timer1_disable();
    power_timer2_disable();

    clock_prescale_set(clock_div_256);
    delay((1e3 * seconds - micros()/1000) / 256);
    clock_prescale_set(clock_div_1);

    // Simply rebooting (with a JMP 0) would not reactivate the various
    // peripherals, so we instead fix the timers and re-enable all peripherals,
    // and then simply jump back into the loop

    // The slowed down timer0 will now be totally off -- let's correct it
    extern volatile unsigned long timer0_millis, timer0_overflow_count;
    timer0_millis += 1e3 * seconds - (1e3 * seconds / 256);

    // reset micros() (used for getPerf()) (see
    // hardware/arduino/cores/arduino/wiring.h for how/why this works)
#if defined(TCNT0)
	TCNT0 = 0;
#elif defined(TCNT0L)
	TCNT0L = 0;
#endif
    timer0_overflow_count = 0;

    power_timer2_enable();
    power_timer1_enable();
    power_usart0_enable();
    power_twi_enable();
    power_spi_enable();
    power_adc_enable();

#elif CONFIG_UC_STM32F103

    LowPower.shutdown((uint32_t)(1000 * seconds));
    // this should never be reached, but let's make sure...
    reboot();

#endif
#endif
}

void reboot() {
#ifdef CONFIG_UC_ESP8266
    // taken from
    // https://github.com/esp8266/Arduino/issues/1722#issuecomment-321818357
#ifdef CONFIG_SUPPORT_WIFI
    WiFi.forceSleepBegin();
#endif
    wdt_reset();
    ESP.restart();
    while (true)
        wdt_reset();
#elif CONFIG_UC_ATMEGA328
    asm volatile ("jmp 0");
#elif CONFIG_UC_STM32F103
     NVIC_SystemReset();
#endif
}


void checkButtons() {
#ifdef CONFIG_SUPPORT_PCF8574
    long now = millis();

    #ifdef CONFIG_SUPPORT_GPS
    if (CONFIG_PCF8574_BTN_ANCHOR != -1) {
        bool anchor = pcf8574.digitalRead(CONFIG_PCF8574_BTN_ANCHOR);
        if (anchor != buttonState[CONFIG_PCF8574_BTN_ANCHOR]) {
            debugPrint((String)"Anchor button changed to " + anchor);

            // Ignore change if last change was less than 20ms ago
            if (now - lastChangeTime[CONFIG_PCF8574_BTN_ANCHOR] > 20) {

                if (anchor != 0) {
                    // Button has been released
                    debugPrint((String)"Anchor button pressed for " + (now -
                                lastChangeTime[CONFIG_PCF8574_BTN_ANCHOR]) + " ms");
                    if (anchorageWatch) {
                        #ifdef CONFIG_LCD
                        lcd.clear();
                        lcd.print("Anchorage Watch");
                        lcd.setCursor(1, 1);
                        #endif
                        if (now - lastChangeTime[CONFIG_PCF8574_BTN_ANCHOR] > 1300) {
                            anchorageWatch = false;
                            #ifdef CONFIG_LCD
                            lcd.print("DISABLED");
                            #endif
                        } else {
                            if (nmea.isValid()) {
                                anchorageLat = nmea.getLatitude();
                                anchorageLon = nmea.getLongitude();
                                #ifdef CONFIG_LCD
                                lcd.print("RESET");
                                #endif
                            } else {
                                #ifdef CONFIG_LCD
                                lcd.print("ERROR: No GPS fix!");
                                #endif
                            }
                        }
                        #ifdef CONFIG_LCD
                        delay(500);
                        #endif

                    } else {
                        if (now - lastChangeTime[CONFIG_PCF8574_BTN_ANCHOR] > 1300) {
                            #ifdef CONFIG_LCD
                            lcd.clear();
                            lcd.print("Anchorage Watch");
                            lcd.setCursor(1, 1);
                            #endif
                            if (nmea.isValid()) {
                                anchorageWatch = true;
                                #ifdef CONFIG_LCD
                                lcd.print("ENABLED");
                                #endif
                                anchorageLat = nmea.getLatitude();
                                anchorageLon = nmea.getLongitude();
                                alert(true); delay(100); alert(false);
                            } else {
                                #ifdef CONFIG_LCD
                                lcd.print("ERROR: No GPS fix!");
                                #endif
                            }
                            #ifdef CONFIG_LCD
                            delay(500);
                            #endif

                        } // endif (pressed for >2s)
                    }
                } // endif (pressed/released)

                alert(false);
                buttonState[CONFIG_PCF8574_BTN_ANCHOR] = anchor;
                lastChangeTime[CONFIG_PCF8574_BTN_ANCHOR] = now;
            }

        }
    }
    #endif // GPS support?

    if (CONFIG_PCF8574_BTN_LOG != -1) {
        bool logbut = pcf8574.digitalRead(CONFIG_PCF8574_BTN_LOG);
        if (logbut != buttonState[CONFIG_PCF8574_BTN_LOG]) {
            debugPrint((String)"Log button changed to " + logbut);

            if (logbut == 0) {
                // Button has been pressed
                lastChangeTime[CONFIG_PCF8574_BTN_LOG] = now;
            } else {
                // Button has been released
                debugPrint((String)"Log button pressed for " + (now -
                            lastChangeTime[CONFIG_PCF8574_BTN_LOG]) + " ms");
                if (logEnabled) {
                    if (now - lastChangeTime[CONFIG_PCF8574_BTN_LOG] > 1300) {
                        logEnabled = false;
                        #ifdef CONFIG_LCD
                        lcd.clear();
                        lcd.print("Logging");
                        lcd.setCursor(1, 1);
                        lcd.print("DISABLED");
                        delay(300);
                        #endif
                        #ifdef CONFIG_OPENLOG
                        // Initialize card, to ensure that we open a new file
                        // next time
                        olSer.print("\x1a\x1a\x1a");    // 3 times ^Z switches to command mode
                        delay(10);
                        olSer.print("reset\r\n");
                        delay(200);
                        #endif
                    } else {
                        // short press: force logging
                        lastlogtime = 0;
                    }

                } else {
                    if (now - lastChangeTime[CONFIG_PCF8574_BTN_LOG] > 1300) {
                        logEnabled = true;
                        #ifdef CONFIG_SUPPORT_GPS
                        gpsDistance = 0;
                        #endif
                        #ifdef CONFIG_LCD
                        lcd.clear();
                        lcd.print("Logging");
                        lcd.setCursor(1, 1);
                        lcd.print("ENABLED");
                        #endif
                        alert(true); delay(100); alert(false);
                        delay(500);
                    } // endif (pressed for >2s)
                }
            } // endif (pressed/released)

            alert(false);
            buttonState[CONFIG_PCF8574_BTN_LOG] = logbut;
        }
    }
#endif
}


void alert(bool on) {
#ifdef CONFIG_SUPPORT_PCF8574
    debugPrint((String)"Alert is now " + (on ? "ON" : "OFF") );
    if (CONFIG_PCF8574_OUT_BUZZER != -1) {
        pcf8574.digitalWrite(CONFIG_PCF8574_OUT_BUZZER, on);
    }
#endif
}


void setBlueLed(bool on) {
    // switch blue LED on/off

    if (CONFIG_PIN_BLUE < 0) return;

    debugPrint((String)"Switching blue LED " + (on ? "ON" : "OFF") );

    pinMode(CONFIG_PIN_BLUE, OUTPUT);

#ifdef CONFIG_INV_BLUE
    on = ! on;
#endif

    digitalWrite(CONFIG_PIN_BLUE, on ? HIGH : LOW);
}


void setupWebserver() {
#ifdef CONFIG_SUPPORT_WIFI
    char host[32];

    unsigned long int chipId = ESP.getChipId();
    sprintf(host, "chip-%08lx", chipId);

    WiFi.mode(WIFI_AP);
    char passwd[] = "bapotesta";
    WiFi.softAP(host, passwd);

    MDNS.begin(host);

    httpUpdater.setup(&httpServer);
    httpServer.on("/",  HTTP_GET, &webForm);
    httpServer.on("/style.css",  HTTP_GET, &webCSS);
    httpServer.on("/config",  HTTP_POST, &storeConfig);
    httpServer.on("/reset", HTTP_GET, &webReset);
    httpServer.on("/i2cscan", HTTP_GET, &webI2Cscan);
    httpServer.begin();

    MDNS.addService("http", "tcp", 80);

    #ifdef CONFIG_SERIAL
    Serial.printf("Ready! Connect to SSID %s with password '%s'"
            " and visit http://192.168.4.1\n", host, passwd);
    #endif

    #ifdef CONFIG_LCD
    Wire.begin(CONFIG_PIN_I2C_SDA, CONFIG_PIN_I2C_SCL);
    lcd.init();
    lcd.backlight();
    lcd.home();
    lcd.print("* Config Mode *");
    setCursor(0,1);
    lcd.print("* 192.168.4.1 *");
    #endif

#endif
}

#ifdef CONFIG_SUPPORT_WIFI

String ipToString (IPAddress ip) {
    const String delim = ".";
    return String(
            (String)ip[0] + delim + ip[1] + delim + ip[2] + delim + ip[3]
            );
}

IPAddress stringToIP (String text) {
    byte octet[4];
    byte pos = 0;

    for (byte i = 0; i < 4; i++) {
        byte dot = text.indexOf('.', pos);
        octet[i] = text.substring(pos, dot).toInt();
        pos = dot + 1;
    }

    return IPAddress(octet[0], octet[1], octet[2], octet[3]);
}

void webForm() {
    // called when client does a  GET /
    String buf;
    char buf2[16];

    buf = indexPage;
    buf.replace("${flashsize}", String(ESP.getFlashChipRealSize()/1024));
    buf.replace("${buildtime}", String(__DATE__ + String(" at ") + __TIME__));

    buf.replace("${ssid}", String(config.ssid));
    buf.replace("${password}", String(config.password));
    buf.replace("${ip}", ipToString(config.ip));
    buf.replace("${netmask}", String(config.netmask));
    buf.replace("${gw}", ipToString(config.gw));
    buf.replace("${mqttip}", ipToString(config.mqttip));
    buf.replace("${mqttport}", String(config.mqttport));

    buf.replace("${usedallas}", config.usedallas ? "checked" : "");
    buf.replace("${dallasres}", String(config.dallasres));
    buf.replace("${biasdallastemp}", String(config.biasDallasTemp));
    buf.replace("${dallaswait}", config.dallaswait ? "checked" : "");

    buf.replace("${usedht}", config.usedht ? "checked" : "");
    buf.replace("${dhttype}", String(config.dhttype));
    buf.replace("${biasdhttemp}", String(config.biasDHTTemp));
    buf.replace("${biasdhthumid}", String(config.biasDHTHumid));
    buf.replace("${dhthi}", config.dhthi ? "checked" : "");

    buf.replace("${usebmp280}", config.usebmp280 ? "checked" : "");
    sprintf(buf2, "0x%02x", config.bmp280addr);
    buf.replace("${bmp280addr}", buf2);
    buf.replace("${bmp280press}", config.bmp280press ? "checked" : "");
    buf.replace("${bmp280slp}", config.bmp280slp ? "checked" : "");

    buf.replace("${usebme280}", config.usebme280 ? "checked" : "");
    sprintf(buf2, "0x%02x", config.bme280addr);
    buf.replace("${bme280addr}", buf2);
    buf.replace("${bme280press}", config.bme280press ? "checked" : "");
    buf.replace("${bme280slp}", config.bme280slp ? "checked" : "");
    buf.replace("${bme280humi}", config.bme280humi ? "checked" : "");

    buf.replace("${usetsl2561}", config.usetsl2561 ? "checked" : "");
    sprintf(buf2, "0x%02x", config.tsl2561addr);
    buf.replace("${tsl2561addr}", buf2);
    buf.replace("${tsl2561inttime}", String(config.tsl2561inttime));
    buf.replace("${tsl2561raw}", config.tsl2561raw ? "checked" : "");

    buf.replace("${battery}", config.battery ? "checked" : "");
    buf.replace("${battraw}", config.battraw ? "checked" : "");

    buf.replace("${dowifi}", config.dowifi ? "checked" : "");

    buf.replace("${doperf}", config.doperf ? "checked" : "");
    buf.replace("${perfraw}", config.perfraw ? "checked" : "");

    buf.replace("${deltat}", String(config.deltat));
    buf.replace("${heightasl}", String(config.heightASL));

    httpServer.send(200, "text/html", buf);
}

void webCSS() {
    httpServer.send(200, "text/css", css);
}

void getConfig() {
    // read config from EEPROM

    // check if the first byte is "magic" (i.e. EEPROM has been written before)
    if (EEPROM.read(0) == 0x42) {
        debugPrint("EEPROM magic byte 0x42 found.");
        // check version of config storage
        if (EEPROM.read(1) == config.cfgversion) {
            // read config
            EEPROM.get(1, config);

            // conversions

            // convert netmask "/25" -> "255.255.255.128"
            // [XXX] there must be an easier way to do this
            if (config.netmask < 8+1) {
                IPSubnet = { (uint8_t)(0xff - (0xff >> config.netmask)),
                    0, 0, 0};
            } else if (config.netmask < 16+1) {
                IPSubnet = { 0xff,
                    (uint8_t)(0xff - (0xff >> (config.netmask-8))), 0, 0};
            } else if (config.netmask < 24+1) {
                IPSubnet = { 0xff, 0xff,
                    (uint8_t)(0xff - ((uint8_t)0xff >> (config.netmask-16))), 0};
            } else {
                IPSubnet = { 0xff, 0xff, 0xff,
                    (uint8_t)(0xff - ((uint8_t)0xff >> (config.netmask-24)))};
            }

        } else {
            debugPrint("Ignoring EEPROM, wrong cfg version");
        }
    } else {
        // otherwise we rely on the defaults
        debugPrint("No config found, using hardcoded defaults.");
    }
}

void storeConfig() {
    String buf = "";

    debugPrint("Received form data");

    buf += (String)"Number of received args: " + httpServer.args() + "\n";
    for (int i = 0; i < httpServer.args(); i++) {
        buf += httpServer.argName(i) + ": " + httpServer.arg(i) + "\n";
    }

    if (httpServer.hasArg("ssid"))
        httpServer.arg("ssid").toCharArray(config.ssid, 32);

    if (httpServer.hasArg("password"))
        httpServer.arg("password").toCharArray(config.password, 32);

    if (httpServer.hasArg("ip"))
        config.ip = stringToIP(httpServer.arg("ip"));

    if (httpServer.hasArg("netmask"))
        config.netmask = httpServer.arg("netmask").toInt();

    if (httpServer.hasArg("gw"))
        config.gw = stringToIP(httpServer.arg("gw"));

    if (httpServer.hasArg("mqttip"))
        config.mqttip = stringToIP(httpServer.arg("mqttip"));

    if (httpServer.hasArg("mqttport"))
        config.mqttport = httpServer.arg("mqttport").toInt();

    if (httpServer.hasArg("usedallas"))
        config.usedallas = true;
    else
        config.usedallas = false;

    if (httpServer.hasArg("dallasres"))
        config.dallasres = httpServer.arg("dallasres").toInt();

    if (httpServer.hasArg("dallaswait"))
        config.dallaswait = true;
    else
        config.dallaswait = false;

    if (httpServer.hasArg("usedht"))
        config.usedht = true;
    else
        config.usedht = false;

    if (httpServer.hasArg("dhttype"))
        config.dhttype = httpServer.arg("dhttype").toInt();

    if (httpServer.hasArg("dhthi"))
        config.dhthi = true;
    else
        config.dhthi = false;


    config.usebmp280 = httpServer.hasArg("usebmp280") ? true : false;

    if (httpServer.hasArg("bmp280addr"))
        // strtol() will automatically handle "0x" prefix
        config.bmp280addr = strtol(httpServer.arg("bmp280addr").c_str(), NULL, 0);

    config.bmp280press = httpServer.hasArg("bmp280press") ? true : false;

    config.bmp280slp = httpServer.hasArg("bmp280slp") ? true : false;


    config.usebme280 = httpServer.hasArg("usebme280") ? true : false;

    if (httpServer.hasArg("bme280addr"))
        // strtol() will automatically handle "0x" prefix
        config.bme280addr = strtol(httpServer.arg("bme280addr").c_str(), NULL, 0);

    config.bme280press = httpServer.hasArg("bme280press") ? true : false;

    config.bme280slp = httpServer.hasArg("bme280slp") ? true : false;

    config.bme280humi = httpServer.hasArg("bme280humi") ? true : false;


    config.usetsl2561 = httpServer.hasArg("usetsl2561") ? true : false;

    if (httpServer.hasArg("tsl2561addr"))
        // strtol() will automatically handle "0x" prefix
        config.tsl2561addr = strtol(httpServer.arg("tsl2561addr").c_str(), NULL, 0);

    if (httpServer.hasArg("tsl2561inttime"))
        config.tsl2561inttime = httpServer.arg("tsl2561inttime").toInt();

    config.tsl2561raw = httpServer.hasArg("tsl2561raw") ? true : false;


    if (httpServer.hasArg("heightasl"))
        config.heightASL = httpServer.arg("heightasl").toInt();

    if (httpServer.hasArg("battery"))
        config.battery = true;
    else
        config.battery = false;

    if (httpServer.hasArg("battraw"))
        config.battraw = true;
    else
        config.battraw = false;

    if (httpServer.hasArg("dowifi"))
        config.dowifi = true;
    else
        config.dowifi = false;

    if (httpServer.hasArg("doperf"))
        config.doperf = true;
    else
        config.doperf = false;

    if (httpServer.hasArg("perfraw"))
        config.perfraw = true;
    else
        config.perfraw = false;

    if (httpServer.hasArg("deltat"))
        config.deltat = httpServer.arg("deltat").toInt();

    debugPrint("Saving form data");
    EEPROM.put(0, 0x42);        // magic byte
    EEPROM.put(1, config);
    EEPROM.commit();            // ESP8266 EEPROM library needs this

    httpServer.send(200, "text/plain", buf);
}

void webReset() {
    // reset config by invalidating EEPROM
    debugPrint("Erasing config");
    EEPROM.put(0, 0x00);
    EEPROM.commit();            // ESP8266 EEPROM library needs this

    httpServer.send(200, "text/plain", "Config erased. Reset required!");
}

void webI2Cscan() {
    String output = "";
    I2Cscan(output);
    httpServer.send(200, "text/plain", output);
}

#endif


void I2Cscan(String &output) {
    // Code taken from
    // https://github.com/RobTillaart/Arduino/tree/master/sketches/MultiSpeedI2CScanner
    //
    // Scan I2C bus and put result into "output"

    const long speed[] = {
        50, 100, 200, 400, 800
    };
    int speeds = 5;

    // activate power to sensors
    if (CONFIG_PIN_POWER_SENSORS >= 0) {
        pinMode(CONFIG_PIN_POWER_SENSORS, OUTPUT);
        // when pinMode is INPUT, writing LOW will disable the internal pullup
        digitalWrite(CONFIG_PIN_POWER_SENSORS, HIGH);
    }
    // wait a bit for all sensors to "boot"
    delay(20);

    output = "\r\n";

    output += "HEX        kHz:";
    for (uint8_t s = 0; s < speeds; s++) {
        output += "\t" + String(speed[s], DEC);
    }
    output += "\r\n";

    for (uint8_t s = 0; s < speeds + 2; s++)
    {
        output += "--------";
    }
    output += "\r\n";

    #ifdef CONFIG_UC_ESP8266
    Wire.begin(CONFIG_PIN_I2C_SDA, CONFIG_PIN_I2C_SCL);
    #elif CONFIG_UC_ATMEGA328
    Wire.begin();
    #elif CONFIG_UC_STM32F103
    Wire.setSCL(CONFIG_PIN_I2C_SCL);
    Wire.setSDA(CONFIG_PIN_I2C_SDA);
    Wire.begin();
    #endif

    for (uint8_t address = 0x00; address < 0x80; address++) {
        bool found[speeds];
        bool fnd = false;

        for (uint8_t s = 0; s < speeds ; s++) {
            Wire.setClock(speed[s] * 1000);
            Wire.beginTransmission(address);
            if (Wire.endTransmission() == 0) {
                found[s] = true;
                fnd = true;
                delay(5);
            } else {
                found[s] = false;
            }
        }

        if (fnd) {
            output += "0x";
            if (address < 0x10) output += "0";
            output += String(address, HEX);

            output += "\t";

            for (uint8_t s = 0; s < speeds ; s++) {
                output += "\t ";
                output += found[s] ? "V" : ".";
            }
            output += "\r\n";
        }
    }

    Wire.setClock(100000);
}


void debugI2Cscan() {
    #ifdef CONFIG_SERIAL
    Serial.println("Starting I2C-scan; if this crashes, check PIN_I2C_SCL and PIN_I2C_SDA configuration");
    // Run I2Cscan and print results to Serial
    String output = "";
    I2Cscan(output);
    Serial.print("\r\n***** I2C scan *****");
    Serial.println(output);
    Serial.println("***** End of I2C scan *****\n");
    #endif
}

/* helper function for getMem() */
#ifndef CONFIG_UC_ESP8266
#ifdef __arm__
// should use uinstd.h to define sbrk but Due causes a conflict
extern "C" char* sbrk(int incr);
#else  // __ARM__
extern char *__brkval;
#endif  // __arm__
#endif  // ESP8266

int freeMemory() {
    #ifdef CONFIG_UC_ESP8266
        return ESP.getFreeHeap();
    #else
        char top;
        #ifdef __arm__
            return &top - reinterpret_cast<char*>(sbrk(0));
        #elif defined(CORE_TEENSY) || (ARDUINO > 103 && ARDUINO != 151)
            return &top - __brkval;
        #else  // __arm__
            return __brkval ? &top - __brkval : &top - __malloc_heap_start;
        #endif  // __arm__
    #endif
}

// vim: sw=4:expandtab:ts=4:tw=80
