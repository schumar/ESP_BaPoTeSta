/*
    Will be called for each incoming value, e.g.
        id    = 0x0280
        type  = TEMP
        value = 2350
        unit  = CENT_DEGC

    Can be used to trigger actions (e.g. an alert)

    Returning false will discard the measurement
*/
bool checkTrigger(unsigned int id, enum sensorType type,
        int32_t value, enum unitType unit) {

    #ifdef CONFIG_NEOPIXEL_CO2AMPEL
    if (type == CO2 && unit == PPM) {
        if (value == 0 || value == INT32_MAX) {
            // error
            setNeoPixel(252);
            #ifdef CONFIG_LCD
            memcpy(lcdpos(CONFIG_LCD_WIDTH/2-3, CONFIG_LCD_HEIGHT-1), "Warten", 6);
            #endif

        } else if (value < CONFIG_CO2AMPEL_CLEAN) {
            setNeoPixel(253);
            #ifdef CONFIG_LCD
            memcpy(lcdpos(CONFIG_LCD_WIDTH/2-3, CONFIG_LCD_HEIGHT-1), "FRISCH", 6);
            #endif

        } else if (value < CONFIG_CO2AMPEL_OK) {
            uint8_t yellows = (uint16_t)(value-CONFIG_CO2AMPEL_CLEAN)   // how many ppm above "clean"
                * CONFIG_NEOPIXEL_LEDS / (CONFIG_CO2AMPEL_OK - CONFIG_CO2AMPEL_CLEAN);
            setNeoPixel(yellows);
            #ifdef CONFIG_LCD
            memcpy(lcdpos(CONFIG_LCD_WIDTH/2-1, CONFIG_LCD_HEIGHT-1), "OK", 2);
            #endif

        } else if (value < CONFIG_CO2AMPEL_WARN) {
            setNeoPixel(254);
            #ifdef CONFIG_LCD
            memcpy(lcdpos(CONFIG_LCD_WIDTH/2-4, CONFIG_LCD_HEIGHT-1), "Lueften", 7);
            #endif

        } else {
            setNeoPixel(255);
            #ifdef CONFIG_LCD
            memcpy(lcdpos(CONFIG_LCD_WIDTH/2-7, CONFIG_LCD_HEIGHT-1), "JETZT LUEFTEN", 13);
            #endif
        }

    }
    #endif

    return true;
}


void setNeoPixel(uint8_t state) {
#ifdef CONFIG_NEOPIXEL
    static uint8_t oldstate = 252;

    if (state == oldstate) return;

    if (state == 252) {   // error
        debugPrint("Ampel: blue");
        ws2812fx.removeActiveSegment(1);
        ws2812fx.setSegment(0, 0, CONFIG_NEOPIXEL_LEDS-1,
                FX_MODE_SCAN, 0x000080, 1000, true);
        ws2812fx.setBrightness(16);
    } else if (state == 253) {
        debugPrint("Ampel: rainbow");
        ws2812fx.removeActiveSegment(1);
        ws2812fx.setSegment(0, 0, CONFIG_NEOPIXEL_LEDS-1,
                FX_MODE_RAINBOW_CYCLE, RED, 2000, false);
        ws2812fx.setBrightness(32);
    } else if (state == 254) {
        debugPrint("Ampel: yellow");
        uint32_t colors[] = {0x202000, 0xA0A000};
        ws2812fx.removeActiveSegment(1);
        ws2812fx.setSegment(0, 0, CONFIG_NEOPIXEL_LEDS-1,
                FX_MODE_FADE, colors, 4000, false);
        ws2812fx.setBrightness(16);
    } else if (state == 255) {
        debugPrint("Ampel: RED");
        ws2812fx.removeActiveSegment(1);
        ws2812fx.setSegment(0, 0, CONFIG_NEOPIXEL_LEDS-1,
                FX_MODE_BLINK, RED, 1000, false);
        ws2812fx.setBrightness(64);
    } else {
        debugPrint((String)"Ampel: " + state);
        if (state > 0) {
            ws2812fx.setSegment(1, 0, state-1,
                    FX_MODE_STATIC, 0x303000, 1000, false);
            ws2812fx.setSegment(0, state, CONFIG_NEOPIXEL_LEDS-1,
                    FX_MODE_STATIC, 0x004000, 1000, false);
        } else {
            ws2812fx.removeActiveSegment(1);
            ws2812fx.setSegment(0, 0, CONFIG_NEOPIXEL_LEDS-1,
                    FX_MODE_STATIC, 0x004000, 1000, false);
        }
        ws2812fx.setBrightness(16);
    }

    updateNeopixel();

    oldstate = state;
#endif
}

