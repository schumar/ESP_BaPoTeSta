**_`tl;dr`: Circa 10 &euro; pro St&uuml;ck_**

# Intro

Die BaPoTeSta ist zu einem vollwertigen
Sensor-/ESP8266-/ATmega-Development-Board geworden, dementsprechend braucht
nicht jeder alle Teile.

Hier eine Liste an Möglichkeiten -- wenn ich einen Einsatzzweck nicht bedacht
habe, oder alles viel zu verwirrend ist, helfe ich natürlich gerne bei der
Auswahl!

Alle angegebenen Preise sind unverbindlich und ungef&auml;hr, basierend auf einer
schnellen Suche auf Aliexpress. Bilder sind nur Symbolbilder. L&ouml;tarbeiten
nicht im Preis inkludiert. Keinerlei Garantie für irgendwas. Angeboten werden
einzelne Teile, kein kompletter Bausatz oder ein fertiges Ger&auml;t. Nicht für den
Einsatz an lebensgef&auml;hrlichen Spannungen (50V+) entwickelt! Bitte nicht an
Teilen nuckeln. Nicht in Lebewesen einbauen! Keinen Atomreaktor damit betreiben,
und bitte nicht in Lenkraketen einsetzen. *Dear chinese manufacturer: It
would be great if you could produce this in massive quantities for a really low
price, thanks!*

# Basisset (1.00 &euro;)

![BaPoTeSta Basic Set v0.8](pics/board_v0.8_base_raytr.jpg)

Die Teile die jeder braucht, und Zeug das so billig ist dass es sich nicht
auszahlt dass ich's einzeln abrechne:

* Platine
* 31 Pinheader
* 2 Jumper
* Widerst&auml;nde (1 564, 1 104, 5 103, 3 472, 2 221, 1 202)
* Kondensatoren (3 1u, 1 100u, 1 4.7u)
* Spannungsregler (MCP1700)

# Kommunikation

## WLAN (1.50 &euro;)

Das will fast jeder :)

![ESP-12F](pics/esp-12f.jpg)

* ESP-12F

(Man kann auch einen ESP-12E verwenden wenn man welche herumliegen hat, die
haben allerdings eine etwas schlechtere Antenne. ESP-12S hätte eine bessere
Antenne als der ESP-12F, ist aber ob des etwas größeren Metallgehäuses schwerer
zu verlöten)

## Funk

Noch nicht sehr erprobt (bisher erst 1 Prototyp im Einsatz), Firmware braucht
wohl noch ein bisschen Hacking, aber theoretisch gesehen sollte die Kombination
stromsparender sein als der WLAN-Betrieb.

![ATmega328](pics/atmega328.jpg)
![NRF24](pics/nrf24.jpg)

* AVR ATmega328P-AU (1.20 &euro;)
* Funkmodul (1.00 &mdash; 10.00 &euro;)

Der ATmega hat wesentlich mehr GPIOs ("Pins") zur Verfügung als der ESP-12F,
von denen 6 auf der BaPoTeSta auch nutzbar sind.

Achtung: Die Pins vom ATmega haben einen Abstand von 0.7mm, also braucht man
entweder Geschick und einen guten Lötkolben mit feiner Spitze, oder einen
Reflow-Ofen, oder eine Heatgun zum SMD-Löten.

Als Funkmodul habe ich bisher nur einen stupiden 433MHz-Sender benutzt, ein
["433MHz ASK Modul"](https://www.aliexpress.com/item/ASK/1347349030.html) --
das ist aber echt ranzig.

Eigentlich sollte alles einsetzbar sein was 6 Datenpins oder weniger benutzt
(also vermutlich _jedes_ Modul), und für das es eine Arduino-Library gibt.

Ich kann da gern beratend zur Seite stehn, es gibt viele Möglichkeiten,
inklusive LoRa, ZigBee, Bluetooth und GSM ;)

Eine 2., abgespeckte BaPoTeSta kann als Empf&auml;nger dienen.


# Firmware draufspielen

Eine fertig programmierte, funktionierende BaPoTeSta mit ESP-12 kann man
übers Webinterface mit neuer Firmware versorgen.

Aber für den Fall dass man das Ding mal verflasht ists ganz praktisch einen
USB-TTL-Konverter in der Schublade zu haben, um das Teil wiederzubeleben.

Unbedingt notwendig auch für die ATmega-Variante, oder falls man eine
spezielle Firmware ohne Webinterface benutzt.

Natürlich braucht man das nur 1mal, auch wenn man 20 BaPoTeSta im Einsatz
hat.

![CP2102](pics/cp2102.jpg)

* USB-TTL-Konverter (1.00 &euro;)


# Stromversorgung

## Eingang

Wie kommt der Strom "in das Kastl"?

### "Zu Fu&szlig;", via extern geladenem Akku

Siehe unten, "Austauschbarer Akku"

### Mini-USB (Buchse) (0.60 &euro;)

(Micro-USB ist zwar heutzutage verbreiteter, aber wirklich schwierig zu
verlöten ohne Reflow-Ofen)

![Mini-USB Socket](pics/mini-usb.jpg)

* Mini-USB Buchse

### USB (fixes Kabel) (0.70 &euro;)

![USB cable](pics/usbcable.jpg)

* USB-Kabel (auf der einen Seite USB-A, auf der andern is wurscht,
  das wird dann eh wegschnitten)

### Externe Versorgung mit bis zu 6 Volt

* nix ;)

### Externe Versorgung mit über 6 Volt (0.60 &euro;)

![DC Buck Converter](pics/dcbuck.jpg)

* DC-DC Buck Converter

Das Zeug ist nicht sehr stromsparend, aber wer die BaPoTeSta mit einer
Autobatterie betreibt ist daran vermutlich eh nit intressiert ;)

### Solarpanel (?)

Gute Idee. Noch nie probiert. Angeblich zahlt sich für so kleine Anwendungen
weder [MPPT](https://en.wikipedia.org/wiki/Maximum_power_point_tracking)
noch eine automatische Nachführung aus, also bleibt

* Solarpanel
* DC-DC Buck Converter (falls das Teil mehr als 6V liefert) (0.60 &euro;)

## Akku

Nicht notwendig wenn man eine fixe externe Versorgung hat!

### Ladeelektronik (0.10 &euro;)

Nice-to-have wenn "Eingang" == "Zu Fu&szlig;", sonst _notwendig_

![Ladeelektronik](pics/mcp73831-onboard.jpg)

* Ladekontroller MCP73831-2-OT
* 2 LEDs

### Fixer Akku (3.50 &euro;)

![103450 Akku](pics/103450.jpg)

* 103450 Akku (10x34x50mm) (eingebaute Schutzschaltung)

### Austauschbarer Akku

Mehr Kapazit&auml;t als ein 103450er, und auch mit externem Ladeger&auml;t ladbar.

* 18650 Akku (ohne Schutzschaltung) (3.50 &euro;)
* Schutzschaltung (NCP300 und BSS806N) (0.60 &euro;)
* Akkuhalter (0.80 &euro;)


# Sensoren

Mit den bisherigen Teilen kann die BaPoTeSta nur die Eingangsspannung und
(bei der ESP-12 Variante) die WLAN-St&auml;rke melden. Auch lustig, aber da geht
mehr!

Leider hat man keine sehr gro&szlig;e Anzahl an Pins zur Verfügung um Zeug
an die BaPoTeSta anzuh&auml;ngen. Wenn man viele Werte messen will, ist es
daher am besten man besorgt Module die via
[I²C](https://en.wikipedia.org/wiki/I%C2%B2C) kommunizieren (das ist ein
Bus, d.h. man kann mehrere Ger&auml;te daran betreiben).

Die I²C-Sensoren bekommt man meist als Modul (verbindet man also dann via
Kabel mit der BaPoTeSta, siehe unten "Kabel"), nur DS18B20 und DHT22 kann
man auch direkt auflöten.

Die hier aufgelisteten Sensoren sind nur Beispiele; es gibt noch viele
andere die von diversen Arduino-Libraries unterstützt werden (dann muss
man das halt in die Firmware aufnehmen, ist keine gro&szlig;e Kunst).  
Ich kann da gerne beratend zur Seite stehen!

## Für Akkubetrieb geeignet

### Dallas DS18B20 (Temperatur)

Der "erste" BaPoTeSta-Sensor, seit v0.2 dabei.

Kommunziert via [1-Wire](https://en.wikipedia.org/wiki/1-Wire), man kann
also theoretisch mehrere davon mit einer BaPoTeSta betreiben; das ist
allerdings Firmware-seitig noch nicht implementiert.

![DS18B20](pics/ds18b20.jpg)
![DS18B20 Kabel](pics/ds18b20-cable.jpg)

* DS18B20 (0.60 &euro;)
* DS18B20 mit Kabel, wasserdicht (1.00 &euro;)

[Datenblatt](https://datasheets.maximintegrated.com/en/ds/DS18B20.pdf)

### Thermoelement (Temperatur) (I²C) (3.50 &euro;)

Kann Temperaturen bis zu 400 bis 600&deg;C messen (je nach verwendetem
Thermoelement).

![Thermocouple](pics/thermocouple.jpg)

* Thermoelement (1.50+ &euro;)
* MAX31855 Modul (2.00 &euro;)

Noch kein Firmware-Support! (aber es existiert eine Arduino-Library)

### DHT22 (Temperatur, Feuchtigkeit) (2.50 &euro;)

Auch schon kampferprobt, ein sehr billiger Feuchtigkeits-Sensor.  
Nicht sehr genau, aber für die meisten Zwecke "gut genug".

![DHT22](pics/dht22.jpg)

[Datenblatt](https://www.sparkfun.com/datasheets/Sensors/Temperature/DHT22.pdf)

### Bosch BMP280 (Temperatur, Druck) (I²C) (0.70 &euro;)

![BMP280](pics/bmp280.jpg)

[Datenblatt](http://datasheet.octopart.com/BMP280-Bosch-datasheet-13691204.pdf)

### Bosch BME280 (Temperatur, Druck, Feuchtigkeit) (I²C) (2.20 &euro;)

![BME280](pics/bme280.jpg)

[Datenblatt](http://datasheet.octopart.com/BME280-Bosch-Tools-datasheet-119732019.pdf)

### TSL2561 (Helligkeit) (I²C) (1.00 &euro;)

Noch kein Firmware-Support! (aber es existiert eine Arduino-Library)

![TSL2561](pics/tsl2561.jpg)

[Datenblatt](https://cdn-shop.adafruit.com/datasheets/TSL2561.pdf)

### CCS811 (Kohlenmonoxid) (I²C) (8.00 &euro;)

Noch kein Firmware-Support! (aber es existiert eine Arduino-Library)

![CCS811 Modul](pics/ccs811.jpg)

### Taster, Schalter

Da kaum Pins frei sind, und ein Schalter im Normalfall einen Pin belegt,
empfehle ich die Verwendung eines I²C-Port-Multipliers. Der h&auml;ngt am
I²C-Bus (braucht also keine zus&auml;tzlichen Pins), und stellt 8 zus&auml;tzliche Pins
zur Verfügung.

![PCF8574](pics/pcf8574.jpg)
![Buttons](pics/buttons.jpg)

* PCF8574 Modul (1.00 &euro;)
* Taster, Schalter

Natürlich kann man darüber auch Sensoren ansteuern die sich wie Schalter
verhalten (Wasserstandssensor, Reed-Kontakt, Neigungssensor,...)

Noch kein Firmware-Support! (aber es existiert eine Arduino-Library)

## Nur für st&auml;ndige Stromversorgung

### NEO-7M (GPS: Position, Geschwindigkeit)

Ein billiges GPS-Modul ist spürbar besser als das was in Handys verbaut ist.
Natürlich kein AGPS-Support, d.h. der initiale Fix kann schon 10 Minuten
brauchen, aber es wird ja fast nie abgeschalten.

Die Kommunikation erfolgt über ein serielles (RS232) Protokoll (NMEA),
benötigt also einen eigenen Pin.

![NEO-7M Modul](pics/neo-7m.jpg)

* NEO-7M Modul mit Antenne (5.00 &euro;)

Firmware-Support nur im `nautic`-Branch

### Gassensoren der MQ-Reihe

Es gibt eine ganze Menge Sensoren deren Name mit "MQ" beginnt, die die
Konzentration von bestimmten Gasen messen, hier die
[Liste auf arduino.cc](https://playground.arduino.cc/Main/MQGasSensors)

Sie haben den Vorteil dass sie in der Form von sehr billigen Modulen
erh&auml;ltlich sind, aber eine Menge Nachteile:

* Irrer Stromverbrauch (fast 1 Watt) &rarr; kein Batteriebetrieb!
* Teilweise komplizierte Messmethoden, die vom Modul garnicht unterstützt
  werden
* Benötigen eine "Einbrennzeit" von 1..2 Tagen, bevor sie sinnvolle Werte
  liefern
* Messen meist mehrere Gase auf einmal
* Nicht kalibriert, d.h. Ergebnisse sind nicht mit publizierten Grenzwerten
  vergleichbar
* Analoger Ausgang, d.h. benötigen zus&auml;tzlich einen I²C-AD-Wandler

![MQ Sensoren](pics/mq-sensors.jpg)
![ADS1115](pics/ads1115.jpg)

Aber ob des Preises natürlich trotzdem eine lustige Spielerei, also:

* MQ-x (1.00&mdash;1.50 &euro; pro Modul)
* ADS1115 ADC (1.50 &euro;)

Der AD-Wandler kann 4 Kan&auml;le messen, d.h. nur 1 pro 4 Sensoren nötig.

Noch kein Firmware-Support! (aber einfach zu implementieren)

### Gewittersensor (25.00 &euro;)

Der AS3935 von AMS (in Graz) kann Blitze in bis zu 40km Entfernung detektieren,
und damit also vor einem nahenden Gewitter warnen.

![AS3935](pics/as3935.jpg)

Leider

* ist das Modul recht teuer
* ist der Chip etwas eigenartig anzusprechen, z.B. hat er die I²C-Adresse 0x03
  (eigentlich nicht zul&auml;ssig, "Reserved for Future Use")
* liefert mein Test-Modul nur "too much noise" Fehler, eventuell weil die
  Antenne nicht korrekt abgestimmt ist -- der Hersteller hat scheinbar die
  Bauteiltoleranzen zu nachl&auml;ssig gew&auml;hlt, d.h. Austausch von
  SMD-Kondensatoren ist angesagt :(
* muss man zum Testen auf ein Gewitter warten, auf "künstliche" Blitze
  spricht der Chip (glücklicherweise) nicht an

Aber wer gern experimentieren will...

### MMA8451 (Beschleunigung) (I²C) (2.00 &euro;)

Ich wei&szlig; noch nicht genau wofür man das brauchen könnte, aber für irgendwas
ists sicher gut ;)

![MMA8451](pics/mma8451.jpg)

Noch kein Firmware-Support, noch ungetestet, Arduino-Library ist vorhanden

[Datenblatt](http://cache.freescale.com/files/sensors/doc/data_sheet/MMA8451Q.pdf)

### MPU-9250 (Beschleunigung, Magnetfeld, Rotation) (I²C) (2.50 &euro;)

Sehr günstiger 9-DOF Sensor

![MPU-9250 Modul](pics/mpu-9250.jpg)

Kein Firmware-Support, Arduino-Library ist vorhanden

### LSM9DS0 (Beschleunigung, Magnetfeld, Rotation, Temperatur) (I²C) (15.00 &euro;)

Sehr cooles Teil, aber auch entsprechend teuer

![LSM9DS0](pics/lsm9ds0.jpg)

Kein Firmware-Support, Arduino-Library ist vorhanden

[Datenblatt](http://www.adafruit.com/datasheets/LSM9DS0.pdf)


# Andere Peripherie

## LCD (3.00&mdash;4.50 &euro;)

Oft nachgefragt, jetzt endlich verfügbar!

![LCD](pics/lcd.jpg)

Natürlich nur sinnvoll mit st&auml;ndiger Stromversorgung, ein Display das nur
alle 5 Minuten für ein paar Sekunden einen Wert anzeigt ist etwas schwer
abzulesen.

Die Module gibt es in verschiedenen Grö&szlig;en (16x2, 16x4, 20x4 Zeichen),
und glücklicherweise mit I²C-Adapter, d.h. es wird kein zus&auml;tzlicher Pin
verbraucht.

Firmware-Support nur im `nautic`-Branch, für 1602 und 1604, 2004 ist noch
ungetestet.

## OLED-Display (I²C)

Einfarbig und klein (~1 Zoll), aber dafür 128x64 Pixel, via I²C steuerbar,
und billig:

* 0.96" (2.00 &euro;)
* 1.3" (3.00 &euro;)

![OLED 0.96"](pics/oled096.jpg)

Noch kein Firmware-Support, Arduino-Library vorhanden

## TFT

Genau wie bei LCDs: Nur sinnvoll mit st&auml;ndiger Stromversorgung.

Ich habe leider noch kein reines I²C-TFT gefunden. Es gibt aber welche die
zus&auml;tzlich zu den 2 I²C-Pins nur 2 weitere Datenpins benötigen (mit dem
"ST7735" Controller), diese sollten sich ansteuern lassen.

Wer es versuchen will ist herzlich eingeladen :)

![TFT 0.96"](pics/tft096.jpg)

* 0.96" (160x80) (2.50 &euro;)
* 1.8" (160x128) (4.50 &euro;)

Noch kein Firmware-Support, vermutlich aufw&auml;ndig -- dafür bunt!!

## e-Ink Display (13 &euro;)

Sollte für Akkubetrieb geeignet sein!

![E-Ink](pics/eink.jpg)

* 1.54" black/white (200x200)
* 1.54" black/white/red (200x200)
* 1.54" black/white/yellow (152x152)

[Source](https://github.com/MHEtLive/MH-ET-LIVE-E-Papers),
[Arduino Library](https://github.com/ZinggJM/GxEPD),
[Info](http://mh.nodebb.com/topic/40/mh-et-live-1-54-inches-e-paper)

## LEDs, L&auml;mpchen, Biepser

Wie schon bei "Taster, Schalter" oben: Am besten mit einem I²C-Portextender

Noch kein Firmware-Support! (aber es existiert eine Arduino-Library für den
Portextender)

## SD-Karten-Modul (0.40 &euro;)

SD-Module werden über SPI angesprochen, benötigen also 4 Pins (eventuell
reichen auch 3), soviele hat man meist nicht übrig.

Wer es versuchen will ist herzlich eingeladen :)

![SD Module](pics/sd-module.jpg)

Noch kein Firmware-Support, Arduino-Libraries sind aber vorhanden

## SD-Logger (3.50 &euro;)

Es gibt ein fertiges Modul namens "OpenLog", welches einen kleinen ATmega
enth&auml;lt der die SD-Karte anspricht.

![OpenLog](pics/openlog.jpg)

Die [freie Firmware](https://github.com/sparkfun/OpenLog) für das Teil kann
alles was man zum einfachen Loggen braucht, BaPoTeSta-Firmware-Support bereits
im `nautic`-Branch vorhanden.

2 Warnungen:

* Manche der Module auf aliexpress enthalten noch garkeine Firmware, man
  muss sie noch selbst mit einem ATmega-Programmer flashen. Da die nötigen
  Pins dafür nicht auf Header herausgeführt sind, muss man dazu aber feine
  Kabel an die kleinen IC-Pins löten. Geringer Spa&szlig;faktor.
* Bisher erst mit einem einzigen Prototypen getestet, und dort einige
  "Aussetzer" (tw. eine Menge NULs mitten in den Files) beobachtet.

## Sprachausgabe

Sprachausgabe ist noch völlig ungetestet, folgend ein paar Ansätze. Alle führen
zu einem kleinen Audiosignal, das man in einen Verstärker schicken muss.
Der wiederum benötigt aber 12V Versorgungsspannung, d.h. nur mit der Option
"Externe Versorgung mit über 6 Volt" sinnvoll!

![PAM8610 Module](pics/pam8610.jpg)

* PAM8610 Verstärker Modul (1.00 &euro;)
* 1 oder 2 Lautsprecher (mono/stereo)

### Software

Mittels [Talkie](https://github.com/adafruit/Talkie) oder
[TTS](https://github.com/jscrane/TTS).

### MP3-Modul

Ein paar Phrasen vorab aufnehmen, als MP3 auf eine SD-Karte speichern, und die
dann in ein MP3-Modul stecken.

Ansteuerung über RS232, d.h. 2 zus&auml;tzliche Pins werden benötigt.

![MP3 Module](pics/mp3player.jpg)

* MP3 Player Module (1.20 &euro;)
* dazupassende SD-Karte

### XFS5152CE TTS-Modul (15.00 &euro;)

Teuer, aber lustiges Spielzeug. Spricht Englisch und Chinesisch. Ansteuerung
auch über I²C, d.h. kein zusätzlicher Pin notwendig.

![XFS5152CE Modul](pics/xfs5152ce.jpg)

* XFS5152CE Modul

[Datenblatt](www.iflytek.com/upload/contents/2014/07/53be5e3ec4047.pdf)

# Kabel

Wie man diverse Module mit der BaPoTeSta verbindet h&auml;ngt davon ab, ob man
sie im selben Geh&auml;use verbauen will, oder mit einem l&auml;ngeren Kabel "au&szlig;en".

## Im Geh&auml;use

Sogenannte "Dupont" female-female Kabel gibts in gro&szlig;en Mengen (20 oder 40
Stück) in diversen L&auml;ngen (10/20/30cm).

![Dupont female-female Kabel](pics/dupont-ff.jpg)

Eine Packung (zB. 40 zu 10cm: 0.70 &euro;) reicht daher für mehrere BaPoTeStas.

### JST-XH

Eine Alternative sind Kabel mit JST-XH Steckern. Leider sind die dazugehörigen
Buchsen sehr groß, so dass man bei Einsatz einer solchen für P10 P9 nicht mehr
benutzen kann.

![JST-XH Kabel](pics/jst-xh.jpg)
![JST-XH Buchse auf BaPoTeSta](pics/jst-xh-socket.jpg)

Vorteil: Robustere Verbindung, die Kabelenden kann man direkt an die
Sensor-Module löten.  
10er-Pack (Kabel, Stecker, Buchse) kostet circa 1.20 &euro;.

## Au&szlig;erhalb

Da die I²C-Sensoren nur 4 Leitungen benötigen, kann man den Anschluss über
4-polige
[Klinkenverbindungen](https://en.wikipedia.org/wiki/Phone_connector_(audio))
("TRRS", tip-ring-ring-sleeve) bewerkstelligen.

Ich empfehle den Kauf eines Klinkenkabels, das man in der Mitte durchschneidet.
Dann hat man also gleich 2 Kabel mit Stecker, die man jeweils an ein Modul
lötet, und am BaPoTeSta-Geh&auml;use bringt man dann Klinkenbuchsen an.

Die Klinkenbuchse kann man dann direkt mittels eines Kabels auf die BaPoTeSta
löten, oder man benutzt die oben erwähnten JST-XH Kabel.

![TRRS Kabel](pics/trrs-cable.jpg)
![TRRS Buchse](pics/trrs-socket.jpg)

* Klinkenkabel (1.00 &euro;) (1 Kabel &rarr; 2 Module)
* Klinkenbuchse (0.50 &euro;)

Vorsicht! I²C ist eigentlich nicht für Uebertragung über ein langes Kabel
gemacht. 1m sollte noch klappen, 5m sicher nicht mehr.

# Geh&auml;use

Gibt's noch keines, aber ich bin sicher dass jemand von Euch geschickt genug
ist, sowas in FreeCAD, OpenSCAD, Blender, oder irgendeiner propriet&auml;ren
Software zu designen.

Pull-Requests are welcome!

