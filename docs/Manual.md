# Read This First!

If you have acquired a finished BaPoTeSta, you can skip ahead to
[Configuration](#configuration) now!

The following instructions are for the board as I designed it -- if you are
using another PCB, or have modified the schematics, they might still be a good
rough guideline.

# Parts List ("BOM")

Download the
[interactive BOM](https://gitlab.com/schumar/ESP_BaPoTeSta/raw/master/KiCAD/bom/ibom.html?inline=false)
and open it in your browser.  
This will also be useful during assembly!

# Soldering

If you haven't done any soldering before (or your knowledge is a little rusty),
you might want to check the Internet for tutorials :) e.g.

* [Great Scott: How to Solder properly](https://www.youtube.com/watch?v=VxMV6wGS3NY)
* [Sparkfun: How to Solder: Through-Hole Soldering](https://learn.sparkfun.com/tutorials/how-to-solder-through-hole-soldering)
* [Sparkfun: How to Solder: Castellated Mounting Holes](https://learn.sparkfun.com/tutorials/how-to-solder-castellated-mounting-holes)
* [Aaron Cake: How To Solder](http://www.aaroncake.net/electronics/solder.htm)

but if you have some time, and want to have a good understanding of soldering,
check out
[PACE: Basic Soldering Lesson 1](https://www.youtube.com/watch?v=vIT4ra6Mo0s)  
(it's American, so prepare to snicker whenever the presenter pronounces
soldering as "soddering" :)

Important: Avoid consuming coffeine before soldering, to ensure that you have
a steady hand!

## SMD Parts

Start by tinning the right ("eastern") pads of horizontal parts, and the top
("northern") pads of all 2-pad parts (`R*`, `C*`, `D*`).

Solder the resistors (`R*`) first. They are labelled with a 3- or 4-digit code;
the last digit is the number of trailing zeroes, e.g. `472` would be 4700 Ohms.  
The board is labelled with 3-digit codes, so if a resistor has a 4-digit code,
you need to convert by dropping the 3rd digit and increasing the last:

| 4-digit | 3-digit
|--------:|-------:
| `1002`  | `103`
| `1003`  | `104`
| `2001`  | `202`
| `2200`  | `221`
| `4701`  | `472`
| `5603`  | `564`

Solder down the resistors at the already tinned pad. Extra points if
you make sure that the labelling (e.g. `103`) is visible, and rotated correctly
(left-to-right or bottom-to-top).

You can

| skip  | if you don't
|------:|------------------------------------
| `R1`  | want to measure the battery voltage
| `R2`  | want to measure the battery voltage
| `R7`  | have any I2C sensors
| `R11` | have any I2C sensors
| `R12` | have a DS18B20 sensor
| `R14` | need the charging electronics
| `R15` | need the charging electronics
| `R16` | need the charging electronics

If you need the charging electronics, also solder the 2 LEDs.  
Important: `D2` (the left LED) needs to have the small green mark at the top
(north), while `D1` has it at the bottom (south). If you solder them the wrong
way round, they won't work!

`D2` will light up during charging, `D1` when the battery is fully charged
(in case you want to choose some different, specific colors).

When all the parts are positioned nicely and soldered down on one pin, solder
their 2nd pin.

That was easy, right? Now comes the tricky part: soldering the tricky parts,
i.e. those with more than 2 pins: `U1`, `U3`, `U4` and `Q2`.

In case you are not sure which of those is which, here are some (possible)
labels:

| Part | Label
|-----:|--------
| `U3` | KD25
| `U1` | CS..
| `U4` | SECCV
| `Q2` | YE..

Again, you should tin one pad first (I recommend a corner pin), solder down
the part on that pin, and when it's positioned nicely, solder the other pins.

You can

| skip | if you don't
|-----:|------------------------------------
| `U3` | need the charging electronics
| `U4` | use a battery without protection circuitry (18650)
| `Q2` | use a battery without protection circuitry (18650)

The next parts are easier again: The capacitors (`C*`). They are a bit bigger
than the parts we have soldered so far, have only 2 pins, and it doesn't matter
which way you solder them.

## Bridge

If you want to power your BaPoTeSta directly from USB instead of a battery,
and thus have not soldered the charging electronics, you have to either
connect one `Charge +` pin with the `Battery +` pin (solder in a cable, or
do this later on, when the pin-headers are soldered in, with a DuPont cable),
or create a small solder bridge between the two rightmost pins of `U3` now.  
(this might be easier if you solder down a 3mm piece of wire)

If you are are not using a battery without protection circuitry (18650),
i.e. have not soldered `U4` and `Q2`, you need to connect the top left pad of
`Q2` to the bottom one -- again, a 3mm piece of wire might make this easier.

## ESP-12

Important: You do _not_ need to solder the 5 pins on the western (left)
edge of the module!

I've found this to be a good way to solder the ESP-12:

* Tin the TX pad of the PCB
* Put the module on the PCB
* Now heat the TX pin a little while pressing (gently!) down on the module.  
  Try to move the module so that it's perfectly aligned.
* Do the same for VCC
* Solder all pins (do TX/VCC last!)
* If you aren't an experienced solderer/ess(?), shortly heat each pin again
  (until the solder melts again), to ensure that all connections are flawless

## Mini USB socket

Tin the rightmost pin, solder down the socket, and then solder only the 4 big
ground pads. Do not try to solder the other small pins, we are not using them.

## Cleaning flux residue

Now is a good time to take some isopropanol, maybe mixed 1:1 with acetone,
and, using a brush, clean flux residue from the board (the ugly brown stuff).

## Pin Headers

Speaking from experience:

* Make sure that you count correctly! You need up to 4 4-pin-headers, up to 2
  3-pin-headers, and up to 4 2-pin-headers
* To break them apart, use 2 small pliers. You can also try to do it using just
  your fingers, though.
* Only solder those headers that you are actually going to use!
* Put a jumper on the pins during soldering
* After soldering, those things are *hot* (d'uh). Touching them is bad.
* Soldering might take a little more patience than usual, because the pins have
  a high thermal mass.


## Power

Solder the battery, or the USB-cable, or the wires of an external to the
2 `Battery` pins.

There are 2 small mounting holes next to the pins, which you can put the
cables through if they are thin enough (to prevent them from breaking).

## Additional Pins

Depending on your use case, the following pins might be available for additional
periphery:

| ESP8266 |   Board   | Available if     | Remarks                     |
|--------:|----------:|------------------|-----------------------------|
|     ADC | east pin of R1 | Voltage measurement not needed | Input to ADC  |
|  GPIO15 |     CS    |                  | Has to be pulled down by R6 |
|  GPIO14 |     SCL   | I2C not used     | Can be pulled up by R7      |
|  GPIO13 | pad east of R1 | Sensors permanently powered (swtiched J1) | |
|  GPIO12 |     SDA   | I2C not used     | Can be pulled up by R11     |
|   GPIO5 |     1W    | DS18B20 not used | Can be pulled up by R12     |
|   GPIO4 |     Conf  | Config mode not needed |                       |
|   GPIO3 |     RX    | Serial debug not used | Disconnect while flashing! |
|   GPIO1 |     TX    | Serial debug not used | Disconnect while flashing! |
|   GPIO2 |  DHT Data | DHT22 not used   | Has to be pulled up by R13  |


## Finish

Make one last visual inspection of all soldering joints.

If you want, you can switch on your board now -- it should briefly flash the
blue LED on the module. If the LED doesn't flash, switch off the board, take
your multimeter and test all connections :(  
If it does flash, switch the board off again; no use in wasting energy.

Tin, clean, and re-tin you soldering iron, switch it off, clean the board, put
back any unused parts. Wash your hands if you aren't using lead-free solder!

Take a non water-solvable pen and write an ID on the white boxes on the front
and back of the board. Feel free to choose whatever scheme you want (most
people will go with "1, 2, 3,...", but why not use Klingon letters instead? ;)

Congratulations :)  
You are now ready to flash the firmware!

# Flashing

Right now, your ESP-12 has the stock firmware, which is useless for what we want
to do.

## Firmware

If you want to compile the firmware yourself, see the [Hacking doc](Hacking.md)
on how to do this.

But there are only a few reason to do so:

* Your ESP8266-module has a flash size other than 4MiB
* The configuration of the pre-compiled firmware doesn't work for you (e.g. you
  are using the pins differently, or other sensors,...)
* While you can change the WiFi-password for the Maintenance mode directly in
  the Maintenance web-interface, the board will still start with the default
  password the first time. If you are worried about that, you are even more
  paranoid than me :)
* You don't trust binaries you haven't compiled yourself -- good for you!

Otherwise, just take the [pre-compiled firmware](../precompiled/).

## Connect a Serial-to-USB adapter to your board

You need a Serial-to-USB adapter with support for 3.3V levels instead
of 5V!

* Switch off your board.
* Ensure that your adapter is set to 3.3V!
* Connect the "GND" pin of the adapter to the `blk` pin on the board.
* Connect the "TX" pin of the adapter to the `orn` pin of the board,  
  and the "RX" pin of the adapter to the `yel` pin.
* Do *not* connect VCC, unless you know what you are doing!

## Connect the adapter to your PC

I guess you can figure out how to do this :)

Check `dmesg` for the device name of the adapter (e.g. `/dev/ttyUSB0`)

## Transfer firmware

* Install the [`esptool`](https://github.com/espressif/esptool)
* Put a jumper bridge on the 2 `Flash` pins.
* Switch on the board (or reset it by connecting `RESET` to `GND`)
* Flash with  
  `esptool --chip esp8266 --port /dev/ttyUSB0 0 write_flash 0x0 ESP_BaPoTeSta.ino.bin`
* Remove the jumper, restart the board

If it doesn't work the first time, make sure that you have specified the correct
device name, and that you haven't swapped TX and RX. Try again.

# Configuration

The BaPoTeSta can boot into a configuration mode, where it acts as an AP,
and runs a web-server with a simple page allowing you to configure a few
things.

## Entering configuration mode

* Switch off the power, e.g. by disconnecting the Power jumper  
  ![Power OFF](jumper_off.jpg)
* Set the Config jumper  
  ![Config ON](jumper_config_on.jpg)
* Switch power back on  
  ![Power ON](jumper_on.jpg)
* Connect to BaPoTeSta's AP  
  ![Connect WiFi](wifi_config.png)  
  SSID will be `chip-00xxxxxx`, PW is `bapotesta` (you can change this by
  recompiling the firmware, see above)
* Use a browser to visit `http://192.168.4.1`  
  ![Visit 192.168.4.1](browser_config.png)

## What to configure

When setting up a brand new BaPoTeSta, you will most likely want to set up
networking (WiFi credentials, IP adresses,...), as well as your height above
sea-level (when having a barometric sensor).

But feel free to look/play around -- the web-interface does *not* allow you
to change the WiFi-credentials for the config-mode, so there is no danger
of locking yourself out!

When done, don't forget to press **Apply** `:)`

## Leaving configuration mode

* Switch off the power, e.g. by disconnecting the Power jumper  
  ![Power OFF](jumper_off.jpg)
* Remove the Config jumper  
  ![Config ON](jumper_config_off.jpg)
* Switch power back on  
  ![Power ON](jumper_on.jpg)

# F.A.Q.

## Why no DHCP?

Booting BaPoTeSta, measuring battery level, connecting to WiFi,
connecting to MQTT, measuring temperature/pressure/humidity using a BME280,
sending that data via MQTT, and going back to sleep takes about **0.3 seconds**.

Acquiring an IP address via DHCP takes at least a few seconds, which
would thus *massively* decrease battery life.

If you are absolutely sure that you don't care about that, you can enable
DHCP in the firmware configuration menu, see [Hacking](Hacking.md).

## Why is there no support for *xxx* in the firmware?

Possibly because you haven't asked for it via [Issue](https://gitlab.com/schumar/ESP_BaPoTeSta/-/issues) ;)  
or I simply haven't found time to implement it yet -- this is a hobby project after all, but you are
very welcome to help!

## The battery measurement "circuitry" wastes power!!

It's true that those 2 resistors are wasting power; the alternative would be
to switch them off when not measuring, using a MOSFET.

This was in fact in place already, but given that a MOSFET costs extra money,
needs extra soldering work, and that the current circuitry only pulls ca.
6 **μ**A, I decided to go back to this simple approach.

## Do I have to use the BaPoTeSta board?

"... Can't I just use an Arduino Uno, or a WeMos D1, or a BluePill?"

Sure you can! The BaPoTeSta board has a few niceties those other HW might
be missing (extremely low power consumption, battery management, pull-ups
for I2C/1Wire/DHT), but the software has already been tested on all the
above-mentioned boards!

## Why is this written "in Arduino"?

* Libraries for nearly all sensors are already available for Arduino
* Supporting 3 different platforms (BaPoTeSta can currently run on ESP8266,
  ATmega328, and STM32F1) would be a lot harder to do w/o Arduino

## "BaPoTeSta"?

**Ba**ttery **Po**wered **Te**mperature **Sta**tion.

You can of course power it via USB instead, and measure other data than
just temperature, but that's what that project initially did, and the
name stuck.


