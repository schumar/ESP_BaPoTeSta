# How to add support for a new sensor

(just a quick preliminary list)

## Documentation

* add library requirements to `Hacking.md`

## `Kconfig`

* add required configs to the `Sensors` submenu
* update `configs/all_[yn]_defconfig` by running `generate_defconfigs.sh`

## `html_css.h`

* add `SUPPORT` calculation
* add `inputgroup`
* add `SUPPORT` in the `Info` part

## `ESP_BaPoTeSta.h`

* If necessary, add `sensorType`, `sensorTypeName`, `unitType`, `unitTypeName`
* add web-interface-configurable stuff to `struct config`
* increase configVersion

## `ESP_BaPoTeSta.ino`

* `#include` library
* add necessary global variables
* In setupNormal():
    * For I2C-sensors, add config.useXXX to "setup I2C" condition
    * add setup routine
* write `getFoo()` function, wrap in `#ifdef CONFIG_SUPPORT_foo`
* add to `collectData()` function
* add config'able stuff to `webForm()` and `storeConfig()`

## Server

* update `server/graphs.pl`

## Test!

The provided `fuzzconfig` script will generate random configurations, and then
try a compilation run. (This will often report "Sketch too big", but don't worry
about that). Let this run for some time -- it should never abort by itself.

n.b. this *will* overwrite your `.config`; the old one will be saved in `.config.bak`,
but will of course be gone forever if you run the script a second time without doing a
```shell
cp -a .config.bak .config
```
in between runs.

