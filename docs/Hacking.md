# Hacking

## Install Arduino

Get Arduino from [Arduino.cc](https://www.arduino.cc/en/Main/Software)

## Add ESP8266 to Arduino

Start the Arduino IDE, go to File/Preferences, and add the following line to "Additional Boards Manager URLs:"  
`http://arduino.esp8266.com/stable/package_esp8266com_index.json`

Go to Tools/Board: "something"/Boards Manager...  
Search for "esp82", click on the "esp8266 by ESP Community", click Install

(for details, see [ESP8266 core for Arduino](https://github.com/esp8266/Arduino))

## Configure parameters for your board

* Select Tools / Board: "something" / "Generic ESP8266 Module"
* Select Tools / Board: "Generic ESP8266 Module" / Flash Size: "something" / 4M (1M SPIFFS)

The defaults of the other parameters should be fine.

## Add required libraries

It's a good idea to simply install all libraries that are supported,
even if you are not going to use them.

Select Sketch / Include Library / Manage Libraries...  
and install

* Adafruit BMP280 Library
* Adafruit BME280 Library
* DallasTemperature
* DHT sensor library
* PubSubClient
* SparkFun CCS811 Library
* LiquidCrystal I2C
* MH-Z19
* Adafruit ADS1x15
* jm\_PCF8574
* STM32duino Low Power
* Adafruit NeoPixel
* GxEPD2

## Clone this repository

```shell
cd ~/Arduino
git clone https://gitlab.com/schumar/ESP_BaPoTeSta.git
```

## Set up personal defaults

Compile-time settings can be done using Kbuild (the configuration system used
by the Linux kernel).

```shell
cd ESP_BaPoTeSta/firmware
make menuconfig
```

(cool people use `nconfig` instead of `menuconfig`, as it allows
entering/leaving submenus with cursor right/left, respectively)

If this does not work for you (maybe you forgot to install `libncurses5-dev`,
or you are on some weird OS), you can instead configure the options manually,

```shell
cd ESP_BaPoTeSta/firmware/ESP_BaPoTeSta
rm autoconf.h
cp ../configs/all_y_defconfig autoconf.h
```

and then edit `autoconf.h`. Disable features by putting `# ` in front of the
line, do *not* set to `n`!

## Start coding

Open the `ESP_BaPoTeSta` project in Arduino, and hack away. Of course, you can
also use your favourite editor/IDE instead.

## Compile

### Using the Arduino IDE

In the Arduino IDE, click "Verify".  
If you get an error about `autoconf.h`, see
[above (Set up personal defaults)](#set-up-personal-defaults).  
If you get an error about `versionfile.h`, run `make versionfile` on the commandline,
or create an empty file (it's only used for showing the version on LCDs and/or serial debugging output).

### Using `arduino-cli`

The provided `Makefile` uses `arduino-cli`; install it first:
```shell
cd /tmp
wget https://downloads.arduino.cc/arduino-cli/arduino-cli_latest_Linux_64bit.tar.gz
tar xf arduino-cli_latest_Linux_64bit.tar.gz
mv arduino-cli ~/bin/
cd -
```

If you don't want to use the IDE at all, you need to use `arduino-cli` to install
the support for ESP8266 and the libraries:

Create config file
```shell
arduino-cli config init
```

Add the required board-manager URLs to that file, e.g.
```yaml
board_manager:
  additional_urls:
    - http://arduino.esp8266.com/stable/package_esp8266com_index.json
    - https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json
```
and disable `metrics`.

Update the list of boards/libraries
```shell
arduino-cli core update-index
```

Optional, but recommended: Upgrade all existing boards/libraries:
```shell
arduino-cli upgrade
```

Install support for the boards you need:
```shell
arduino-cli core install esp8266:esp8266
arduino-cli core install STMicroelectronics:stm32
arduino-cli core install arduino:avr
```

and install the required libraries (see above for a full list):
```shell```
arduino-cli lib install 'Adafruit BMP280 Library' 'Adafruit BME280 Library' \
  DallasTemperature 'DHT sensor library' PubSubClient \
  'SparkFun CCS811 Arduino Library' 'LiquidCrystal I2C' MH-Z19 \
  'Adafruit ADS1X15' jm_PCF8574 'STM32duino Low Power' 'Adafruit NeoPixel' \
  GxEPD2
```

You can then
```shell
cd ~/Arduino/ESP_BaPoTeSta/firmware
make
```

Available targets:
* `make image_esp`: Compile for ESP8266
* `make flash`: Compile and flash to ESP8266 using serial connection
* `make fflash`: "Fast flash", don't compile but just send via serial connection, at 460800 Baud
* `make image_avr`: Compile for ATmega328
* `make flash_avr`: Compile flash to ATmega328 using serial connection
* `make image_stm32`: Compile for STM32 "Blue Pill"
* `make flash_stm32`: Compile and flash to STM32 using USB

## Upload

### OTA

The simplest way to flash a new firmware is to use the web-interface.  
Put a jumper bridge on the 2 "Conf" pins, and reset the board!

![Conf pins](pics/pins_conf.jpg)

Connect to the newly created WiFi-AP, and visit
[`192.168.0.4`](http://192.168.0.4/)

At the bottom of the page is the "Firmware: ... Update" function.

Provide the firmware file that was created in the "Compile" step above.

### Connect a Serial-to-USB adapter to your board

If the above doesn't work (brand-new board, or broken firmware), you
have to do it the manual way:

* Switch off your board.
* Ensure that your adapter is set to 3.3V!
* Connect the "GND" pin of the adapter to the "GND" pin of the ESP.
* Connect the "TX" pin of the adapter to the "RX" pin of the ESP, and vice versa.
* Do *not* connect VCC, unless you know what you are doing!
* Put a jumper bridge on the 2 "Flash" pins.

![Flash pins](pics/pins_flash.jpg)

(graphics proudly drawn with an [IBM TrackPoint(tm)](https://xkcd.com/243/)...)

### Connect to your PC

I guess you can figure out how to do this :)

### Compile and transfer firmware using the Arduino IDE

Press Ctrl+U in Arduino (or click the "Play" button)  
While the sketch is compiling, switch on the board.

If it doesn't work the first time, make sure that the correct serial port is
selected in Tools/Port, and try again.

### Compile and transfer firmware via commandline

```shell
cd ~/Arduino/ESP_BaPoTeSta/firmware
make flash
```

or, if you have a good Serial-to-USB adapter and short cables between it and
your BaPoTeSta, try
```
make fflash
```
instead -- this should be about 4 times faster.


## ATmega

To upload the firmware to the ATmega (via the RX/TX pins), you first need to
flash a bootloader (using a dedicated ISP-programmer), but only after setting
the correct fuses (see [fuse calculator](http://www.engbedded.com/fusecalc)):

    avrdude -p m328p -c usbasp -v -v -U lfuse:w:0xe2:m -U hfuse:w:0xde:m -U efuse:w:0x05:m
    avrdude -p m328p -c usbasp -v -v -U flash:w:optiboot_atmega328.hex:i

You can check the current fuse values using

    avrdude -p m328p -c usbasp -v -v

(this is also a good test to see if the programmer is working)

Arduino IDE board settings: Arduino Pro or Pro Mini, ATmega328 (3.3V, 8 MHz)

To compile or flash via commandline:

```shell
make image_avr
make flash_avr
```

# Blue Pill

Start the Arduino IDE, go to File/Preferences, and add the following line to "Additional Boards Manager URLs:"  
`https://github.com/stm32duino/BoardManagerFiles/raw/main/package_stmicroelectronics_index.json`

Go to Tools/Board: "something"/Boards Manager...  
Search for "stm32", click on the "STM32 MCU based boards", click Install

(for details, see [`Arduino_Core_STM32` on github](https://github.com/stm32duino/Arduino_Core_STM32))

In the Tools menu, choose
* Board: Generic STM32F1 series
* Board Part Number: BluePill F103CB (or C8 with 128k)
* U(S)ART Support: Enabled (no generic 'Serial')
* USB support (if available): CDC (generic 'Serial' supersede U(S)ART)
* C Runtime Library: Newlib Nano + Float Printf

The above will give you all debug output via USB; if you want it on A9 (TX1) instead, set
* U(S)ART Support: Enabled (generic 'Serial')
* USB support (if available): CDC (no generic 'Serial')

You also need to install those additional libraries:
* STM32duino Low Power
* STM32duino RTC

To compile or flash via commandline:

```shell
make image_stm32
make flash_stm32
```

