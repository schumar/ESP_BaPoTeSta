/*
 * Case for BaPoTeSta
 * with 18650 or 103450 battery
*/

// Render Bottom part
show_bot = true;
// Render Top part
show_top = true;
// View PCB
show_pcb = true;
// View Battery
show_bat = true;

board_rev = "0.8";  // ["0.7", "0.8"]

battery_type = 1;   // [0:18650, 1:103450]

// free space between PCB and battery (for throughole soldered pins)
battfree = 2;       // [0:0.5:3]

// height of PCB (incl connectors)
pcbh = 20;          // [5:25]

// width of PCB
pcbw = 39.3;
// length of PCB
pcbl = 49.5;

// wall strength
w = 1.8;            // [1:0.1:3]

// space between walls of bottom and top
spc = 0.3;          // [0:0.1:0.8]

// diameter of pegs for sensors
pegd = 2.8;

// This is just here to hide the rest of the variables in Customizer
module noop(){}

usbcenter = 7;      // distance from PCB edge to center of USB socket
usboffset = 3;    // distance from PCB floor to center of USB plug
usbwidth  = 9;      // width of USB socket
usbheight = 6;      // height of USB socket

huns = 25.4/100;    // hundreths inch
mh1x =  55*huns; // mounting hole 1 X (in 1/100 inch)
mh1y =  60*huns; // mounting hole 1 Y (in 1/100 inch)
mh2x =  15*huns; // mounting hole 2 X (in 1/100 inch)
mh2y = 160*huns; // mounting hole 2 Y (in 1/100 inch)
mh3x = 145*huns; // mounting hole 3 X (in 1/100 inch)
mh3y =  60*huns; // mounting hole 3 Y (in 1/100 inch)

baty = 120*huns; // battery pins Y
txy  = 185*huns; // TX pin Y
rsty =  90*huns; // Reset pin Y
ledy = 100*huns; // LEDs Y

espy = 127.5*huns;  // ESP center
espw =  17;      // width of ESP-12F
espo =   6;      // overhang of ESP

$fs=0.5;

// PCB size with added 0.5mm for printer inaccuracies
pcbwp = pcbw+0.5;
pcblp = pcbl+0.5;

battls = [83, 53];  // length of battery (+holder) (with cables!)
batths = [21, 11];  // height of battery (+holder) (with battery)
battws = [21, 35];  // width of battery (+holder)

// useful stuff
battl = battls[battery_type];
batth = batths[battery_type];
battw = battws[battery_type];

// MH2/MH3 screws only possible if battery is small enough
mhscrews = (mh3x-mh2x-4 > battw) ? 1 : 0;

battc = (mhscrews == 1) ? mh3x-2-battw/2 : pcbwp-battw/2-w;

pcbfloor = batth+battfree;

module round() {
    difference(){
        offset(r=w) children();
        children();
    }
}

module peg() cylinder(d=pegd, h=3);


// Floor
module floor(){
    translate([0,0,-w]) linear_extrude(w){
        offset(r=w) square([battl, pcbwp]);
    }
}

// Walls
module walls(){
    difference(){
        union(){
            // outer walls
            linear_extrude(pcbfloor+pcbh) {
                round() square([battl, pcbwp]);
            }
            // PCB guides
            linear_extrude(pcbfloor) {
                // left wall
                difference(){
                    square([pcblp, w]);
                    // battery pin, hole for battery wire
                    translate([baty-2, 0]) square([7, w+0.1]);
                }
                // right wall
                difference(){
                    translate([0, pcbwp-w]) square([txy-2, w]);
                    // Reset pin
                    translate([rsty-2, pcbwp-w]) square([4, w+0.1]);
                }
                // top wall
                square([w, battc-battw/2-2]);
                if (mhscrews == 1) {
                    // MH2
                    translate([mh2y-2, 0]) square([4, mh2x]);
                    translate([mh2y, mh2x]) circle(r=2);
                    // MH3
                    translate([mh3y-2, mh3x]) square([4, pcbwp-mh3x]);
                    translate([mh3y, mh3x]) circle(r=2);
                }
            }
            // PCB stoppers
            linear_extrude(pcbfloor+1.6-0.01) {
                translate([pcblp, 0]) square(w);
                translate([pcblp, pcbwp-w, 0]) square([w, w]);
            }
            // PCB fix (from above)
            translate([0, 0, pcbfloor+1.6+0.01]) linear_extrude(pcbh-1.6) {
                translate([mh2y+2, 0]) square([pcblp-mh2y-2,w]);
            }
        }
        // MH2 hole (M2)
        translate([mh2y, mh2x, pcbfloor-10]) cylinder(d=1.6, h=10.1);
        // MH3 hole (M2)
        translate([mh3y, mh3x, pcbfloor-10]) cylinder(d=1.6, h=10.1);
        // USB socket
        if (board_rev == "0.8") {
            translate([-w-0.1, usbcenter-usbwidth/2, pcbfloor+usboffset-usbheight/2])
                cube([w+0.2, usbwidth, usbheight]);
        } else if (board_rev == "0.7") {
            translate([usbcenter-usbwidth/2, -w-0.1, pcbfloor+usboffset-usbheight/2])
                cube([usbwidth, w+0.2, usbheight]);
        }
        // ESP antenna
        translate([espy-espw/2, pcbwp-0.1, pcbfloor+1.6]) cube([espw, espo+0.1, 1.2]);
        // charge LEDs
        for (o=[-2:2:2])
            translate([ledy+o-0.5, -w-0.1, pcbfloor+1.6]) cube([1, w+0.2, 6]);
        // marks for snap-in connections
        translate([-w-0.1, pcbwp/2-3, pcbfloor+1.6+2]) cube([0.1+0.15, 6, 6]);
        translate([battl+w-0.15, pcbwp/2-3, pcbfloor+1.6+2]) cube([0.1+0.15, 6, 6]);
    }
}

// Ceiling
module ceiling(){
    translate([0,0,pcbfloor+pcbh]) union() {
        linear_extrude(w) difference() {
            // base plate
            offset(r=w) square([battl, pcbwp]);
            // hole for cables
            translate([0, pcbwp/2-10*huns]) square([50*huns, 20*huns]);
        }
        // add some pegs to attach sensors
        // they have 2.5mm holes, BME280: 1 hole, BMP280: 2 holes, 10.8mm dist,
        // TSL2561 8.7mm dist, 811 15.5mm
      translate([0, 0, w-pegd/2]) union() {
            translate([3, -w, 0]) rotate([90, 0, 0]) peg();
            translate([3+8.7, -w, 0]) rotate([90, 0, 0]) peg();
       //     translate([-w+0.1, pcbwp/2, 0]) rotate([0, -90, 0]) peg();
       //     translate([-w+0.1, pcbwp/2-10.8, 0]) rotate([0, -90, 0]) peg();
       //     translate([-w, pcbwp/2+15.5, 0]) rotate([0, -90, 0]) peg();
       }
    }
}

module case(){
    floor();
    walls();
    ceiling();
}

// bounding box to differentiate between bottom and top part of case
module bottombox(d=-spc/2){
    translate([-w-0.1, -w-0.1, -w-0.1]) cube([battl+2*w+0.2, pcbwp+2*w+0.2, pcbfloor+1.6+w+0.1]);
    // edge
    translate([0, 0, pcbfloor+1.6-0.01]) union() {
        linear_extrude(2) {
            difference() {
                offset(delta=w+0.1) square([battl, pcbwp]);
                offset(delta=w/2-d) square([battl, pcbwp]);
                // don't create edge over ESP antenna
                translate([espy-espw/2, pcbwp-0.1]) square([espw, espo+0.1]);
            }
        }
    }
}

module connectors() {
    translate([0, 0, pcbfloor+1.6-0.01]) union() {
        translate([-w/2, pcbwp/2-6, 1]) rotate([-90, 0, 0]) cylinder(d=1.2, h=12, $fs=0.1);
        translate([battl+w/2, pcbwp/2-6, 1]) rotate([-90, 0, 0]) cylinder(d=1.2, h=12, $fs=0.1);
    }
}


// Bottom part of case
if (show_bot) {
    color([0,1,1,0.5]) render() intersection(){
        case();
        union() {
            bottombox(-spc/2);
            connectors();
        }
    }
}

// Top part of case
if (show_top) {
render() difference(){
        difference(){
            case();
            #translate([-w-0.1, pcbwp+w-8.5,+pcbfloor]) cube([18, 9, 31]);
        }
        union() {
            bottombox(+spc/2);
            connectors();
        }
    }
}

// PCB
if (show_pcb) {
    %translate([0,0,pcbfloor]) rotate([0,0,90]) import(str("bapotesta_v", board_rev, ".stl"));
}

// Battery (+holder)
if (show_bat) {
    if (battery_type == 0) {
        %translate([(battl-65)/2, battc, batth-18/2]) rotate([0,90,0]) color("green") cylinder(d=18, 65);
        %color("black") union() {
            translate([2, battc-battw/2, 0])        cube([battl-4, battw, batth-18]);
            wall = (battl-65)/2-4;
            translate([2, battc-battw/2, batth-18]) cube([wall, battw, 18]);
            translate([battl-wall-2, battc-battw/2, batth-18]) cube([wall, battw, 18]);
        }
    } else if (battery_type == 1) {
        // we draw the battery shorter by 2mm, as this is taken up by cables etc
        %color("grey") translate([1, battc-battw/2, 0]) union() {
            translate([0, batth/2, batth/2]) rotate([0, 90, 0]) cylinder(d=batth, h=battl-2);
            translate([0, batth/2, 0])       cube([battl-2, battw-batth, batth]);
            translate([0, battw-batth/2, batth/2]) rotate([0, 90, 0]) cylinder(d=batth, h=battl-2);
        }
    }
}

