#!/usr/bin/perl -w
#
# Make pretty graphs out of the data in the RRD files created
# by mqttlog.pl
#

use warnings;
use strict;

use RRDs;
use POSIX qw(strftime);
use Data::Dumper;

my $basedir = '/home/martin/sensors';
my $outdir = '/srv/http/sensors/graphs';

my %chipname = (
    'chip-009d557a' => 'Sensor 0 Hasengehege',
    'chip-009baf92' => 'Sensor 1 Wohnzimmer',
    'chip-001199a1' => 'Sensor 2 Schlafzimmer',
    'chip-00119998' => 'Sensor 3 Hasengehege',
);

my %sensorname = (
    '0000' => 'intern',
    '000b' => 'DHT11',
    '0015' => 'DHT21',
    '0016' => 'DHT22',
    '002a' => 'NTC',
    '0189' => 'Sen189',
    '0219' => 'MH-Z19',
    '0280' => 'BMP280',
    '0537' => 'Ziel',       # "SET"
    '0701' => 'GPS',
    '07d5' => 'TDS',
    '0811' => 'CCS811',
    '0f00' => 'Dallas',
    '1115' => 'ADCch0',
    '1116' => 'ADCch1',
    '1117' => 'ADCch2',
    '1118' => 'ADCch3',
    '1280' => 'BME280',
    '1820' => 'Dallas',
    '2561' => 'TSL2561',
    '2812' => 'LEDs',
    '4104' => 'AJSR04',
    '4347' => 'Heizng',     # "HEAT"
    'b8ff' => 'Dallas',
);


my %typename = (
    'temp'        => 'Temperatur',
    'humidity'    => 'Feuchtigkeit',
    'tempHI'      => 'Hitzeindex',
    'battery'     => 'Batteriespannung',
    'time'        => 'Messdauer',
    'pressure'    => 'Luftdruck',
    'pressureASL' => 'Luftdruck 0m',
    'power'       => 'Leistung',
    'wifiRSSI'    => 'WiFi RSSI',
    'GPS-sats'    => 'GPS Sats',
    'GPS-time'    => 'GPS Tageszeit',
    'GPS-date'    => 'GPS Datum',
    'latitude'    => 'Breitengrad',
    'longitude'   => 'Laengengrad',
    'altitude'    => 'Hoehe',
    'GPS-hdop'    => 'GPS Genauigkeit',
    'speed'       => 'Geschwindigkeit',
    'direction'   => 'Richtung',
    'eCO2'        => 'equiv. CO2',
    'eTVOC'       => 'fluechtige Komp',
    'CO2'         => 'CO2',
    'CO2bg'       => 'CO2 (basis)',
    'CO2calc'     => 'CO2 (berechnet)',
    'CO2raw'      => 'CO2 (roh)',
    'distance'    => 'Entfernung',
    'progress'    => 'Fortschritt',
    'light'       => 'Licht',
    'lightBB'     => 'Licht Breitband',
    'lightIR'     => 'Licht Infrarot',
    'luminance'   => 'Helligkeit',
    'TDS'         => 'TDS',
    'turbidity'   => 'Verschmutzung',
    'freemem'     => 'Freier RAM',
    'voltage'     => 'Spannung',
);

my %unit = (
    'temp'        => {name => 'Celsius',  div => 100},
    'humidity'    => {name => 'Prozent',  div => 100},
    'battery'     => {name => 'Volt',     div => 1000},
    'time'        => {name => 'Sekunden', div => 1e6},
    'pressure'    => {name => 'hPa',      div => 100},
    'pressureASL' => {name => 'hPa',      div => 100},
    # 'power'       => {name => 'Prozent',  div => 100},
    'power'       => {name => 'Watt',     div => 100},
    'wifiRSSI'    => {name => 'dBm',      div => 1},
    'altitude'    => {name => 'Meter',    div => 1},
    'latitude'    => {name => 'Grad',     div => 1e6},
    'longitude'   => {name => 'Grad',     div => 1e6},
    'speed'       => {name => 'Knoten',   div => 1000},
    'direction'   => {ignore => 1},
    'GPS-time'    => {ignore => 1},
    'GPS-date'    => {ignore => 1},
    'GPS-sats'    => {name => 'Anzahl',   div => 1},
    'GPS-hdop'    => {name => 'Meter',    div => 10},
    'eCO2'        => {name => 'ppM',      div => 1},
    'eTVOC'       => {name => 'ppB',      div => 1},
    'CO2'         => {name => 'ppM',      div => 1},
    'CO2calc'     => {name => 'ppM',      div => 1},
    'CO2raw'      => {name => 'Wert',     div => 1},
    'distance'    => {ignore => 1},
    'progress'    => {ignore => 1},
    'light'       => {name => 'Wert',     div => 1},
    'luminance'   => {name => 'Lux',      div => 1},
    'raw'         => {name => 'Wert',     div => 1},
    'lux'         => {name => 'Lux',      div => 1},
    'byte'        => {name => 'Byte',     div => 1},
    'TDS'         => {name => 'TDS',      div => 1},
    'turbidity'   => {name => 'NTU',      div => 1},
    'freemem'     => {name => 'kiB',      div => 1024},
    'voltage'     => {name => 'mV',       div => 1000},
);

my %range = (
    'daily' => {
        'duration' => '24h',
        'retrieve' => '49h',
        'previous' => 86400,
        'smooth'   => 3600,
        'name'     => 'Tag',
    },
    'weekly' => {
        'duration' => '7d',
        'retrieve' => '15d',
        'previous' => 86400*7,
        'smooth'   => 3600*6,
        'name'     => 'Woche',
    },
    'monthly' => {
        'duration' => '30d',
        'retrieve' => '65d',
        'previous' => 86400*30,
        'smooth'   => 86400,
        'name'     => 'Monat',
    },
    'yearly' => {
        'duration' => '365d',
        'retrieve' => '737d',
        'previous' => 86400*30*365,
        'smooth'   => 86400*7,
        'name'     => 'Jahr',
    },
);

my %consfunc = (
    'min' => 'MINIMUM',
    'avg' => 'AVERAGE',
    'max' => 'MAXIMUM',
    'lst' => 'LAST',
);

# colors taken from http://colorbrewer2.org/
## my @colors = qw( 1b9e77 d95f02 7570b3 e7298a 666666 e6ab02 66a61e a6761d );
my @colors = qw( 1b9e77 d95f02 7570b3 e7298a 666666 e6ab02 66a61e a6761d
                 2bae77 e96f02 8580b3 f7398a 767666 f6bb02 76b61e b6861d
                 1b9e77 d95f02 7570b3 e7298a 666666 e6ab02 66a61e a6761d
                 2bae77 e96f02 8580b3 f7398a 767666 f6bb02 76b61e b6861d
                 1b9e77 d95f02 7570b3 e7298a 666666 e6ab02 66a61e a6761d
                 2bae77 e96f02 8580b3 f7398a 767666 f6bb02 76b61e b6861d
                 );

# Collect a list of RRD files, e.g.
# chip-009d557a/sensor-002a-temp-centdegc.rrd
# chip-001199a1/sensor-0016-humidity-centpercent.rrd

opendir (my $dh, $basedir) || die "can't opendir $basedir: $!.";

my %rrdfiles;
my %prettyname;

while (readdir $dh) {
    if (/(chip-.*)/) {
        my $chipid = $1;
        my $chipdir = "$basedir/$1";
        opendir (my $dhc, $chipdir) || die "can't opendir $chipdir: $!.";
        while (readdir $dhc) {
            next if /^\./;
            # sensor-0701-GPS sats-.
            if (/(^sensor-(\w+)-([\w -]+)-(\w*).rrd)$/) {
                my $rrdfile = "$chipdir/$1";
                my $sensorid = $2;
                my $type = $3;
                my $unit = $4;

                next if $unit eq 'raw';

                # printf "Found $rrdfile\n";

                unless (exists $typename{$type}) {
                    print STDERR "Can't find pretty typename for '$type'\n";
                    $typename{$type} = $type;
                }

                $prettyname{$rrdfile} = sprintf '%-22s %-7s %s',
                    (exists $chipname{$chipid} ? $chipname{$chipid} : "Sensor $chipid"),
                    (exists $sensorname{$sensorid} ? $sensorname{$sensorid} : ''),
                    $typename{$type};

                # Put some different types into a single graph
                $type = 'temp' if $type eq 'tempHI';
                $type = 'light' if $type eq 'lightBB' or $type eq 'lightIR';
                $type = 'CO2' if $type eq 'CO2calc';
                $type = 'CO2' if $type eq 'CO2bg';
                # $type = 'pressure' if $type eq 'pressureASL';
                push @{$rrdfiles{$type}}, $rrdfile;

            } else {
                printf "WARN: Found extraenous file $chipdir/$_\n"
                    unless /\.bak$/;
            }
        }
    }
}

#print Dumper(\%rrdfiles);
#print Dumper(\%prettyname);

foreach my $type (keys %rrdfiles) {

    unless (exists $unit{$type}) {
        print STDERR "Don't know how to handle type '$type'\n";
        next;
    }

    next if exists $unit{$type}{'ignore'};

    # prepare arrays
    my $idx = 0;
    my @file;
    my @name;
    my @def;
    foreach my $file (sort @{$rrdfiles{$type}}) {
        next if time() - (stat($file))[9] > 7*24*3600;
        $file[$idx] = $file;
        $name[$idx] = $prettyname{$file};
        $def[$idx] = "${type}${idx}";
        $idx++;
    }

    foreach my $range (keys %range) {

        my @opts = (
            "$outdir/$type-$range.png",
            '--end', 'now', '--start', 'end-'.$range{$range}{'duration'},
            '--title', $typename{$type}.' ('.$range{$range}{'name'}.')',
            '--vertical-label', $unit{$type}{'name'},
            '--width', '700', '--height', '350',
            '--slope-mode', '--imgformat', 'PNG',
        );

        # As the atmospheric pressure is usually around 1000 (mbar), we need
        # to use alt-auto-scale
        push @opts, '--alt-autoscale', '--alt-y-grid' if $type =~ /^pressure/;

        # header for legend
        push @opts,
            #         Sensor 0 Schlafzimmer Dallas Batteriespannung
            sprintf 'COMMENT:  %45s  Minimum   Schnitt  Maximum    Jetzt\l', '';

        # build value definitions (DEF, VDEF, CDEF)
        for my $i (0..$#file) {
            # raw value from file
            push @opts, sprintf 'DEF:raw%s=%s:value:AVERAGE:start=end-%s',
                $def[$i], $file[$i], $range{$range}{'retrieve'};
            # scale (e.g. centidegree -> degree)
            push @opts, sprintf 'CDEF:%s=raw%s,%d,/',
                $def[$i], $def[$i], $unit{$type}{'div'};
            # get min/avg/max/last for legend
            foreach my $cons ('min', 'avg', 'max', 'lst') {
                push @opts, sprintf 'VDEF:%s%s=%s,%s',
                    $cons, $def[$i], $def[$i], $consfunc{$cons};
            }
            # smooth curves
            push @opts, sprintf 'CDEF:smo%s=%s,%d,TRENDNAN',
                $def[$i], $def[$i], $range{$range}{'smooth'};
            push @opts, sprintf 'SHIFT:smo%s:-%d',
                $def[$i], $range{$range}{'smooth'} / 2;
            # previous day/week/whatever
            push @opts, sprintf 'CDEF:prv%s=%d,1,%d,%s,PREDICT',
                $def[$i], $range{$range}{'previous'} - $range{$range}{'smooth'}/2,
                $range{$range}{'smooth'}, $def[$i];
        }

        # Now draw the lines

        # line at 0 for temp-graphs
        push @opts, sprintf 'HRULE:0#00000080' if $type eq 'temp';
        # line at 3.2V for battery graphs
        push @opts, sprintf 'LINE1:3.2#C00000' if $type eq 'battery';

        for my $i (0..$#file) {
            # the previous period
            push @opts, sprintf 'LINE1:prv%s#%sc0', $def[$i], $colors[$i];
            # current line
            push @opts, sprintf 'LINE2:%s#%s80', $def[$i], $colors[$i];
            # the smoothed line
            push @opts, sprintf 'LINE2:smo%s#%s:%-45s', $def[$i], $colors[$i], $name[$i];

            # and add the values to the legend
            foreach my $cons ('min', 'avg', 'max', 'lst') {
                push @opts, sprintf 'GPRINT:%s%s:%%7.2lf', $cons, $def[$i];
            }
            push @opts, 'COMMENT:\l';

        }

        RRDs::graph(@opts);
        my $err = RRDs::error;
        if ($err) {
            printf STDERR "ERROR while creating $type-$range.png: $err.";
            next;
        }
        #print Dumper(\@opts);
        #print "\n";
    }

}

