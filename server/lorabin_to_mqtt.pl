#!/usr/bin/perl -w

use strict;
use warnings;
use Data::Dumper;
$Data::Dumper::Terse = 1;
$Data::Dumper::Sortkeys = 1;
use Net::MQTT::Simple;
use POSIX;

my $mqtt = Net::MQTT::Simple->new('sheeana');
my $port = '/dev/ttyUSB0';
$port = $ARGV[0] if @ARGV;

system('stty', '-F', $port, '9600', '-parenb', 'cs8', 'raw');

open my $fd, '<', $port or die $!;

$| = 1;

# build bitfield for select()
my $rin = '';
vec($rin, fileno($fd), 1) = 1;
my $ein = $rin;

my @type = (
    "temp",
    "battery",
    "humidity",
    "time",
    "tempHI",
    "pressure",
    "pressureASL",
    "wifiRSSI",
    "GPS-sats",
    "GPS-time",
    "GPS-date",
    "latitude",
    "longitude",
    "altitude",
    "GPS-hdop",
    "speed",
    "direction",
    "distance",
    "power",
    "lightBB",
    "lightIR",
    "luminance",
    "eCO2",
    "eTVOC",
    "CO2",
    "CO2bg",
    "CO2raw",
    "voltage",
    "TDS",
    "turbidity",
    "freemem",
);

my @unit = (
    "centdegc",
    "percent",
    "raw",
    "millivolt",
    "microsec",
    "centpercent",
    "pascal",
    "dbm",
    "",
    "microdegree",
    "millidegree",
    "meter",
    "decimeter",
    "seconds",
    "milliknots",
    "meterperhour",
    "mmpersec",
    "dekameter",
    "centinautmile",
    "centiwatt",
    "lux",
    "ppm",
    "ppb",
    "millimeter",
    "microvolt",
    "ntu",
    "byte",
);

while (1) {
    my @in = ();
    while (my $nfound = select(my $rout = $rin, undef, my $eout = $ein, 1)) {
        last if $rout ne $rin or $eout ne chr(0);
        sysread $fd, my $in, 1;
        push @in, ord($in);
    }
    next if @in == 0;
    printf "%s\n", POSIX::strftime("%F %TZ", gmtime());
    parse(\@in);
    print "\n";
}


sub parse {
    my @raw = @{$_[0]};
    my @rawcopy = @raw;

    printf "IN (%2d): %s\n", scalar @raw, join(' ', map { sprintf('%02x', $_) } @raw);

    if (@raw < 9) {     # shortest message has start+chip+nr+end, 2+4+1+2
        printf "%-8s %s\n", 'STUB:', join(' ', map { sprintf('%02x', $_) } @raw);
        return;
    }

    my $n = 0;

    # search for start pattern
    my $start = search(\@raw, [ 0xC0, 0xFE ]);
    unless (defined $start) {
        printf "%-8s %s\n", 'NOC0FE:', join(' ', map { sprintf('%02x', $_) } @raw);
        return;
    }
    splice @raw, 0, $start+2;      # remove start pattern

    my $chipid = join '', map { sprintf('%02x', $_) } reverse splice @raw, 0, 4;
    #printf "ChipID: %s\n", $chipid;

    my $nr = splice @raw, 0, 1;
    #printf "nrMeas: %d\n", $nr;

    if (@raw < (8 * $nr + 2)) {
        printf "%-6s %d<%d %s\n", 'SHORT:', scalar @raw, 8*$nr+2, join(' ', map { sprintf('%02x', $_) } @raw);
        return;
    }

    my @data;
    for my $i (1..$nr) {
        push @data, {
            sensorid => shift(@raw) + (1<<8)*shift(@raw),
            type     => $type[shift @raw],
            unit     => $unit[shift @raw],
            value    => (shift(@raw) + (1<<8)*shift(@raw) + (1<<16)*shift(@raw) + (1<<24)*shift(@raw))
        }
    }
    my $end = search(\@raw, [ 0xBE, 0xAD ]);
    unless (defined $end) {
        printf "%-8s %s\n", 'NOBEAD:', join(' ', map { sprintf('%02x', $_) } @raw);
        return;
    }
    if ($end > 0) {
        # The remaining part should be BE AD; if it isn't, sth weird has
        # happened, let's start again after removing the first byte.
        printf "%-8s %s\n", 'NOTBEAD:', join(' ', map { sprintf('%02x', $_) } @raw);
        splice @rawcopy, 0, 1;
        parse(\@rawcopy);
        return;
    }

    splice @raw, 0, 2;      # remove end pattern

    # all is fine!!
    #print Dumper($chipid, \@data);
    for my $i (0..$nr-1) {
        $mqtt->publish(sprintf("chip-%s/sensor-%d/%s-%s", $chipid, $data[$i]{sensorid}, $data[$i]{type}, $data[$i]{unit}) => $data[$i]{value});
        printf("chip-%s/sensor-%d/%s-%s ", $chipid, $data[$i]{sensorid}, $data[$i]{type}, $data[$i]{unit}); printf "%d\n", $data[$i]{value};
    }

    printf "%-8s %s\n", 'REST:', join(' ', map { sprintf('%02x', $_) } @raw);
}


sub search {
    my @hay = @{$_[0]};
    my @needle = @{$_[1]};
    my $len = @needle;
    for my $i (0..(@hay-$len)) {
        my $found = 1;
        for my $j (0..$#needle) {
            if ($hay[$i + $j] != $needle[$j]) {
                $found = 0;
                last;
            }
        }
        return $i if $found;
    }

    return;
}




