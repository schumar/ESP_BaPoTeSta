#  ESP8266 Battery Powered Temperature Station

## Intro

Using only an ESP8266 (no Atmel AVR) and a sensor, it's possible to build a very cheap and power-efficient WiFi-enabled weather station.

The sensors are reporting their data via MQTT, so the data can be received with [OpenHAB](http://www.openhab.org/),
[FHEM](http://fhem.de/fhem.html) or a small Perl script.

There are now more than 100 BaPoTeStas (50 of the latest version, v0.8).

## Documentation

* [Manual](docs/Manual.md)
* [Hacking](docs/Hacking.md)
* [Shopping list](docs/Einkaufsliste.md) (German)

![BaPoTeSta v0.8 Schematic](docs/Schematic.png)

## Pictures

![BaPoTeSta Board v0.8 with ATmega Rendering](docs/pics/board_v0.8_render.jpg)

![BaPoTeSta Webinterface](docs/pics/webinterface.png)

![RRD Graph](docs/pics/temp-daily.png)

## To Do

see [Issues](https://gitlab.com/schumar/ESP_BaPoTeSta/issues)

## Thanks

* Many thanks to all the people working on the [Arduino core for ESP8266](https://github.com/esp8266/Arduino)
* Cheers to [jdunmire](https://github.com/jdunmire/kicad-ESP8266) for his ESP8266 KiCAD library!
