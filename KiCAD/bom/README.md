# Interactive BOM

Generated with

```shell
python2 ./InteractiveHtmlBom/generate_interactive_bom.py --dark-mode --checkboxes Placed --layer-view F --blacklist 'REF*,U5,MH*,TP*,P6' --sort-order 'R,D,U,Q,C,~,P'  ~/Arduino/ESP_BaPoTeSta/KiCAD/ESP_BaPoTeSta.kicad_pcb
```

using [Interactive HTML BOM plugin for KiCad](https://github.com/openscopeproject/InteractiveHtmlBom)

As this should be a help for assembly, I've manually commented out the "Footprint" column.
